[Setup]
AppName=SMAC Thread Check Center
AppVersion=1.05
DefaultDirName={pf}\SMAC\SMAC Thread Check Center
DefaultGroupName=SMAC Thread Check Center
UninstallDisplayIcon={app}\SMAC Thread Check Center.exe
;LicenseFile=Files\License.txt
OutputDir=.\Output

[InstallDelete]
Type: files; Name: "{app}\Actuators\*"

[Files]
Source: "Files\SMAC Thread Check Center.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Documenten\Help document.pdf"; DestDir: "{app}"; Flags: ignoreversion
Source: "Files\Actuators\*"; DestDir: "{app}\Actuators"; Flags: ignoreversion recursesubdirs

; Source: "Readme.txt"; DestDir: "{app}"; \
; Flags: isreadme

[Icons]
Name: "{commonprograms}\SMAC Thread Check Center"; Filename: "{app}\SMAC Thread Check Center.exe"
Name: "{commondesktop}\SMAC Thread Check Center"; Filename: "{app}\SMAC Thread Check Center.exe"

[Run]
Filename: "{app}\SMAC Thread Check Center.exe"; Description: "Launch SMAC Thread Check Center"; Flags: postinstall nowait skipifsilent