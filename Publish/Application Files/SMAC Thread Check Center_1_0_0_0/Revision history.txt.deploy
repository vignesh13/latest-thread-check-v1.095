Version 1.05

25 September 2014

- Demo add new actuator

====================================================================================================
Version 1.04

7 August 2014

- Double allowable rotary error during start of rotary motion.

====================================================================================================
Version 1.03

6 August 2014

- Increase allowable rotary error during start of rotary motion.

====================================================================================================
Version 1.02

5 August 2014

- Added .ini file for LAR300-50-81 (GPL42 with 16!1 gear)
- Changed/ corrected file name of LAR55-50-71 (P8542 with 16!1 gear)
                Now this is: LAR55-50-71 (P8524 with 16!1 gear)
- Changed/ corrected file name of LAR55-50-75 (P8542 with 16!1 gear)
                Now this is: LAR55-50-75 (P8524 with 16!1 gear)
- Changed/ corrected file name of LAR55-100-71 (P8542 with 16!1 gear)
                Now this is: LAR55-100-71 (P8524 with 16!1 gear)
- Updated help document


====================================================================================================
Version 1.01

21 May 2014

- Make sure push-pull test allways reports a positive value
- Added Ctrl-A (select all) and Ctrl-C (Copy selection to clipboard) to controller output window

====================================================================================================
Version 1.0

13 May 2014

- Added actuator LAR55-100-75 (MM3042 with 14:1 gear)
- Added actuator LAR55-100-75 (RE35 with 14:1 gear)
- Added actautor LAR55-100-75 (RE35 with 4 8:1 gear)
- Added actuator LAR55-50-71 (P8542 with 16:1 gear)
- Added actuator LAR55-50-71 (RE35 with 4 8:1 gear)
- Modified actuator LAR55-50-75 (P8542 with 16:1 gear)

====================================================================================================
Version 1.0RC4

13 November 2013

- Changed color of reported values:
  Green: Value is checked and is within limits
  Red:   Value is checked and is out of limits
  Black: Value is not checked
- Increased SQ in MD42 from 30000 to 32000
- Actuator definition file [Constants] section was not read correctly
- updated LAR55-50-75 (P8542 with 16!1 gear).ini



====================================================================================================
Version 1.0RC3

10 october 2013

- Added Actuator LAR55-50-75 (P8542 with 16:1 gear)
- Added Actuator LAR55-100-71 (P8542 with 16:1 gear)
====================================================================================================
Version 1.0RC2

09 september 2013

- Added the tooltip texts
- Removed undo button from teach results.
- Check if thread depth is at least 4 pitches if the "On top" push/pull test is selected
- Changed MD120 (including comment): Added a 0MF command as soon as the max rotary error is exceeded
- New serial handler (copied from LAC-X editor)
  Allways connect to controller at 57600 baud.
  Baudrate is temporary changed to 9600 baud during save program in controller to prevent overrun
  errors.
- Serial status window added
- Added a BR57600 in MD0
- Solved problem with MJ10 in MD45, 46 and 114
- Report rotary error vs position only every 20 msec to prevent too much data for the chart
  The higher baud rate makes this necessary
- Disable Run, Single Cycle and Stop buttons during download program

====================================================================================================
Version 1.0RC1

02 august 2013

- Added software installation instructions and help document
- Changed lower limit for permisable torque input field from 0% to 10%
- Changed default value for permisable torque input field from 50% to 100%
- Removed macro 225 as this is no longer used
- Add line number to error messages to get a better clue of the source
  Removed line number fron the DEPTH PASSED message
- Removed tooltips (temporary) until the text is known
- Corrected comment in rotate in reverse: 0.5 into 0.2 pitch
- Removed license agreement until the text is known
- Minimized the "Dump raw program" button to 5 x 5 pixels.


====================================================================================================
Version 0.1B21

31 july  2013

- Unlinked homong parameters for the linear from the overall speed setting. the Parameters are now
  renamed: VF_HOMING_LINEAR, AF_HOMING_LINEAR and QF_HOMING_LINEAR. The F stands for fixed
- Modified rotary checks on reverse move:
  Implemented a watchdog timer for the total reverse move. Time = 2 x time to come down + 3 seconds
  Implemented a periodic check (every 100 msec) if the rotary moves at least 1 count.
- Removed 3 actuator definition files and keep only the one that has been tested.
- Hide Show all checkbox on the Run tab

====================================================================================================
Version 0.1B20

29 july  2013

- Added EV3 in MD25 to solve problem with non-functioning Reset input
====================================================================================================
Version 0.1B19

26 july  2013

- Changed text "NO THREADS" into "Incorrect Thread Pitch" in MD125
- Changed the Sensitivity from percentage to counts. Range now 10 .. 1250 counts.
  Also changed the name fron Sensitivity to "Rotary sensitivity"

====================================================================================================
Version 0.1B18

24 july  2013

- Added some comments on the thread plug gauge in the bitmaps on the Setup and Run screen
- Set pitch limit to 0.5 pitch (in stead of 0.8) during rotation out of the thread
- Set allowable rotary error to 16000 when rotationg out of the thread.

====================================================================================================
Version 0.1B17

03 july  2013

- Replaced bitmaps on the Setup and Run tabs
- Added a Tuning tab and completely redesigned the layout

====================================================================================================
Version 0.1B16

01 july  2013

- Changed the WN0 and WF0 instructions to real wait loops. Reason: Input3 interrupt can not
  be serviced during a WF of WN instruction
- Created a new macro (macro 6) that waits for the interrupt input to be active.
- Changed the WN3,WF3 sequences in macro 14, 222 and 227 in a jump to this new macro
- Set CN0 as soon as the linear is retracted (in macro 115)

====================================================================================================
Version 0.1B15

28 jun  2013

- Changed rotary sensitivity to 3 * the commanded sensitivity for any rotate out of thread (macro 51 and macro 121)
- Corrected Macro 222 and 225 to act on the reset input (input 3). Also swapped the WF and WN sequence
- Deleted homing routine for the rotary. Rotary is allways home.
- Duplicated the Run button on the Run tab.
- Modified the PLC handshake to be more robust.
  
  For the PLC:
  Step 1: Set the START output LOW
  Step 2: Wait for the READY input to be HIGH.
          This indicates that the actuator is in the retracted position and that the measurement
          results can be read (PASS, FAIL and ERROR inputs)
  Step 3: Read the result of the last measurement (PASS, FAIL and ERROR inputs)
  Step 4: Wait until a new part must be measured and then set the START output HIGH
  Step 5: Wait for the READY input to go LOW. This indicates that the measurement has started
  Step 6: Set the START output LOW and loop back to step 2

  For the Controller:
  Step 1: Wait for the START input to be LOW. Normally this should be the case so this does not
          introduce an extra delay.
  Step 2: Wait an extra 20 msec to debounce the start input
  Step 3: Set the READY output HIGH. This tells the PLC that the results of the last measurement can be
          read and that the actuator is in retracted position.
  Step 4: Wait for the START input to be HIGH. This indicates that the PLC has read the results of the
          last measurement
  Step 5: Set the READY output LOW to tell that the measurement has started and start the measurement.
          During the measurement the PASS, FAIL and ERROR outputs are determined.
  Step 6: After completion of the measurement loop back to step 1
  


====================================================================================================
Version 0.1B14

26 jun  2013

- Added a MC151 in macro 95
- Removed Macro 123 (called nowhere)
- Changed text "DULL THREAD" into "NO THREADS" in macro 125
- Changed macro 4 so that the interrupt input #3 works correctly
- Changed Macro 14 WF1,WN1 into WN3,WF3
- Added a WA150 at the end of macro 50
- Make allowable rotary error during reverse thread find fixed for all actuators (1000 counts)


====================================================================================================
Version 0.1B13

24 jun  2013

- Increased upper limit for push/pull test input field to 5 mm
- Added a WA200 at the end of macro 60 to ignore the initial spike in rotary following error
- Increased allowed rotary error during first 1.5 revolution (in macro 61) by a factor of 3.
  3.0_MAX_ROTARY_ERROR varialble created 
- Changed resolution for sensitivity: 200% now 1250 encoder counts
- Added FR, RI and OO settings for both linear and rotary
  The parameter names and default values in the actuator definition files are:
  FR_LINEAR=1
  RI_LINEAR=0
  OO_LINEAR=0
  FR_ROTARY=0
  RI_ROTARY=0
  OO_ROTARY=0
- Set fail output on all jam/ dull thread conditions (macro 220 and 225)
- Added a SS2 command in macro 1 to define the servo speed.
- Added a WA1 and a MC151 in macro 110


====================================================================================================
Version 0.1B12

14 jun  2013

- Solved problem with serial port on built-in ports.

====================================================================================================
Version 0.1B11

14 jun  2013

- Modified the homing routine for the linear so that this can be called
- Teach routine: Added a teached SQ for the linear home position
  Measured SQ at home position is shown in the GUI
  SQ at home is set at this value + 4000


====================================================================================================
Version 0.1B10

13 jun  2013

- Teach routine: 2.0 pitch in stead of 1 pitch
- Teach routine: Start rotary motor in reverse after landing on part to prevent picking up the thread
- Teach routine: Stop rotary at 2.0 pitch above landing
- Added a magic WA1 in the push/pull test
- Changed default value of Q_FOR_PUSH_PULL_TEST_LINEAR from 20000 into 32000
  This change is also made in the actuator definition files.
- Changed sensitivity for reverse find thread start detection from 0.5 pitch to 0.2 pitch
- The sensitivity setting for rotary has been changed.
  Old setting: 100% corresponds to 0.25 rev of the outgoing shaft
  New setting: 100% corresponds to 1250 encoder counts for all actuators
  Decimal values (example: 25.3%) are allowed
  Sensitivity range changed from 1 to 100 into 1 to 200 % 
- Changed names of actuator definition files. (removed shaft info and added extra rotary info)
  LAR55-100-71FS(with 14:1 gear ratio) is changed into LAR55-100-71 (RE35 with 14:1 gear)
- Added variables Q_BREAKOUT_ROTARY (default value 32000) and T_BREAKOUT_ROTARY (default value 400)
  When starting the rotary in reverse Q_BREAKOUT_ROTARY will be applied during T_BREAKOUT_ROTARY msec
- Reset input: Inserted a WF1 before each WN1 instruction
- Set Pass output after correct part measurement
  


 
====================================================================================================
Version 0.1B9

29 may  2013

- Make sure that rotary is off when idle.
- Cycle time was not correct in single cycle mode

====================================================================================================
Version 0.1B8

22 may  2013

- Rotary resolution was not copied to the application program
- Added Sensitivity setting for the allowable rotary error. 100% means 0.25 revolution of the rotary axis
- Changed units of "Max permisable torque" in file header to percent
- Add options for check at bottom / check at top for thread clearance.
- Add text "Push / pull  Ok" in master program
- Added contact information
- Added Help document (document still empty)


====================================================================================================
Version 0.1B7

16 may  2013

- Changed comment in master program: Reset input is input #3 (insted of #1)
- Installer now deletes all file from the Actuators folder on install
- Added the TPI/mm setting in the header of the output file
- Duplicate Landed height, Measured depth and thread clearance on the setup screen.
- Added Teached landing height textbox.
- Added cycle timer
- Actuator did not rotate out of thread automatic after a "rotary blocked" error during the first
  1.5 revolution running into the thread.
- Report achieved depth also at a dull thread or a rotary blocked error
- Keep rotary on during entire "rotate out of thread and send linear back home" process.
  Rotary is stopped after linear is back home.


====================================================================================================
Version 0.1B6

14 may  2013

- Solved problem with pitch
====================================================================================================
Version 0.1B5

14 may  2013

- Removed all actuator orientation related items
- Implemented 4 actuator definition files
- Removed linear force constant from actuator definition file.
- Removed password clue from password dialog
- Changes were not cleared after save config to file.
- Added selectioon for TPI or mm at pitch setting.

====================================================================================================
Version 0.1B4

07 may  2013

- Messages from the master program intended for the GUI are preceeded by a ! character to
  allow easy filtering of messages.
- The "Show all" checkbox for the terminal window enabled again. The default if this checkbox is off.
- Changed permisablke torque field into a percentage. 100% = SQ22000
- Removed torque constant of the rotary from the actuator definition files
- Added Max SQ value for the rotary in actuator definition files
- Changed description UNDERSIZED/OVERSIZED accordng thread type.
- Implemented the permisable torque. Limits all SQ values of the rotary to this setting
- Implemented the teach function. Autoset landing heigt, position close to part, position limits
  linear SQ for softland and thread pickup and linear force for float in and out of the thread.
- Limit Pitch value to 0.1 mm

====================================================================================================

Version 0.1B3

27 april  2013

- Name of .stcc filename was not always saved between sessions.
- Completely changed the master program
- Pitch offset changed in pitch: This is the actual pitch of the thead to check in mm.
- Made the show all checkbox invisible (show all is allways on)

====================================================================================================
Version 0.1B2

29 march 2013

- Suppressed warning message that occurred on quitting he application with no file loaded and with
  no changes made to the settings.
- Serial port logging was stored in the wrong folder
- Defined output format for messages from the controller.
- Plot rotary error vs position implemented.
- Added check box to show all output from the controller
- Controller output filter implemented. Plot and other special messages are not shown in the terminal
  window unless the "Show all" checkbox is checked.
- Password protection for the setup tab implemented. The password is SMAC5807 and can not be changed.
- Application icon created. simple icon like Smac control center. Text is: SMAC TCC
- Installer created (with empty license agreement file)
- Run button implemented - Sends an Escape, waits for the prompt and then starts macro 0
- Disable user input in terminal window when not logged in.
- Stop button implemented - Sends an Escape and waits for the prompt and then sends an 0AB.MF
- Single Cycle button implemented - Sends an escape, waits for the prompt and then start macro 26
- Touch off button implemented - Sends an escape, waits for the prompt and then start macro 200
- Toolips implemented (Text still nonsense)
- Run screen: Added Threads clearance, Thread start location and Thread depth measured values
- Pictures: After the penguins and the candy for the eye more realistic pictures now. Getting close ...


====================================================================================================
Version 0.1B1

15 march 2013

- Solved bug in orientation combobox: Vertical with shaft down added.
- Added errormessage if Actuaters folder is empty
- Initial enabled state of input fields was not correct.
- Added "Save settings to file" and "Load settings from file" functionality
  The default file suffix for these files is .stcc which stands for SMAC Thread Check Center
  File format is standard .ini format.
- Added functionality of checkboxes "Check landing height", "Rotate in reverse" and "Check thread clearance"
  Disabling a checkbox will replace the first macro line in this function by a RC command and
  will delete the other macro lines in that command. A comment shows that the function is disabled.
- Added test that checks if input values are within the limits of the actuator.
- Application now remembers comport settings
- Application now remembers settings from last opened file
- File name shown in title bar
- Keep track of changed values and added warning on quitting application without saving
  and on loading new settings without saving.
- Prepared warning for run button without saving first
- Changed section names in Actuator definition files:
  [VerticalVithShaftUp] -> [Vertical with shaft up]
  [VerticalWithShaftDown] -> [Vertical with shaft down]
- Intelligent port open mechanism on application start:
  If no ports found: Warning message.
  If only 1 port found: open this port.
  If more ports found: open last opend port, if no last opened port: message to select a port
- Removed Connect/Discommect button. Connect/Disconnect simply opens and closes the COM port
- Removed diagnostics output window
- Cosmetic changes: Added some "Candy for the Eye"
====================================================================================================
PRERELEASE

8 March 2013

Version 0.1 PREVIEW

- First publication with very limited functionality

