### Main Functionality ###

GUI written in C# which provides a simple and straightforward way to quickly configure motion parameters of 
a variety of SMAC single/dual axis actuators and controllers. It provides a functionality for checking

Oversize or undersized threads
Cross threads
Thread depth
No threads
Mis-located threads
Thread pitch
Shallow or blocked hole
Minor diameter out of specthread
Major diameter out of spec
Pitch diameter out of spec
Pitch
Foreign objects (e.g. broken tap, swarf etc)
Double tapped thread
Thread depth
Missing threads
Cylindericity


Version 1.05
http://www.smac-mca.com

### Setup ###

Download and compile with Visual Studio 2015 (or newer)