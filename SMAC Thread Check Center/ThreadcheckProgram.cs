﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Windows.Forms;

namespace SMAC_Thread_Check_Center
{
    class ThreadcheckProgram
    {
        private List<string> theProgram = new List<string>(); // hold compiled LAC1 program
        public Variables variables = null; // PID variables to be set or left default

        // used to hold, move, and manipulate translated LAC1 code
        private List<string> translateFrom = new List<string>();
        private List<string> translateInto = new List<string>();

        // Stores master program for thread check in LAC1 code

        private string[] masterProgram = new string[]
        {
            ";",
            ";   I/O ALLOCATION",
            "; ==================================",
            "; OUTPUT 0: READY/RETRACTED/HOME",
            "; OUTPUT 1: PASS",
            "; OUTPUT 2: FAIL",
            "; OUTPUT 3: ERROR/ JAM",
            "; INPUT 0: START TEST CYLE",
            "; INPUT 1: NOT USED",
            "; INPUT 2: NOT USED",
            "; INPUT 3: RESET (In Case of Jam Condition)",
            ";",
            "; Register usage:",
            ";",
            "; 20: Min position of linear during reverse thread find",
            "; 21: Linear datum point. This is the position of the softland",
            "; 22: Linear 2 pitch above the softland position",
            "; 23: Linear 0.5 pitch above the softland position",
            "; 24: Linear 3 pitch below datum (point for push/pull test)",
            "; 25: Maximum time to rotate out of the thread",
            "; 26: Rotary position of next check",
            "; 27: Linear position of last check",
            "; 28: Fail output",
            "; 29: Result of push/pull test",
            "; 30: General purpose working register",
            "; 31: Linear position of required depth",
            "; 32: Time to do next rotate out of thread check",
            "; 33: Rotary position of last out of thread check",
            "; 34: Timer used for reporting following errors",
            ";",
            "0MF",
            "RM",
            "; **CLEAR SCREEN, CONFIGURE I/O CHANNELS******",
            "MD0,BR57600,MG\"!I0\"",
            "MD1,CH0,CH1,CH2,CH3,CF0,CF1,CF2,CF3,UM1,SS2",
            ";",
            "; **LOAD PID'S FOR AXIS 1 (Linear) & AXIS 2 (Rotary)",
            "MD2,0MF,PH0,1PM,1SG[P_LINEAR],SI[I_LINEAR],SD[D_LINEAR],IL[IL_LINEAR],FR[FR_LINEAR],RI[RI_LINEAR],OO[OO_LINEAR]",
            "MD3,2PM,SG[P_ROTARY],SI[I_ROTARY],SD[D_ROTARY],IL[IL_ROTARY],FR[FR_ROTARY],RI[RI_ROTARY],OO[OO_ROTARY]",
            ";",
            "; ****OPTIONAL Setting up Interrupt (input 3)********",
            "; **  This Disables the Linear and Rotary axis when input 3 is activated.",
            "MD4,AL9,LV3,EV3",
            ";",
            "; Set Servo phasing for rotary to 0 for right handed thread and to 3 for left handed",
            ";",
            "MD5,2PH[PHASING_ROTARY]",
            ";",
            "; Do homing for the linear and then continue",
            ";",
            "MD6,MC10,MJ15",
            ";",
            "; Endless loop that can only be exited by the interrupt routine",
            "; The interrupt routine is entered by pressing the reset button",
            ";",
            "MD8,NO,RP",
            ";",
            "; The interrupt routine",
            ";",
            "MD9,0MF,DV3,WF3,UM1,EV3,MJ0",
            ";",
            "; Homing routine for linear axis",
            ";",
            "MD10,1SV[VF_HOMING_LINEAR],SA[AF_HOMING_LINEAR],SQ[QF_HOMING_LINEAR],VM,MN,DI1,GO,WA250",
            "MD11,RW538,IB[HARDSTOP_THRESHOLD_LINEAR],DI0,JR2,RP,1FI,MJ12",
            "MD12,RL448,IC10,MJ13,NO,RW538,IG[INDEX_LIMIT_LINEAR],MJ14,NO,RP",
            "MD13,AB,WA5,PM,GH,MG\"LINEAR HOME\",RC",
            "MD14,MG\"14-NO INDEX\",0MF,CN3,MG\"CLEAR THREAD GAUGE, PUSH RESET INPUT\",MJ8",
            ";",
            "; No homing routine for rotary axis necessary",
            ";",
            "MD15,2DH,SV[V_HOMING_ROTARY],SA[A_HOMING_ROTARY],SQ[Q_HOMING_ROTARY],MF,MJ25",
            ";",
            "; Entry point for Single cycle command from user interface",
            ";",
            "MD24,2MF,MG\"!I1\",MG\"Single cycle\",MJ28",
            ";",
            "; Start of main loop",
            "; - Wait for start input (input0) to be OFF",
            "; - Wait for start input (input0) to be ON",
            "; - Then clear all used registers and start the overall timer",
            ";",
            "MD25,2MF,EV3,1SQ[Q_HOME_LINEAR],CN0",
            "MD26,IF0,JR3,NO,JR-3,IN0,MJ27,NO,JR-3",
            "MD27,MG\"!I1\",MG\"IP 0\",MJ28",
            "MD28,UM1,CF0,CF1,CF2,CF3",
            "MD29,AL0,AR20,AR21,AR22,AR23,AR24,AR25,AR26,AR27,AR28,AR29,AR30,AR31,WL1830,MJ30",
            ";",
            "; Move linear close to part",
            ";",
            "MD30,1PM,MN,1SQ[Q_MOVE_CLOSE_TO_PART_LINEAR],SV[V_MOVE_CLOSE_TO_PART_LINEAR],SA[A_MOVE_CLOSE_TO_PART_LINEAR],MA[POS_CLOSE_TO_PART_LINEAR],GO,WS50,MJ40",
            ";",
            "; Softland on part",
            ";",
            "MD40,1VM,MN,SQ[Q1_SOFTLAND_LINEAR],SV[V_SOFTLAND_LINEAR],SA[A_SOFTLAND_LINEAR],DI0,GO,WA100",
            "MD41,RW538,IG[PERR_SOFTLAND_LINEAR],MJ42,NO,RP",
            ";",
            "; Change to force mode, capture and report the datum (Format is Hnnnn)",
            "; Check if the datum is within limits",
            "; Calculate the position 2 pitch above the datum and 0.5 pitch above datum",
            "; Calculate the position for the push/pull test",
            "; Calculate the position for the minimum depth",
            ";",
            "MD42,1PM,SQ32000,QM0,MN,SQ[Q2_SOFTLAND_LINEAR],WA150,RL494,AR21,MG\"!H\":0",
            "MD43,RL494,IB[PART_POSITION_MIN_LINEAR],MJ45,NO,IG[PART_POSITION_MAX_LINEAR],MJ46,NO,MG\"HEIGHT OK\"",
            "MD44,RA21,AS[0.5_PITCH_LINEAR],AR24,AS[1.5_PITCH_LINEAR],AR23,RA21,AA[1.0_DEPTH_LINEAR],AR31,RA21,AA[3.0_PITCH_LINEAR],AR24,AL0,WL1830,MJ50",
            ";",
            "MD45,1PM,MN,GH,WS20,CN2,MG\"45-PART TOO HIGH\",MJ6",
            "MD46,1PM,MN,GH,WS20,CN2,MG\"46-PART TOO LOW/NO PART PRESENT\",MJ6",
            ";",
            "; Rotate in reverse to find first thread",
            "; During reverse rotation Reg20 holds the minimum value (peak detect) of the linear.",
            "; The start of thread is found when the linear is 0.2 pitch above the detected minimum",
            "; This process must be completed within 2.0 revolution, if not then \"no thread found\" error.",
            "; During search the following error of the rotary may not exceed 1000 encoder counts",
            ";",
            "MD50,RA21,AR20,2VM,2MN,2SV[V_REVERSE_FIND_ROTARY],SA[A_REVERSE_FIND_ROTARY],SQ[Q_REVERSE_FIND_ROTARY],DI1,GO,WA150,2DH",
            "MD51,MC169,IG[20.0_MAX_ROTARY_ERROR],MJ54,NO,RL638,IB-[2.0_REV_ROTARY],MJ53,NO,RL494,IB@20,AR20,RP,AS@20,IB[0.2_PITCH_LINEAR],RP,NO",
            "MD52,2PM,2DH,RL494,AR30,MG\"Threads Aligned\",MJ60",
            "MD53,MG\"53-No Threads, Misaligned\",MJ106",
            "MD54,MG\"54-Rotary Error,Clear Jam\",MJ106",
            ";",
            ";",
            "; Start rotation forward",
            "; Rotate 1.5 revolution into the thread and then check if the linear has moved at least",
            "; 1 full pitch inward",
            ";",
            "MD60,2VM,2DH,MN,DI0,MC150,GO,WA20,MC151,WA200",
            "MD61,MC169,IG[20.0_MAX_ROTARY_ERROR],MJ64,NO,RL638,IG[1.5_REV_ROTARY],MJ62,NO,RP",
            "MD62,RL494,AS@30,IG[1.0_PITCH_LINEAR],MJ70,NO,MJ63",
            ";  ",
            "MD63,MG\"63-NO THREADS/MISALIGNED\",MC91,MJ105",
            "MD64,MG\"64-ROTARY BLOCKED\",MC91,MJ105",
            ";",
            "; The thread has been picked up.",
            "; Now rotate further with low linear force until required depth is reached.",
            "; During the move the following error of the rotary and the increase of the linear are checked.",
            "; If the depth is to the desired depth to achieve (GOOD PART) jump to line 80",
            ";",
            "MD70,2VM,MN,MC152,DI0,GO,WA200,RL494,AR27,RL638,MC170,AA[1.0_REV_ROTARY],AR26,1SQ[Q_FLOAT_LINEAR],RL1830,AR34",
            "MD71,MC171,MC120,RL494,IG@31,2MF,MJ90,RP",
            "; ",
            "; Depth achieved: Next will be the push/pull test (if enabled)",
            ";",
            "; ",
            "MD90,MG\"DEPTH PASSED\",MC91,2AB,MC92,MJ95",
            ";",
            "; Report depth in the format !Dnnn",
            ";",
            "MD91,RL494,AS@21,MG\"!D\":0,2AB,RC",
            ";",
            "; Calculate watchdog to rotate out of the thread.",
            "; Only if watchdog is 0",
            "; Watchdog = 2 x RCLOCK + 3000 msec",
            "; Store watchdog in Reg 25",
            ";",
            "MD92,RA25,IU0,NO,RC,RL1830,AM2,AA3000,AR25,RC",
            ";",
            "; Initialize parameters for next rotate out of the thread check.",
            "; Reg32 (time to do next check) = now + 100 msec",
            "; Reg33 (rotary position of last check) = current rotary position",
            ";",
            "MD93,RL1830,AA100,AR32,RL638,AR33,RC",
            ";COMMENT1 (Filled in by the front end)",
            ";COMMENT2 (Filled in by the front end)",
            ";COMMENT3 (Filled in by the front end)",
            ";COMMENT4 (Filled in by the front end)",
            ";COMMENT5 (Filled in by the front end)",
            "MD95,1MF,RL494,2VM,DI1,MN,MC150,GO,MC151,2SQ[Q_BREAKOUT_ROTARY],WA[T_BREAKOUT_ROTARY],MC93,MC152",
            "MD96,MC121,RL494,IB@24,NO,JR2,RP,2SA[A_BRAKE_FOR_PUSH_PULL_TEST_ROTARY],2SQ[Q_BRAKE_FOR_PUSH_PULL_TEST_ROTARY],2PM,2ST,WS100",
            ";COMMENT6 (Filled in by the front end)",
            ";COMMENT7 (Filled in by the front end)",
            ";COMMENT8 (Filled in by the front end)",
            "MD97,1AB,WA1,1PM,MN,SQ[Q_FOR_PUSH_PULL_TEST_LINEAR],1QM0,SQ-[Q_FOR_PUSH_PULL_TEST_LINEAR],WA300,RL494,AR29",
            "MD98,1QM0,SQ[Q_FOR_PUSH_PULL_TEST_LINEAR],GO,WA300,RL494,AS@29,MC170,AR29,MG\"!P\":29",
            "MD99,RA29,IG[PUSH_PULL_LIMIT],MJ100,NO,MG\"Push-pull test OK\",MJ110",
            "MD100,MG\"100-Push-pull test out of limits\",MJ105",
            ";",
            "; Macro 105: Set error output and rotate out of thread",
            "; Macro 106: Set error output and move linear back home",
            ";",
            "MD105,MC180,MJ110",
            "MD106,MC180,,MJ113",
            ";",
            "; Macro 110: Rotate out of thread and then send linear back home",
            ";",
            "MD110,MC92,2AB,WA1,2MF,2VM,MN,DI1,MC150,GO,MC151,2SQ[Q_BREAKOUT_ROTARY],WA[T_BREAKOUT_ROTARY],MC152",
            "; Move to 1.0 pitch above datum point",
            "MD111,1PM,MN,SV[V_ROTATE_BACK_LINEAR],SQ[Q_FLOAT_LINEAR],MA@22,GO,MC93",
            ";",
            "; During the reverse move monitor if rotary is moving every 100 msec",
            "; Also check the overall timeout",
            "; Increase force of the linear during the last 1.5 revolution of the rotary",
            ";",
            "MD112,MC121,RL638,IB[1.5_REV_ROTARY],1SQ[Q2_SOFTLAND_LINEAR],NO,RL494,IB@23,NO,MJ113,RP",
            "MD113,1PM,MN,SV[V_MOVE_BACK_HOME_LINEAR],SA[A_MOVE_BACK_HOME_LINEAR],SQ[Q_MOVE_BACK_HOME_LINEAR],GH,WS200,2ST,WS100,RL494,IG[0.5_PITCH_LINEAR],NO,MJ114,MJ115",
            "MD114,MG\"114-LINEAR NOT RETRACTED\",CN2,CN3,MJ6",
            "MD115,RA28,IE0,CN1,NO,CN0,MG\"LINEAR RETRACTED\",RL1830,MG\"Cycle time is \":0:N,MG\" msec.\",MJ25",
            ";",
            "; Error Checks during rotate into thread:",
            "; - Check if position error of the rotary stays within the specified sensitivity(and stops ASAP when detected)",
            "; - Check after each revolution if the linear position has increased at least 80 % of pitch",
            ";",
            "MD120,RW682,MC170,IG[MAX_ROTARY_ERROR],0MF,MJ124,RL638,MC170,IB@26,RC,NO,RL494,AS@27,IB[0.8_PITCH_LINEAR],MJ125,NO,RA26,AA[1.0_REV_ROTARY],AR26,RL494,AR27,RC",
            ";",
            "; Error Checks during rotate out of thread:",
            "; - Check the overall watchdog for timeout",
            "; - Check every 100 msec if the rotary has moved since last check",
            ";",
            "MD121,RL1830,IG@25,NO,MJ126,IB@32,NO,RC,RL638,IB@33,MC93,RC,MJ220",
            "MD124,MG\"124-ROTARY BLOCKED\",MC91,MJ105",
            "MD125,MG\"125-Incorrect Thread Pitch\",MC91,MJ105",
            "MD126,MG\"126-SAFETY TIME OUT ERROR DURING REVERSE OUT OF THREAD\",MG\"PRESS RESET BUTTON\",0AB,MF,CN3,MJ8",
            "",
            "",
            ";",
            ";",
            "; ****Soft Start for the Rotary *****",
            "MD150,2SV[V_SOFT1_ROTARY],SA[A_SOFT1_ROTARY],SQ[Q_SOFT1_ROTARY],RC",
            "MD151,2SV[V_SOFT2_ROTARY],SA[A_SOFT2_ROTARY],SQ[Q_SOFT2_ROTARY],RC",
            "MD152,2SV[V_SOFT3_ROTARY],SA[A_SOFT3_ROTARY],SQ[Q_SOFT3_ROTARY],RC",
            "MD153,2SV[V_SOFT4_ROTARY],SA[A_SOFT4_ROTARY],SQ[Q_SOFT4_ROTARY],RC",
            ";",
            "; Macro 169: Get absolute value of rotary following error in accum",
            ";",
            "MD169,RW682,MJ170",
            ";",
            "; Calculate absolute value of ACC",
            "; Negative values are made positive by complementing and adding 1.",
            "; This is a faster operation than multiplying by -1",
            "; ",
            "MD170,IB0,AC,AA1,RC",
            ";",
            "; Report linear position and rotary error (separated by a comma)",
            "; Reports only once in 20 msec (timer is kept in reg 34)",
            ";",
            "MD171,RL1830,AS@34,IB0,RC,NO,RL1830,AA20,AR34,RL494,AS@21,MG\"!\":0:N,MG\",\":N,RW682,TR0,RC",
            ";",
            "; Set fail output and write 1 to reg 28",
            ";",
            "MD180,CN2,AL1,AR28,RC",
            ";",
            "; Macro 220 is executed when the rotary is blocked during a reverse move",
            "; Manual intervention of the operator is necessary.",
            "; Only way out is to press the reset button",
            ";",
            "MD220,0AB,MF,CN3",
            "MD221,MG\"221-Rotary blocked during reverse move\"",
            "MD222,MG\"Remove block and press reset button\",MJ8",
            ";",
            "; Routine to teach linear force at landing position:",
            ";",
            "; - Do a homing, wait 3 seconds and report linear force (format !Rnnnn)",
            "; - Softland on the part (without initial fast move)",
            "; - Do a position move to 2.0 pitch above softland position",
            "; - Report softland position (format !Lnnnn)",
            "; - After a wait of 1 second report THRO at this position (format !Qnnnn)",
            "; - Go back home",
            ";",
            "MD240,MC10,WA3000,RW530,MG\"!R\":0",
            "MD241,1PM,SQ30000,VM,MN,SQ[Q_SETUP_LINEAR],SV[V_SETUP_LINEAR],SA[A_SETUP_LINEAR],DI0,GO,WA100",
            "MD242,RW538,IG[PERR_SOFTLAND_LINEAR],MJ243,NO,RP",
            "MD243,RL494,MG\"!L\":0,AS[2.0_PITCH_LINEAR],AR30,2DI1,VM,MN,GO,1PM,MA@30,GO,WS1000,2ST,WS1000",
            "MD244,RW530,MG\"!Q\":0,1GH,WS100,MJ25"
            // ";",
            // ";",
            // "; Routine used for debugging only",
            // ";",
            // "MD250,MG\"Rotate Backward\",2AB,2VM,MN,DI1,MC150,GO,WA250,MC152"            
        };

        // Stores master program for thread check in LAC1 code
        private string[] masterProgram26 = new string[]
        {
            ";",
            ";   I/O ALLOCATION",
            "; ==================================",
            "; OUTPUT 0: READY/RETRACTED/HOME",
            "; OUTPUT 1: FAULT",
            "; OUTPUT 2: Used By controller ",
            "; OUTPUT 3: Used by controller",
            "; INPUT 0: START TEST CYLE",
            "; INPUT 1: NOT USED",
            "; INPUT 2: RESET (In Case of Jam Condition)",
            "; INPUT 3: Used by controller ",
            ";",
            "; Register usage:",
            ";",
            "; 20: Min position of linear during reverse thread find",
            "; 21: Linear datum point. This is the position of the softland",
            "; 22: Linear 2 pitch above the softland position",
            "; 23: Linear 0.5 pitch above the softland position",
            "; 24: Linear 3 pitch below datum (point for push/pull test)",
            "; 25: Maximum time to rotate out of the thread",
            "; 26: Rotary position of next check",
            "; 27: Linear position of last check",
            "; 28: Fail output",
            "; 29: Result of push/pull test",
            "; 30: General purpose working register",
            "; 31: Linear position of required depth",
            "; 32: Time to do next rotate out of thread check",
            "; 33: Rotary position of last out of thread check",
            "; 34: Timer used for reporting following errors",
            ";",
            "0MF",
            "RM",
            "SE16383",
            "; **CLEAR SCREEN, CONFIGURE I/O CHANNELS******",
            "MD0,WA5000,BR57600 ",
            "; ***wait added in to allow rotary motor phase to be found***",
            "MD1,CH0,CH1,CH2,CH3,CF0,CF1,UM1,SS2",
            ";",
            "; **LOAD PID'S FOR AXIS 1 (Linear) & AXIS 2 (Rotary)",
            "MD2,0MF,PH0,1PM,1SG[P_LINEAR],SI[I_LINEAR],SD[D_LINEAR],IL[IL_LINEAR],FR[FR_LINEAR],RI[RI_LINEAR],OO[OO_LINEAR]",
            "MD3,2PM,PH0,SG[P_ROTARY],SI[I_ROTARY],SD[D_ROTARY],IL[IL_ROTARY],FR[FR_ROTARY],RI[RI_ROTARY],OO[OO_ROTARY],MJ6",
            ";",
            "; Set Servo phasing for rotary to (0) for right handed thread and to (3)for left handed",
            ";",
            "; Do homing for the linear and then continue",
            ";",
            "MD6,MC10,MJ15",
            ";",
            "; Endless loop that can only be exited by the interrupt routine",
            "; The interrupt routine is entered by pressing the reset button",
            ";",
            "MD8,NO,RP",
            ";",
            "; The interrupt routine",
            ";",
            "MD9,0MF,DV2,WF2,UM1,EV2,MJ0",
            ";",
            "; Homing routine for linear axis",
            ";",
            "MD10,1SV[VF_HOMING_LINEAR],SA[AF_HOMING_LINEAR],SQ[QF_HOMING_LINEAR],VM,MN,DI1,GO,WA250",
            "MD11,RW538,IB[HARDSTOP_THRESHOLD_LINEAR],DI0,JR2,RP,1FI,MJ12",
            "MD12,RL448,IC10,MJ13,NO,RW538,IG[INDEX_LIMIT_LINEAR],MJ14,NO,RP",
            "MD13,AB,WA5,PM,GH,MG\"LINEAR HOME\",RC",
            "MD14,MG\"14-NO INDEX\",0MF,CN1,MG\"CLEAR THREAD GAUGE, PUSH RESET INPUT\",MJ8",
            ";",
            "; No homing routine for rotary axis necessary",
            ";",
            "MD15,2DH,SV[V_HOMING_ROTARY],SA[A_HOMING_ROTARY],SQ[Q_HOMING_ROTARY],MF,MJ25",
            ";",
            "; Entry point for Single cycle command from user interface",
            ";",
            "MD24,2MF,MG\"!I1\",MG\"Single cycle\",MJ28",
            ";",
            "; Start of main loop",
            "; - Wait for start input (input0) to be OFF",
            "; - Wait for start input (input0) to be ON",
            "; - Then clear all used registers and start the overall timer",
            ";",
            "MD25,2MF,EV2,1SQ[Q_HOME_LINEAR],CN0",
            "MD26,IF0,JR3,NO,JR-3,IN0,MJ28,NO,JR-3",
            "MD27,MG\"!I1\",MG\"IP 0\",MJ28",
            "MD28,UM1,CF0,CF1",
            "MD29,AL0,AR20,AR21,AR22,AR23,AR24,AR25,AR26,AR27,AR28,AR29,AR30,AR31,WL1830,MJ30",
            ";",
            "; Move linear close to part",
            ";",
            "MD30,1PM,MN,1SQ[Q_MOVE_CLOSE_TO_PART_LINEAR],SV[V_MOVE_CLOSE_TO_PART_LINEAR],SA[A_MOVE_CLOSE_TO_PART_LINEAR],MA[POS_CLOSE_TO_PART_LINEAR],GO,WS50,MJ40",
            ";",
            "; Softland on part",
            ";",
            "MD40,1VM,MN,SQ[Q1_SOFTLAND_LINEAR],SV[V_SOFTLAND_LINEAR],SA[A_SOFTLAND_LINEAR],DI0,GO,WA100",
            "MD41,RW538,IG[PERR_SOFTLAND_LINEAR],MJ42,NO,RP",
            ";",
            "; Change to force mode, capture and report the datum (Format is Hnnnn)",
            "; Check if the datum is within limits",
            "; Calculate the position 2 pitch above the datum and 0.5 pitch above datum",
            "; Calculate the position for the push/pull test",
            "; Calculate the position for the minimum depth",
            ";",
            "MD42,1PM,SQ32000,QM0,MN,SQ[Q2_SOFTLAND_LINEAR],WA150,RL494,AR21,MG\"!H\":0",
            "MD43,RL494,IB[PART_POSITION_MIN_LINEAR],MJ45,NO,IG[PART_POSITION_MAX_LINEAR],MJ46,NO,MG\"HEIGHT OK\"",
            "MD44,RA21,AS[0.5_PITCH_LINEAR],AR24,AS[1.5_PITCH_LINEAR],AR23,RA21,AA[1.0_DEPTH_LINEAR],AR31,RA21,AA[3.0_PITCH_LINEAR],AR24,AL0,WL1830,MJ50",
            ";",
            "MD45,1PM,MN,GH,WS20,CN1,MG\"45-PART TOO HIGH\",MJ6",
            "MD46,1PM,MN,GH,WS20,CN1,MG\"46-PART TOO LOW/NO PART PRESENT\",MJ6",
            ";",
            "; Rotate in reverse to find first thread",
            "; During reverse rotation Reg20 holds the minimum value (peak detect) of the linear.",
            "; The start of thread is found when the linear is 0.2 pitch above the detected minimum",
            "; This process must be completed within 2.0 revolution, if not then \"no thread found\" error.",
            "; During search the following error of the rotary may not exceed 1000 encoder counts",
            ";",
            "MD50,MJ60; Rotate in reverse to find first thread is disabled",
            ";",
            ";",
            "; Start rotation forward",
            "; Rotate 1.5 revolution into the thread and then check if the linear has moved at least",
            "; 1 full pitch inward",
            ";",
            "MD60,2VM,2DH,MN,DI0,MC150,GO,WA20,MC151,WA200",
            "MD61,MC169,IG[20.0_MAX_ROTARY_ERROR],MJ64,NO,RL638,IG[1.5_REV_ROTARY],MJ62,NO,RP",
            "MD62,RL494,AS@30,IG[1.0_PITCH_LINEAR],MJ70,NO,MJ63",
            ";",
            "MD63,MG\"63-NO THREADS/MISALIGNED\",MC91,MJ105",
            "MD64,MG\"64-ROTARY BLOCKED\",MC91,MJ105",
            ";",
            "; The thread has been picked up.",
            "; Now rotate further with low linear force until required depth is reached.",
            "; During the move the following error of the rotary and the increase of the linear are checked.",
            "; If the depth is to the desired depth to achieve (GOOD PART) jump to line 80",
            ";",
            "MD70,2VM,MN,MC152,DI0,GO,WA200,RL494,AR27,RL638,MC170,AA[1.0_REV_ROTARY],AR26,1SQ[Q_FLOAT_LINEAR],RL1830,AR34",
            "MD71,MC171,MC120,RL494,IG@31,2MF,MJ90,RP",
            ";",
            "; Depth achieved: Next will be the push/pull test (if enabled)",
            ";",
            ";",
            "MD90,MG\"DEPTH PASSED\",MC91,2AB,MC92,MJ95",
            ";",
            "; Report depth",
            ";",
            "MD91,RL494,AS@21,MG\"!D\":0,2AB,RC",
            ";",
            "; Calculate watchdog to rotate out of the thread.",
            "; Only if watchdog is 0",
            "; Watchdog = 2 x RCLOCK + 3000 msec",
            "; Store watchdog in Reg 25",
            ";",
            "MD92,RA25,IU0,NO,RC,RL1830,AM2,AA3000,AR25,RC",
            ";",
            "; Initialize parameters for next rotate out of the thread check.",
            "; Reg32 (time to do next check) = now + 100 msec",
            "; Reg33 (rotary position of last check) = current rotary position",
            ";",
            "MD93,RL1830,AA100,AR32,RL638,AR33,RC",
            ";",
            "; Push/pull test is disabled:",
            "; Rotate out of the thread",
            ";",
            "MD95,MJ110",
            ";",
            "; Macro 105: Set error output and rotate out of thread",
            "; Macro 106: Set error output and move linear back home",
            ";",
            "MD105,MC180,MJ110",
            "MD106,MC180,MJ113",
            ";",
            "; Macro 110: Rotate out of thread and then send linear back home",
            ";",
            "MD110,MC92,2AB,WA1,2MF,2VM,MN,DI1,MC150,GO,MC151,2SQ[Q_BREAKOUT_ROTARY],WA[T_BREAKOUT_ROTARY],MC152",
            "; Move to 1.0 pitch above datum point",
            "MD111,1PM,MN,SV[V_ROTATE_BACK_LINEAR],SQ[Q_FLOAT_LINEAR],MA@22,GO,MC93",
            ";",
            "; During the reverse move monitor if rotary is moving every 100 msec",
            "; Also check the overall timeout",
            "; Increase force of the linear during the last 1.5 revolution of the rotary",
            ";",
            "MD112,MC121,RL638,IB[1.5_REV_ROTARY],1SQ[Q2_SOFTLAND_LINEAR],NO,RL494,IB@23,NO,MJ113,RP",
            "MD113,1PM,MN,SV[V_MOVE_BACK_HOME_LINEAR],SA[A_MOVE_BACK_HOME_LINEAR],SQ[Q_MOVE_BACK_HOME_LINEAR],GH,WS200,2ST,WS100,RL494,IG[0.5_PITCH_LINEAR],NO,MJ114,MJ115",
            "MD114,MG\"114-LINEAR NOT RETRACTED\",CN2,CN3,MJ6",
            "MD115,RA28,IE0,CN1,NO,CN0,MG\"LINEAR RETRACTED\",RL1830,MG\"Cycle time is \":0:N,MG\" msec.\",MJ25",
            ";",
            "; Error Checks during rotate into thread:",
            "; - Check if position error of the rotary stays within the specified sensitivity(and stops ASAP when detected)",
            ";",
            ";",
            "MD120,RW682,MC170,IG[MAX_ROTARY_ERROR],0MF,MJ124,RC",
            ";",
            "; Error Checks during rotate out of thread:",
            "; - Check the overall watchdog for timeout",
            "; - Check every 100 msec if the rotary has moved since last check",
            ";",
            "MD121,RL1830,IG@25,NO,MJ126,IB@32,NO,RC,RL638,IB@33,MC93,RC,MJ220",
            "MD124,MG\"124-ROTARY BLOCKED\",MC91,MJ105",
            "MD125,MG\"125-Incorrect Thread Pitch\",MC91,MJ105",
            "MD126,MG\"126-SAFETY TIME OUT ERROR DURING REVERSE OUT OF THREAD\",MG\"PRESS RESET BUTTON\",0AB,MF,CN1,MJ8",
            ";",
            ";",
            "; ****Soft Start for the Rotary *****",
            "MD150,2SV[V_SOFT1_ROTARY],SA[A_SOFT1_ROTARY],SQ[Q_SOFT1_ROTARY],RC",
            "MD151,2SV[V_SOFT2_ROTARY],SA[A_SOFT2_ROTARY],SQ[Q_SOFT2_ROTARY],RC",
            "MD152,2SV[V_SOFT3_ROTARY],SA[A_SOFT3_ROTARY],SQ[Q_SOFT3_ROTARY],RC",
            "MD153,2SV[V_SOFT4_ROTARY],SA[A_SOFT4_ROTARY],SQ[Q_SOFT4_ROTARY],RC",
            ";",
            "; Macro 169: Get absolute value of rotary following error in accum",
            ";",
            "MD169,RW682,MJ170",
            ";",
            "; Calculate absolute value of ACC",
            "; Negative values are made positive by complementing and adding 1.",
            "; This is a faster operation than multiplying by -1",
            ";",
            "MD170,IB0,AC,AA1,RC",
            ";",
            "; Report linear position and rotary error (separated by a comma)",
            "; Reports only once in 20 msec (timer is kept in reg 34)",
            ";",
            "MD171,RL1830,AS@34,IB0,RC,NO,RL1830,AA20,AR34,RL494,AS@21,MG\"!\":0:N,MG\",\":N,RW682,TR0,RC",
            ";",
            "; Set fail output and write 1 to reg 28",
            ";",
            "MD180,CN1,AL1,AR28,RC",
            ";",
            "; Macro 220 is executed when the rotary is blocked during a reverse move",
            "; Manual intervention of the operator is necessary.",
            "; Only way out is to press the reset button",
            ";",
            "MD220,0AB,MF,CN1",
            "MD221,MG\"221-Rotary blocked during reverse move\"",
            "MD222,MG\"Remove block and press reset button\",MJ8",
            ";",
            "; Routine to teach linear force at landing position:",
            ";",
            "; - Do a homing, wait 3 seconds and report linear force ",
            "; - Softland on the part (without initial fast move)",
            "; - Do a position move to 2.0 pitch above softland position",
            "; - Report softland position",
            "; - After a wait of 1 second report THRO at this position ",
            "; - Go back home",
            ";",
            "MD240,MC10,WA3000,RW530,MG\"!R\":0",
            "MD241,1PM,SQ30000,VM,MN,SQ[Q_SETUP_LINEAR],SV[V_SETUP_LINEAR],SA[A_SETUP_LINEAR],DI0,GO,WA100",
            "MD242,RW538,IG[PERR_SOFTLAND_LINEAR],MJ243,NO,RP",
            "MD243,RL494,MG\"!L\":0,AS[2.0_PITCH_LINEAR],AR30,2DI1,VM,MN,GO,1PM,MA@30,GO,WS1000,2ST,WS1000",
            "MD244,RW530,MG\"!Q\":0,1GH,WS100,MJ25"
        };

        //
        // Summary: Gets count of lines of code in master program
        //
        public int CountRaw()
        {
            if (Globals.mainForm.cmbController.Text == "LAC-26")
            {
                return masterProgram26.Count();
            }
            else
            {
                return masterProgram.Count();
            }
        }

        //
        // Summary: Get untranslated code at a given line in the master program
        //
        public string GetRawString(int index)
        {
            if (Globals.mainForm.cmbController.Text == "LAC-26")
            {
                return masterProgram26[index];
            }
            else
            {
                return masterProgram[index];
            }
        }

        //
        // Summary: Gets count of lines of code in user built program
        // Only valid after a call to Create.
        //
        public int Count()
        {
            return theProgram.Count;
        }

        //
        // Summary: Get the line of LAC1 program code at the given index and return 
        //
        public string GetLine(int index) // Only valid after a call to Create
        {
            // Trace.Assert Checks a condition; if the condition is false, outputs message box showing call stack.
            // should be index > theProgram.Count?
            Trace.Assert(index < theProgram.Count, "Invalid index " + index.ToString());
            return theProgram[index];
        }

        //
        // Summary: open a message box displaying a given string
        //
        public void Show()
        {
            if (Globals.mainForm.cmbController.Text == "LAC-26")
            {
                foreach (string s in masterProgram26)
                {
                    MessageBox.Show(s);
                }
            }
            else
            {
                foreach (string s in masterProgram)
                {
                    MessageBox.Show(s);
                }
            }
        }

        //
        // Summary: Get global speed of the application and Apply the setting 
        // to all accelerations, all speeds and all times
        //
        public void SetSpeed(double factor)
        {
            for (int i = 0; i < variables.Count(); i++)
            {
                Variable var = variables.Get(i);
                if ((var.Name.IndexOf("A_") == 0) || (var.Name.IndexOf("V_") == 0))
                {
                    variables.Multiply(var.Name, factor);
                }
            }
        }

        //
        // Summary: Sets resolution of motion for rotary action of an electric motor
        // holds values for 25%, 100%, 150%, and 200% of the action
        //
        public void SetRotaryResolution(int encoderCounts)
        {
            // MessageBox.Show("SetRotaryResolution(" + encoderCounts.ToString() + ")");
            variables.SetValue("0.25_REV_ROTARY", (int)(encoderCounts * 0.25));
            variables.SetValue("1.0_REV_ROTARY", encoderCounts);
            variables.SetValue("1.5_REV_ROTARY", (int)(encoderCounts * 1.5));
            variables.SetValue("2.0_REV_ROTARY", encoderCounts * 2);
        }

        //
        // Summary: Sets 
        //
        public void SetRotarySensitivity(int encoderCounts)
        {
            // MessageBox.Show("SetRotarySensitivity(" + encoderCounts.ToString() + ")");
            variables.SetValue("MAX_ROTARY_ERROR", encoderCounts);
            variables.SetValue("20.0_MAX_ROTARY_ERROR", 20 * encoderCounts);
        }

        //
        // Summary: 
        //
        public void ResetTranslation()
        {
            translateFrom.Clear();
            translateInto.Clear();
        }

        //
        // Summary: 
        //
        public void Translate(string from, string into)
        {
            translateFrom.Add(from);
            translateInto.Add(into);
        }

        //
        // Summary: 
        //
        public void EnableRotateReverseFind(bool enable)
        {
            if (!enable)
            {
                Translate("MD50,", "MD50,MJ60; Rotate in reverse to find first thread is disabled");
                Translate("MD51,", "");
                Translate("MD52,", "");
                Translate("MD53,", "");
                Translate("MD54,", "");
            }
        }

        //
        // Summary: 
        //
        public void EnableHeightCheck(bool enable)
        {
            if (!enable)
            {
                Translate("MD43,", "MD43,MJ44; Part height check is disabled");
                Translate("MD45,", "");
                Translate("MD46,", "");
            }
        }

        //
        // Summary: 
        //
        public void EnableSecondaryPitchCheck(bool enable)
        {
            if (enable)
            {
                string name = "MAX_ROTARY_ERROR";
                int MRE = variables.GetValue(name);
                string changed = string.Format("MD120,RW682,MC170,IG{0},0MF,MJ124,RC", MRE);
                Translate("MD120,", changed);
            }
        }

        //
        // Summary: 
        //
        public void EnablePushPull(bool enabled, bool top)
        {
            if (!enabled)
            {
                Translate(";COMMENT1", ";");
                Translate(";COMMENT2", "; Push/pull test is disabled:");
                Translate(";COMMENT3", "; Rotate out of the thread");
                Translate(";COMMENT4", ";");
                Translate(";COMMENT5", "");
                Translate(";COMMENT6", "");
                Translate(";COMMENT7", "");
                Translate(";COMMENT8", "");

                Translate("MD95,", "MD95,MJ110");
                Translate("MD96,", "");
                Translate("MD97,", "");
                Translate("MD98,", "");
                Translate("MD99,", "");
                Translate("MD100,", "");
            }
            else
            {
                if (top)
                {
                    Translate(";COMMENT1", ";");
                    Translate(";COMMENT2", "; For the push/pull test:");
                    Translate(";COMMENT3", "; First rotate back to 3 pitches from landing point");
                    Translate(";COMMENT4", "; During move check rotary following error and check if linear keeps decreasing");
                    Translate(";COMMENT5", ";");
                    Translate(";COMMENT6", ";");
                    Translate(";COMMENT7", ";Rotary stopped 3 pitches from the landing point: Do the push/pull test now");
                    Translate(";COMMENT8", ";");

                }
                else    // It must be bottom then
                {
                    Translate(";COMMENT1", ";");
                    Translate(";COMMENT2", "; Push/pull test at bottom of thread");
                    Translate(";COMMENT3", "; Stop the rotary and do the push / pull test after stop");
                    Translate(";COMMENT4", ";");
                    Translate(";COMMENT5", "");
                    Translate(";COMMENT6", ";");
                    Translate(";COMMENT7", "; Rotary stopped at the bottom: Do the push/pull test now");
                    Translate(";COMMENT8", ";");
                    Translate("MD95", "MD95,2PM,2ST,WS100,MJ97");
                    Translate("MD96,", "");
                }
            }
        }

        //
        // Summary: 
        //
        public void SetRotaryDirection(string direction)
        {
            //
            // The master program is created for standard right handle thread.
            // In case of left handed thread all the output and encoder phasing
            // of the rotary axis must be reversed.
            //
            switch (direction)
            {
                case Globals.STR_THREAD_DIRECTION_RIGHT_HANDED:
                    variables.SetValue("PHASING_ROTARY", 0);        // Default phasing
                    break;
                case Globals.STR_THREAD_DIRECTION_LEFT_HANDED:
                    variables.SetValue("PHASING_ROTARY", 3);        // Both output and encoder reversed
                    break;

            }
        }

        //
        // Summary: 
        //
        public void SetThreadType(string threadtype)
        {
            //
            // The master program is created for standard right handle thread.
            // In case of left handed thread all the output and encoder phasing
            // of the rotary axis must be reversed.
            //
            switch (threadtype)
            {
                case Globals.STR_THREAD_TYPE_INTERNAL:
                    Translate("MD100", "MD100,MG\"Push-pull test out of limits: OVERSIZED Thread\",MJ105");
                    break;
                case Globals.STR_THREAD_TYPE_EXTERNAL:
                    Translate("MD100", "MD100,MG\"Push-pull test out of limits: UNDERIZED Thread\",MJ105");
                    break;

            }
        }

        //
        // Summary: 
        //
        private void LimitTorque(string variableName, int maxvalue)
        {
            if (variables.GetValue(variableName) > maxvalue)
            {
                variables.SetValue(variableName, maxvalue);
            }
        }

        //
        // Summary: 
        //
        public void SetPermisableTorque(int maxSQ)
        {
            LimitTorque("Q_HOMING_ROTARY", maxSQ);
            LimitTorque("Q_REVERSE_FIND_ROTARY", maxSQ);
            LimitTorque("Q_BRAKE_FOR_PUSH_PULL_TEST_ROTARY", maxSQ);
            LimitTorque("Q_SOFT1_ROTARY", maxSQ);
            LimitTorque("Q_SOFT2_ROTARY", maxSQ);
            LimitTorque("Q_SOFT3_ROTARY", maxSQ);
            LimitTorque("Q_SOFT4_ROTARY", maxSQ);
        }

        //
        // Summary: 
        //
        private int limitLinearSQ(int sq)
        {
            if (sq > 32000)
            {
                sq = 32000;
            }
            return sq;
        }

        //
        // Summary: 
        //
        public void setLinearForce(string teachedSQ)
        {
            if (teachedSQ != "")
            {
                int sq = int.Parse(teachedSQ);

                variables.SetValue("Q2_SOFTLAND_LINEAR", limitLinearSQ(sq + 5000));
                variables.SetValue("Q_FLOAT_LINEAR", limitLinearSQ(sq + 1000));
            }
        }

        //
        // Summary: 
        //
        public void setLinearForceHome(string teachedSQ)
        {
            if (teachedSQ != "")
            {
                int sq = int.Parse(teachedSQ) + 4000;
                variables.SetValue("Q_HOME_LINEAR", limitLinearSQ(sq + 4000));
            }
        }

        //
        // Summary: 
        //
        public void SetLinearPID(double P, double I, double D)
        {
            variables.Multiply("P_LINEAR", P / 100.0);
            variables.Multiply("I_LINEAR", I / 100.0);
            variables.Multiply("D_LINEAR", D / 100.0);
        }

        //
        // Summary: 
        //
        public void SetRotaryPID(double P, double I, double D)
        {
            variables.Multiply("P_ROTARY", P / 100.0);
            variables.Multiply("I_ROTARY", I / 100.0);
            variables.Multiply("D_ROTARY", D / 100.0);
        }

        //
        // Summary: 
        //
        public void SetPositionCloseToPart(int encoderCounts)
        {
            variables.SetValue("POS_CLOSE_TO_PART_LINEAR", encoderCounts);
        }

        //
        // Summary: 
        //
        public void SetLandedHeightMin(int encoderCounts)
        {
            variables.SetValue("PART_POSITION_MIN_LINEAR", encoderCounts);
        }

        //
        // Summary: 
        //
        public void SetLandedHeightMax(int encoderCounts)
        {
            variables.SetValue("PART_POSITION_MAX_LINEAR", encoderCounts);
        }

        //
        // Summary: 
        //
        public void SetPitch(int encoderCounts)
        {
            variables.SetValue("0.2_PITCH_LINEAR", (2 * encoderCounts + 5) / 10);
            variables.SetValue("0.5_PITCH_LINEAR", (5 * encoderCounts + 5) / 10);
            variables.SetValue("0.8_PITCH_LINEAR", (8 * encoderCounts + 5) / 10);
            variables.SetValue("1.0_PITCH_LINEAR", encoderCounts);
            variables.SetValue("1.5_PITCH_LINEAR", (3 * encoderCounts + 1) / 2);
            variables.SetValue("2.0_PITCH_LINEAR", 2 * encoderCounts);
            variables.SetValue("3.0_PITCH_LINEAR", 3 * encoderCounts);
        }

        //
        // Summary: 
        //
        public void SetThreadDepth(int encoderCounts)
        {
            variables.SetValue("1.0_DEPTH_LINEAR", encoderCounts);
        }

        //
        // Summary: 
        //
        public void SetPushPullLimit(int encoderCounts)
        {
            variables.SetValue("PUSH_PULL_LIMIT", encoderCounts);
        }

        //
        // Summary: 
        //
        public bool Create()
        {
            theProgram.Clear();

            if (Globals.mainForm.cmbController.Text == "LAC-26")
            {
                foreach (string s in masterProgram26)
                {
                    string compiled = s.Trim();
                    if (compiled.Length > 0)    // Do not copy empty lines
                    {
                        if (compiled[0] != ';')     // Do not substitute variables in comment lines
                        {
                            for (int i = 0; i < variables.Count(); i++)
                            {
                                Variable v = variables.Get(i);
                                string strTemp = "[" + v.Name + "]";
                                while (compiled.IndexOf(strTemp) > 0)
                                {
                                    compiled = compiled.Replace(strTemp, v.Value.ToString());
                                }
                            }

                            int index1 = compiled.IndexOf('['); // The first delimiter
                            int index2 = compiled.IndexOf(']', index1 + 1);

                            Trace.Assert(index1 == index2, "Unresolved variable in string " + compiled);

                        }

                        for (int i = 0; i < translateFrom.Count; i++)
                        {
                            if (compiled.IndexOf(translateFrom[i]) == 0)
                            {
                                compiled = translateInto[i];
                                break;
                            }
                        }

                        if (compiled.Trim() != "")
                        {
                            theProgram.Add(compiled);
                        }
                    }
                }
            }
            else
            {
                foreach (string s in masterProgram)
                {
                    string compiled = s.Trim();
                    if (compiled.Length > 0)    // Do not copy empty lines
                    {
                        if (compiled[0] != ';')     // Do not substitute variables in comment lines
                        {
                            for (int i = 0; i < variables.Count(); i++)
                            {
                                Variable v = variables.Get(i);
                                string strTemp = "[" + v.Name + "]";
                                while (compiled.IndexOf(strTemp) > 0)
                                {
                                    compiled = compiled.Replace(strTemp, v.Value.ToString());
                                }
                            }

                            int index1 = compiled.IndexOf('['); // The first delimiter
                            int index2 = compiled.IndexOf(']', index1 + 1);

                            Trace.Assert(index1 == index2, "Unresolved variable in string " + compiled);

                        }

                        for (int i = 0; i < translateFrom.Count; i++)
                        {
                            if (compiled.IndexOf(translateFrom[i]) == 0)
                            {
                                compiled = translateInto[i];
                                break;
                            }
                        }

                        if (compiled.Trim() != "")
                        {
                            theProgram.Add(compiled);
                        }
                    }
                }
            }

            return true;
        }
    }

    //
    // Summary: Default constants to be loaded if no actuator profile chosen
    //
    public static class Constants
    {
        public static string[] Defaults()
        {
            return new string[]
            {
                "P_LINEAR, 12",
                "I_LINEAR, 50",
                "D_LINEAR, 600",
                "IL_LINEAR, 8000",
                "FR_LINEAR, 1",
                "RI_LINEAR, 0",
                "OO_LINEAR, 0",
                "FR_ROTARY, 0",
                "RI_ROTARY, 0",
                "OO_ROTARY, 0",
                "P_ROTARY, 80",
                "I_ROTARY, 120",
                "D_ROTARY, 800",
                "IL_ROTARY, 10000",
                "VF_HOMING_LINEAR, 1500000",
                "AF_HOMING_LINEAR, 20000",
                "QF_HOMING_LINEAR, 20000",
                "Q_HOME_LINEAR, 20000",
                "HARDSTOP_THRESHOLD_LINEAR, -2500",
                "INDEX_LIMIT_LINEAR, 2500",
                "V_HOMING_ROTARY, 600000",
                "A_HOMING_ROTARY, 4000",
                "Q_HOMING_ROTARY, 15000",
                "Q_MOVE_CLOSE_TO_PART_LINEAR, 25000",
                "V_MOVE_CLOSE_TO_PART_LINEAR, 1250000",
                "A_MOVE_CLOSE_TO_PART_LINEAR, 20000",
                "Q1_SOFTLAND_LINEAR, 25000",
                "V_SOFTLAND_LINEAR, 500000",
                "A_SOFTLAND_LINEAR, 20000",
                "PERR_SOFTLAND_LINEAR, 2500",
                "Q2_SOFTLAND_LINEAR, 25000",
                "V_REVERSE_FIND_ROTARY, 1000000",
                "A_REVERSE_FIND_ROTARY, 50000",
                "Q_REVERSE_FIND_ROTARY, 15000",
                "A_BRAKE_FOR_PUSH_PULL_TEST_ROTARY, 3000",
                "Q_BRAKE_FOR_PUSH_PULL_TEST_ROTARY, 4000",
                "Q_FOR_PUSH_PULL_TEST_LINEAR, 32000",
                "V_ROTATE_BACK_LINEAR, 200000",
                "Q_ROTATE_BACK_LINEAR, 10000",
                "V_MOVE_BACK_HOME_LINEAR, 2000000",
                "A_MOVE_BACK_HOME_LINEAR, 30000",
                "Q_MOVE_BACK_HOME_LINEAR, 15000",
                "Q_BREAKOUT_ROTARY, 32000",
                "T_BREAKOUT_ROTARY, 400",
                "Q_FLOAT_LINEAR, 20000",
                "V_SOFT1_ROTARY, 50000",
                "A_SOFT1_ROTARY, 2000",
                "Q_SOFT1_ROTARY, 4000",
                "V_SOFT2_ROTARY, 1300000",
                "A_SOFT2_ROTARY, 60000",
                "Q_SOFT2_ROTARY, 22000",
                "V_SOFT3_ROTARY, 1000000",
                "A_SOFT3_ROTARY, 60000",
                "Q_SOFT3_ROTARY, 22000",
                "V_SOFT4_ROTARY, 2000000",
                "A_SOFT4_ROTARY, 60000",
                "Q_SOFT4_ROTARY, 22000",
                "Q_SETUP_LINEAR, 25000",
                "V_SETUP_LINEAR, 500000",
                "A_SETUP_LINEAR, 20000",
                //
                // The following values are filled in by the program
                //
                "PHASING_ROTARY, 0",
                "POS_CLOSE_TO_PART_LINEAR, 45000",
                "PART_POSITION_MIN_LINEAR, 46000",
                "PART_POSITION_MAX_LINEAR, 50000",
                "PUSH_PULL_LIMIT, 100",
                "0.25_REV_ROTARY, 7000",
                "MAX_ROTARY_ERROR, 7000",
                "20.0_MAX_ROTARY_ERROR, 140000",
                "1.0_REV_ROTARY, 28000",
                "1.5_REV_ROTARY, 42000",
                "2.0_REV_ROTARY, 56000",
                "0.2_PITCH_LINEAR, 200",
                "0.5_PITCH_LINEAR, 500",
                "0.8_PITCH_LINEAR, 800",
                "1.0_PITCH_LINEAR, 1000",
                "1.5_PITCH_LINEAR, 1500",
                "2.0_PITCH_LINEAR, 2000",
                "3.0_PITCH_LINEAR, 3000",
                "1.0_DEPTH_LINEAR, 7000"
            };
        }
    }

}
