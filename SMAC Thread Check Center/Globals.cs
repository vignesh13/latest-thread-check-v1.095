﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAC_Thread_Check_Center
{
    public static class Globals
    {
        public static MainForm mainForm;

        public const string STR_THREAD_DIRECTION_RIGHT_HANDED = "Right handed";
        public const string STR_THREAD_DIRECTION_LEFT_HANDED = "Left handed";

        public const string STR_THREAD_TYPE_INTERNAL = "Internal";
        public const string STR_THREAD_TYPE_EXTERNAL = "External";
    }
}
