﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO.Ports;
using System.Windows.Forms;
using System.Diagnostics;

namespace SMAC_Thread_Check_Center
{
    public class Serial
    {
        private SerialPort port = new SerialPort();
        private SerialDataReceivedEventHandler theHandler;
        private string buffer = "";
        private Logfile logFile = new Logfile();

        private static string prompt = "\r\n>";

        public Serial()     // The constructor
        {
            logFile.Filename = "Serial port logging.txt";
            logFile.Timestamp = true;
            logFile.Enabled = true;         // Wil start the logfile
        }
        

        //Connects to Serial Port based on Baud Rate and Port Name.  All other settings are static:
        //No handshaking, no parity, one stop bit, 500ms write timeout, 250ms read timeout, 8 data bits

        public void Open(string strPortname, int iBaudrate, SerialDataReceivedEventHandler handler)
        {
            try
            {
                logFile.Timestamp = true;
                logFile.Enabled = true;
                port.PortName = strPortname;
                port.ReceivedBytesThreshold = 1;
                port.BaudRate = iBaudrate;
                port.DataBits = 8;
                port.WriteTimeout = 500;
                port.ReadTimeout = 500;
                port.Handshake = System.IO.Ports.Handshake.None;
                port.Parity = Parity.None;
                port.StopBits = StopBits.One;
                port.NewLine = "\r";            // Must be forced to be a single CR character
//                MessageBox.Show(port.ReadBufferSize.ToString());
                if (!port.IsOpen)
                {
                    port.Open();
                    Thread.Sleep(100);  // Some delay

                    while (port.BytesToRead > 0)
                    {
                        port.ReadByte();
                    }
                    theHandler = new SerialDataReceivedEventHandler(handler);
                    port.DataReceived += theHandler;       // Add the received handler
                    buffer = "";
                }
                else
                {
                    MessageBox.Show("Port Already Opened");
                }
            }
            catch
            {
                MessageBox.Show(strPortname + " is already opened by another program." + Environment.NewLine + Environment.NewLine +
                    "Please close the program that uses this port and then try again.",
                    "Can not open serial port", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public bool Overflow()
        {
            return (buffer.Length > 500);
        }

        public bool CompleteLineReceived()
        {
            //
            // First copy all available bytes to the buffer
            //

            if (buffer.IndexOf('\r') > 0)
            {
                return true;
            }

            char c = ' ';

            while (port.IsOpen && (port.BytesToRead > 0) && (c != '\r'))
            {
                c = (char)port.ReadChar();
                buffer += c;
            }
            //            Globals.logSerial.Add(buffer+"\n");
            return (buffer.IndexOf('\r') > 0);
        }

        public string GetAllBytes()
        {
            string returnValue = "";
            if (port.BytesToRead > 0)
            {
                returnValue = buffer + port.ReadExisting();
            }
            buffer = "";
            return returnValue;
        }



        public string ReadLine()
        {
            string strTemp = "";
            if (port.IsOpen)
            {
                if (CompleteLineReceived())
                {
                    int i = buffer.IndexOf('\r');
                    strTemp = buffer.Substring(0, i);
                    buffer = buffer.Substring(i + 1);
                }
                else
                {
                    strTemp = buffer + port.ReadLine();
                    buffer = "";
                }
                strTemp = strTemp.Trim();       // TODO: check if this is necessary, seems that ReadLine() also returns the Newline Character
                logFile.Add("< " + strTemp + "\n");
            }
            return strTemp;
        }

        public void WriteString(string strStringToWrite)
        {
            if (port.IsOpen)
            {
                logFile.Add("> " + strStringToWrite + "\n");
                port.Write(strStringToWrite);
            }
         
        }

        public void DisableHandler()
        {
            port.DataReceived -= theHandler;
        }

        public void EnableHandler()
        {
            port.DataReceived += theHandler;
        }


        private bool WaitForPrompt()
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            while (stopwatch.ElapsedMilliseconds < 500)     // Wait 500 msec as maximum
            {
                if (port.BytesToRead > 0)
                {
                    Globals.mainForm.txtTerminal.AppendText(port.ReadExisting());
                    int lastindex = Globals.mainForm.txtTerminal.Text.Length - 1;
                    if ((lastindex >= 2) && (Globals.mainForm.txtTerminal.Text.Substring(lastindex - 2) == prompt))
                    {
                        return true;
                    }
                }
            }
            MessageBox.Show("No response from the controller" + Environment.NewLine + Environment.NewLine +
                "Make sure that:" + Environment.NewLine + Environment.NewLine +
                "- The right COM port is selected." + Environment.NewLine +
                "- The serial cable is connected to the controller." + Environment.NewLine +
                "- The controller is powered.",
                "Comminication error",
                MessageBoxButtons.OK,
                MessageBoxIcon.Error);
            return false;
        }


        public bool WriteStringAndWaitForPrompt(string s)
        {
            port.Write(s);
            return WaitForPrompt();
        }


        public bool AbortAndWaitForPrompt()
        {
            return WriteStringAndWaitForPrompt("\u001b");       // Write an escape character
        }


        public bool IsOpen()
        {
            return port.IsOpen;
        }


        public void Close()
        {
            if (port.IsOpen)
            {

                port.DataReceived -= theHandler;    // Get rid of the handler
                theHandler = null;
                port.Close();
                Thread.Sleep(100);  // Wait some time
            }
        }
    }
}
