﻿using System;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace SMAC_Thread_Check_Center
{
    public class Serial
    {
        // This is the old default baudrate on entering 9600 baud mode
        private int oldBaud = 9600;
        // bool to show connection message
        private bool silentmode = false;
        //store serial port resource.
        private SerialPort port = new SerialPort();
        // Handle the DataReceived event of a SerialPort object
        private SerialDataReceivedEventHandler theHandler;
        private string buffer = "";
        private Logfile logFile = new Logfile();
        private static string prompt = "\r\n>"; // newline character


        public Serial()     // The constructor
        {
            logFile.Filename = "Serial port logging.txt";
            logFile.Timestamp = true;       // write timestamp
            logFile.Enabled = true;         // Will start the logfile
        }

        //
        // Connects to Serial Port based on Baud Rate and Port Name. All other settings are static:
        // No handshaking, no parity, one stop bit, 500ms write timeout, 250ms read timeout, 8 data bits
        //
        public void Open(string strPortname, int iBaudrate, SerialDataReceivedEventHandler handler)
        {
            try
            {
                logFile.Timestamp = true;
                logFile.Enabled = true;
                port.PortName = strPortname;
                port.ReceivedBytesThreshold = 1;
                port.BaudRate = iBaudrate;
                port.DataBits = 8;
                port.WriteTimeout = 500;
                port.ReadTimeout = 500;
                port.Handshake = System.IO.Ports.Handshake.None;
                port.Parity = Parity.None;
                port.StopBits = StopBits.One;
                port.NewLine = "\r"; // Must be forced to be a single CR character
                // MessageBox.Show(port.ReadBufferSize.ToString());

                // If port is not open
                if (!port.IsOpen)
                {
                    // display message on GUI for connection
                    Globals.mainForm.lblStatus.Text = "Connecting ...";
                    Globals.mainForm.lblStatus.BackColor = System.Drawing.Color.Yellow;
                    Globals.mainForm.lblStatus.ForeColor = System.Drawing.Color.Black;
                    Globals.mainForm.Refresh();

                    port.Open();         // try and open port
                    Thread.Sleep(100);   // Some delay
                    port.ReadExisting(); // reads available bytes

                    // set handler and clear buffer of first read data
                    if (SetBaudrate(iBaudrate))
                    {
                        theHandler = new SerialDataReceivedEventHandler(handler);
                        port.DataReceived += theHandler; // Add the received handler
                        // AbortAndWaitForPrompt();
                        buffer = "";
                    }
                    // send error message pop up is connection failed
                    else
                    {
                        MessageBox.Show
                            ("No controller found on " + strPortname + Environment.NewLine +
                             "Please check the following:" + Environment.NewLine +
                             "- The serial cable is connected." + Environment.NewLine +
                             "- The controller is powered." + Environment.NewLine,
                             "No controller",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                        port.Close();
                    }
                }
                // error message trying to connect with a port that is already open
                else
                {
                    MessageBox.Show("Port Already Opened");
                }
            }
            // error message if port open failed which occurs if port is already busy
            catch
            {
                MessageBox.Show(strPortname + " is already opened by another program." + Environment.NewLine + Environment.NewLine +
                    "Please close the program that uses this port and then try again.",
                    "Can not open serial port", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            // display message on GUI for connection
            Globals.mainForm.lblStatus.ForeColor = System.Drawing.Color.White;
            if (port.IsOpen)
            {
                Globals.mainForm.lblStatus.Text = "Connected";
                Globals.mainForm.lblStatus.BackColor = System.Drawing.Color.Green;
            }
            else
            {
                Globals.mainForm.lblStatus.Text = "Not connected";
                Globals.mainForm.lblStatus.BackColor = System.Drawing.Color.Red;
            }
        }

        //
        // Summary: grab all data coming from port including garbage first read bytes
        //
        public string GetAllBytes()
        {
            string returnValue = "";
            // if there is something then grab buffer and first ready bytes
            if (port.BytesToRead > 0)
            {
                returnValue = buffer + port.ReadExisting();
            }
            buffer = "";
            return returnValue;
        }

        //
        // Summary: grab all data coming from port including garbage first read bytes
        //
        public bool SetBaudrate(int baudrate)
        {
            // list holds baudrates to try for connection
            int[] baudrates = new int[4] { 9600, 19200, 38400, 57600 };
            // use to store silent mode setting while checking baudrates
            bool oldSilentmode = silentmode;
            silentmode = true;
            // Check first bytes out for each baudrate
            for (int i = 0; i < baudrates.Count(); i++)
            {
                port.BaudRate = baudrates[i];
                Thread.Sleep(100);
                if (AbortAndWaitForPrompt())
                {
                    Thread.Sleep(100);
                    WriteString("BR" + baudrate.ToString() + "\r");
                    Thread.Sleep(100);
                    port.BaudRate = baudrate;
                    Thread.Sleep(100);
                    port.ReadExisting();
                    silentmode = oldSilentmode;
                    return true;
                }
            }
            silentmode = oldSilentmode;
            return false;
        }

        //
        // Summary: check bytes out using baudrate 9600 if input is true (on)
        // or check using old baudrate in input is false (!on = off)
        //
        public void Mode9600(bool on)
        {
            // store old baudrate, log new one, set and test 9600
            if (on)
            {
                oldBaud = port.BaudRate;
                WriteString("BR9600\r");
                Thread.Sleep(100);
                port.BaudRate = 9600;
                port.ReadExisting();
            }
            // log and test old baudrate
            else
            {
                WriteString("BR" + oldBaud.ToString() + "\r");
                Thread.Sleep(2000);
                port.BaudRate = oldBaud;
                port.ReadExisting();
            }
        }

        //
        // Summary: When called it writes string to logfile and sends string to port 
        //
        public void WriteString(string strStringToWrite)
        {
            if (port.IsOpen)
            {
                logFile.Add("> " + strStringToWrite + "\n");
                port.Write(strStringToWrite);
            }
        }

        //
        // Summary: Removes handler from data received turning handler off
        //
        public void DisableHandler()
        {
            port.DataReceived -= theHandler;
        }

        //
        // Summary: Adds handler to data received turning handler on
        //
        public void EnableHandler()
        {
            port.DataReceived += theHandler;
        }

        //
        // Summary: Adds a 500 millisecond wait 
        // usually for data transmission or port connections
        //
        public bool WaitForPrompt()
        {
            return WaitForPrompt(500);
        }

        //
        // Summary: 
        //
        public bool WaitForPrompt(int msec)
        {
            // create new timer and start it.
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            // continue until allotted time runs out
            while (stopwatch.ElapsedMilliseconds < msec)
            {
                // if there are bytes then we have a connection 
                if (port.BytesToRead > 0)
                {
                    // set bytes read off port as global for main form terminal
                    Globals.mainForm.txtTerminal.AppendText(port.ReadExisting());
                    // get index of last command line
                    int lastindex = Globals.mainForm.txtTerminal.Text.Length - 1;
                    // check if main form terminal is connected to controller 
                    if ((lastindex >= 2) && (Globals.mainForm.txtTerminal.Text.Substring(lastindex - 2) == prompt))
                    {
                        return true;
                    }
                }
            }
            // failed to connect in set time, display error message is silent mode is off
            if (!silentmode)
            {
                MessageBox.Show("No response from the controller" + Environment.NewLine + Environment.NewLine +
                    "Make sure that:" + Environment.NewLine + Environment.NewLine +
                    "- The right COM port is selected." + Environment.NewLine +
                    "- The serial cable is connected to the controller." + Environment.NewLine +
                    "- The controller is powered.",
                    "Communication error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            return false;
        }

        //
        // Summary: If we have a connection then write string and wait, else return false
        //
        public bool WriteStringAndWaitForPrompt(string s)
        {
            if (port.IsOpen)
            {
                port.Write(s);
                return WaitForPrompt();
            }
            else
            {
                return false;
            }
        }

        //
        // Summary: Writes escape char to port closing connection
        //
        public bool AbortAndWaitForPrompt()
        {
            // Write an escape character
            return WriteStringAndWaitForPrompt("\u001b");
        }

        //
        // Summary: Check if port is open
        // true if the serial port is open; otherwise, false. The default is false.
        //
        public bool IsOpen()
        {
            return port.IsOpen;
        }

        //
        // Summary: remove and delete handler, then close port
        //
        public void Close()
        {
            if (port.IsOpen)
            {
                port.DataReceived -= theHandler;
                theHandler = null;
                port.Close();
                Thread.Sleep(100);
            }
        }
    }
}
