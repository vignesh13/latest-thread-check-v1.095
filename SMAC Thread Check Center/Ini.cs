﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;

// Code from: // http://www.codeproject.com/Articles/1966/An-INI-file-handling-class-using-C
// 
// Using the class
// Steps to use the Ini class:
//
// 1. In your project namespace definition add  
// using INI;
// 2. Create a IniFile like this
// IniFile ini = new IniFile("C:\\test.ini");
// 3. Use IniWriteValue to write a new value to a specific key in a section or use IniReadValue to read a value FROM a key in a specific Section.
// That's all. It's very easy in C# to include API functions in your class(es).



namespace SMAC_Thread_Check_Center
{
    /// <summary>
    /// Create a New INI file to store or load data
    /// </summary>
    public class IniFile
    {
        public string path; // path to selected file

        // Used to call any exported function in a native Windows DLL
        [DllImport("kernel32")]

        // Declare methods that you want to access such as write string,
        // get string, and get selection
        private static extern long WritePrivateProfileString(string section,
            string key, string val, string filePath);

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section,
            string key, string def, StringBuilder retVal,
            int size, string filePath);

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileSection(
                string section,
                StringBuilder retVal,
                int size,
                string filePath);

        // INIFile Constructor.
        public IniFile(string IniPath)
        {
            path = IniPath;
        }

        // Write Data to the INI File, for string, doubles, and ints
        public void WriteValue(string Section, string Key, string Value)
        {
            WritePrivateProfileString(Section, Key, Value, this.path);
        }

        public void WriteDoubleValue(string Section, string Key, double Value)
        {
            WritePrivateProfileString(Section, Key, Utils.neutralDouble(Value), this.path);
        }

        public void WriteIntValue(string Section, string Key, int Value)
        {
            WritePrivateProfileString(Section, Key, Value.ToString(), this.path);
        }

        // Read string Data Value From the Ini File
        public string ReadValue(string Section, string Key)
        {
            // create buffer of char length 255
            // optimized with high rewrite string builder class
            StringBuilder temp = new StringBuilder(255);

            // get string length 255 from ini file at selection
            int i = GetPrivateProfileString(Section, Key, "", temp, 255, this.path);
            string retval = temp.ToString();

            // Strip off a comment
            int comment = retval.IndexOf(';');
            if (comment >= 0)       // Strip off a comment
            {
                retval = retval.Substring(0, comment);
            }
            return retval.Trim();
        }

        // Read section keynames From the Ini File
        public string[] ReadSectionKeynames(string Section)
        {
            List<string> retval = new List<string>();
            bool bFoundSection = false; // check if selection found
            foreach (string s in File.ReadAllLines(path))
            {
                string temp = s.Trim();
                if (!bFoundSection)
                {
                    // temp index value will be 0 if selection not found 
                    bFoundSection = temp.IndexOf("[" + Section + "]") == 0;
                }
                else
                {
                    // Go to start of the next section
                    if (temp.IndexOf('[') == 0)
                    {
                        break;
                    }

                    // add values before the equal sign which are keyname values
                    if ((temp.Length > 0) && (temp[0] != ';') && (temp.IndexOf('=') > 0))
                    {
                        retval.Add(temp.Substring(0, temp.IndexOf('=')).Trim());
                    }
                }
            }
            return retval.ToArray();
        }

        // Read double Data Value From the Ini File
        public double ReadDoubleValue(string Section, string Key)
        {
            // grab selected string and initialize return value
            string strTemp = ReadValue(Section, Key);
            double retval = 0;

            // ToString might convert . to , depending on region, runs check
            double dummy = 0.5;
            string localSeparator = ".";
            if (dummy.ToString().Contains(","))
            {
                localSeparator = ",";
            }

            // try to convert string to double accounting for region conversions
            if (!double.TryParse(strTemp.Replace(".", localSeparator), out retval))
            {
                // pop pup message box if it fails
                MessageBox.Show("File : " + path + Environment.NewLine +
                                "Section : " + Section + Environment.NewLine +
                                "Key : " + Key + Environment.NewLine +
                                "Has in incorrect value : " + strTemp);
            }

            return retval;
        }

        // Read int Data Value From the Ini File
        public int ReadIntValue(string Section, string Key)
        {
            // grab selected string and initialize return value
            string strTemp = ReadValue(Section, Key);
            int retval = 0;

            // try to convert string to int
            if (!int.TryParse(strTemp, out retval))
            {
                // pop pup message box if it fails
                MessageBox.Show("File : " + path + Environment.NewLine +
                                "Section : " + Section + Environment.NewLine +
                                "Key : " + Key + Environment.NewLine +
                                "Has in incorrect value : " + strTemp);
            }
            return retval;
        }

        // Read boolean Data Value From the Ini File
        public bool ReadBoolValue(string Section, string Key)
        {
            // grab selected string and initialize return value
            string strTemp = ReadValue(Section, Key);
            bool retval = false;

            // try to convert string to bool
            if (!bool.TryParse(strTemp, out retval))
            {
                // pop pup message box if it fails
                MessageBox.Show("File : " + path + Environment.NewLine +
                                "Section : " + Section + Environment.NewLine +
                                "Key : " + Key + Environment.NewLine +
                                "Has in incorrect value : " + strTemp);
            }
            return retval;
        }
    }
}
