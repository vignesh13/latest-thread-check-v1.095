﻿using System.Windows.Forms;


namespace SMAC_Thread_Check_Center
{
    class MySettings
    {
        // Setting keynames for ini files
        private const string SECTION_SETTINGS = "Settings";
        private const string KEY_ACTUATOR = "Actuator";
        private const string KEY_ORIENTATION = "Orientation";
        private const string KEY_THREAD_DIRECTION = "Thread direction";
        private const string KEY_THREAD_TYPE = "Thread type";
        private const string KEY_POSITION_CLOSE_TO_PART = "Position close to part";
        private const string KEY_CHECK_LANDED_HEIGHT = "Check landed height";
        private const string KEY_LANDED_HEIGHT_MIN = "Landed height min";
        private const string KEY_LANDED_HEIGHT_MAX = "Landed height max";
        private const string KEY_ROTATE_IN_REVERSE = "Rotate in reverse";
        private const string KEY_PITCH = "Pitch";
        private const string KEY_PITCH_UNITS = "Pitch units";
        private const string KEY_THREAD_DEPTH = "Thread depth";
        private const string KEY_CHECK_THREAD_CLEARANCE = "Check thread clearance";
        private const string KEY_CHECK_THREAD_CLEARANCE_AT_TOP = "Check thread clearance at top";
        private const string KEY_CHECK_THREAD_CLEARANCE_AT_BOTTOM = "Check thread clearance at bottom";
        private const string KEY_THREAD_CLEARANCE = "Thread clearance";
        private const string KEY_PERMISABLE_TORQUE = "Permissible torque";
        private const string KEY_TUNING_LINEAR_P = "Tuning linear P";
        private const string KEY_TUNING_LINEAR_I = "Tuning linear I";
        private const string KEY_TUNING_LINEAR_D = "Tuning linear D";
        private const string KEY_TUNING_ROTARY_P = "Tuning rotary P";
        private const string KEY_TUNING_ROTARY_I = "Tuning rotary I";
        private const string KEY_TUNING_ROTARY_D = "Tuning rotary D";
        private const string KEY_SPEED = "Speed";
        private const string KEY_SENSITIVITY = "Sensitivity";
        private const string KEY_TEACHED_SQ_LINEAR = "Teached SQ Linear";
        private const string KEY_TEACHED_SQ_LINEAR_HOME = "Teached SQ Linear Home";
        private const string KEY_TEACHED_HEIGHT = "Teached height";

        // The constructor
        public MySettings()
        {
        }

        // Try and set global values based on keyname values in ini file
        public void Read(string path)
        {
            IniFile inifile = new IniFile(path);
            try
            {
                Globals.mainForm.cmbActuator.SelectedItem = inifile.ReadValue(SECTION_SETTINGS, KEY_ACTUATOR);
                Globals.mainForm.cmbThreadDirection.SelectedItem = inifile.ReadValue(SECTION_SETTINGS, KEY_THREAD_DIRECTION);
                Globals.mainForm.cmbThreadType.SelectedItem = inifile.ReadValue(SECTION_SETTINGS, KEY_THREAD_TYPE);
                Globals.mainForm.chkCheckThreadClearance.Checked = inifile.ReadBoolValue(SECTION_SETTINGS, KEY_CHECK_THREAD_CLEARANCE);
                Globals.mainForm.rbnTop.Checked = inifile.ReadBoolValue(SECTION_SETTINGS, KEY_CHECK_THREAD_CLEARANCE_AT_TOP);
                Globals.mainForm.rbnBottom.Checked = inifile.ReadBoolValue(SECTION_SETTINGS, KEY_CHECK_THREAD_CLEARANCE_AT_BOTTOM);
                Globals.mainForm.ibdPositionCloseToPart.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_POSITION_CLOSE_TO_PART);
                Globals.mainForm.chkCheckLandedHeight.Checked = inifile.ReadBoolValue(SECTION_SETTINGS, KEY_CHECK_LANDED_HEIGHT);
                Globals.mainForm.ibdPartLocationMin.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_LANDED_HEIGHT_MIN);
                Globals.mainForm.ibdPartLocationMax.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_LANDED_HEIGHT_MAX);
                Globals.mainForm.chkRotateReverse.Checked = inifile.ReadBoolValue(SECTION_SETTINGS, KEY_ROTATE_IN_REVERSE);
                Globals.mainForm.ibdPitch.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_PITCH);
                Globals.mainForm.cmbPitchUnits.SelectedItem = inifile.ReadValue(SECTION_SETTINGS, KEY_PITCH_UNITS);
                Globals.mainForm.ibdThreadDepth.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_THREAD_DEPTH);
                Globals.mainForm.ibdThreadClearance.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_THREAD_CLEARANCE);
                Globals.mainForm.ibdMaxTorque.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_PERMISABLE_TORQUE);
                Globals.mainForm.ibdTuningLinearP.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_TUNING_LINEAR_P);
                Globals.mainForm.ibdTuningLinearI.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_TUNING_LINEAR_I);
                Globals.mainForm.ibdTuningLinearD.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_TUNING_LINEAR_D);
                Globals.mainForm.ibdTuningRotaryP.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_TUNING_ROTARY_P);
                Globals.mainForm.ibdTuningRotaryI.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_TUNING_ROTARY_I);
                Globals.mainForm.ibdTuningRotaryD.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_TUNING_ROTARY_D);
                Globals.mainForm.ibdSpeed.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_SPEED);
                Globals.mainForm.ibiRotarySensitivity.Intvalue = inifile.ReadIntValue(SECTION_SETTINGS, KEY_SENSITIVITY);
                Globals.mainForm.txtTeachedSqLinear.Text = inifile.ReadValue(SECTION_SETTINGS, KEY_TEACHED_SQ_LINEAR);
                Globals.mainForm.txtTeachedSqLinearHome.Text = inifile.ReadValue(SECTION_SETTINGS, KEY_TEACHED_SQ_LINEAR_HOME);
                Globals.mainForm.txtTeachedHeight.Text = inifile.ReadValue(SECTION_SETTINGS, KEY_TEACHED_HEIGHT);
            }
            catch
            {
                MessageBox.Show("Error reading settings file " + path, "File I/O error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        // Try to write global values to keyname values in ini file
        public void Write(string path)
        {
            IniFile inifile = new IniFile(path);
            try
            {
                inifile.WriteValue(SECTION_SETTINGS, KEY_ACTUATOR, Globals.mainForm.cmbActuator.SelectedItem.ToString());
                inifile.WriteValue(SECTION_SETTINGS, KEY_THREAD_DIRECTION, Globals.mainForm.cmbThreadDirection.SelectedItem.ToString());
                inifile.WriteValue(SECTION_SETTINGS, KEY_THREAD_TYPE, Globals.mainForm.cmbThreadType.SelectedItem.ToString());
                inifile.WriteValue(SECTION_SETTINGS, KEY_CHECK_THREAD_CLEARANCE, Globals.mainForm.chkCheckThreadClearance.Checked.ToString());
                inifile.WriteValue(SECTION_SETTINGS, KEY_CHECK_THREAD_CLEARANCE_AT_TOP, Globals.mainForm.rbnTop.Checked.ToString());
                inifile.WriteValue(SECTION_SETTINGS, KEY_CHECK_THREAD_CLEARANCE_AT_BOTTOM, Globals.mainForm.rbnBottom.Checked.ToString());
                inifile.WriteValue(SECTION_SETTINGS, KEY_POSITION_CLOSE_TO_PART, Globals.mainForm.ibdPositionCloseToPart.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_CHECK_LANDED_HEIGHT, Globals.mainForm.chkCheckLandedHeight.Checked.ToString());
                inifile.WriteValue(SECTION_SETTINGS, KEY_LANDED_HEIGHT_MIN, Globals.mainForm.ibdPartLocationMin.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_LANDED_HEIGHT_MAX, Globals.mainForm.ibdPartLocationMax.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_ROTATE_IN_REVERSE, Globals.mainForm.chkRotateReverse.Checked.ToString());
                inifile.WriteValue(SECTION_SETTINGS, KEY_PITCH, Globals.mainForm.ibdPitch.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_PITCH_UNITS, Globals.mainForm.cmbPitchUnits.SelectedItem.ToString());
                inifile.WriteValue(SECTION_SETTINGS, KEY_THREAD_DEPTH, Globals.mainForm.ibdThreadDepth.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_THREAD_CLEARANCE, Globals.mainForm.ibdThreadClearance.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_PERMISABLE_TORQUE, Globals.mainForm.ibdMaxTorque.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_TUNING_LINEAR_P, Globals.mainForm.ibdTuningLinearP.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_TUNING_LINEAR_I, Globals.mainForm.ibdTuningLinearI.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_TUNING_LINEAR_D, Globals.mainForm.ibdTuningLinearD.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_TUNING_ROTARY_P, Globals.mainForm.ibdTuningRotaryP.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_TUNING_ROTARY_I, Globals.mainForm.ibdTuningRotaryI.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_TUNING_ROTARY_D, Globals.mainForm.ibdTuningRotaryD.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_SPEED, Globals.mainForm.ibdSpeed.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_SENSITIVITY, Globals.mainForm.ibiRotarySensitivity.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_TEACHED_SQ_LINEAR, Globals.mainForm.txtTeachedSqLinear.Text);
                inifile.WriteValue(SECTION_SETTINGS, KEY_TEACHED_SQ_LINEAR_HOME, Globals.mainForm.txtTeachedSqLinearHome.Text);
                inifile.WriteValue(SECTION_SETTINGS, KEY_TEACHED_HEIGHT, Globals.mainForm.txtTeachedHeight.Text);
            }
            catch
            {
                MessageBox.Show("Error writing settings file " + path, "File I/O error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
