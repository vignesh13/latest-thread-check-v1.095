﻿namespace SMAC_Thread_Check_Center
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        /// Set container to be empty
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Release unmanaged resources used by your application
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.txtTerminal = new System.Windows.Forms.TextBox();
            this.txtPositionCloseToPart = new System.Windows.Forms.TextBox();
            this.lblPositionCloseToPart = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblThreadDepth = new System.Windows.Forms.Label();
            this.txtThreadDepth = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cmbActuator = new System.Windows.Forms.ComboBox();
            this.lblActuator = new System.Windows.Forms.Label();
            this.lblPort = new System.Windows.Forms.Label();
            this.cmbPort = new System.Windows.Forms.ComboBox();
            this.btnLoadSettingsFromFile = new System.Windows.Forms.Button();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label15 = new System.Windows.Forms.Label();
            this.btnActuatorInfo = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.Run = new System.Windows.Forms.TabPage();
            this.pbxPlugRun = new System.Windows.Forms.PictureBox();
            this.lblThreadDepthRun = new System.Windows.Forms.Label();
            this.lblThreadStartRun = new System.Windows.Forms.Label();
            this.lblHomePositionRun = new System.Windows.Forms.Label();
            this.lblActuatorRun = new System.Windows.Forms.Label();
            this.lblThreadClearanceRun = new System.Windows.Forms.Label();
            this.lblPluginfoRun = new System.Windows.Forms.Label();
            this.lblThreadClearance = new System.Windows.Forms.Label();
            this.lblThreadDepthMeasured = new System.Windows.Forms.Label();
            this.lblLandingHeight = new System.Windows.Forms.Label();
            this.pictureBoxRun = new System.Windows.Forms.PictureBox();
            this.Setup = new System.Windows.Forms.TabPage();
            this.cmbController = new System.Windows.Forms.ComboBox();
            this.lblController = new System.Windows.Forms.Label();
            this.lblThreadPitch = new System.Windows.Forms.Label();
            this.lblCloseToPart = new System.Windows.Forms.Label();
            this.lblHomePositionSetup = new System.Windows.Forms.Label();
            this.lblThreadStartSetup = new System.Windows.Forms.Label();
            this.lblThreadDepthSetup = new System.Windows.Forms.Label();
            this.lblThreadClearanceSetup = new System.Windows.Forms.Label();
            this.pbxPlugSetup = new System.Windows.Forms.PictureBox();
            this.lblActuatorSetup = new System.Windows.Forms.Label();
            this.lblPluginfoSetup = new System.Windows.Forms.Label();
            this.rbnBottom = new System.Windows.Forms.RadioButton();
            this.rbnTop = new System.Windows.Forms.RadioButton();
            this.chkCheckThreadClearance = new System.Windows.Forms.CheckBox();
            this.lblThreadClearance2 = new System.Windows.Forms.Label();
            this.lblThreadDepthMeasured2 = new System.Windows.Forms.Label();
            this.lblLandingHeight2 = new System.Windows.Forms.Label();
            this.cmbPitchUnits = new System.Windows.Forms.ComboBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.btnTeach = new System.Windows.Forms.Button();
            this.lblMaxThreadClearance = new System.Windows.Forms.Label();
            this.lblPartLocationMax = new System.Windows.Forms.Label();
            this.lblPartLocationMin = new System.Windows.Forms.Label();
            this.txtPartLocationMax = new System.Windows.Forms.TextBox();
            this.txtPartLocationMin = new System.Windows.Forms.TextBox();
            this.txtPitch = new System.Windows.Forms.TextBox();
            this.chkCheckLandedHeight = new System.Windows.Forms.CheckBox();
            this.chkRotateReverse = new System.Windows.Forms.CheckBox();
            this.cmbThreadType = new System.Windows.Forms.ComboBox();
            this.cmbThreadDirection = new System.Windows.Forms.ComboBox();
            this.pictureBoxSetup = new System.Windows.Forms.PictureBox();
            this.lblPitch = new System.Windows.Forms.Label();
            this.lblThreadType = new System.Windows.Forms.Label();
            this.lblThreadDirection = new System.Windows.Forms.Label();
            this.txtMaxThreadClearance = new System.Windows.Forms.TextBox();
            this.Tuning = new System.Windows.Forms.TabPage();
            this.gbxTuningRotary = new System.Windows.Forms.GroupBox();
            this.txtTuningRotaryP = new System.Windows.Forms.TextBox();
            this.txtTuningRotaryI = new System.Windows.Forms.TextBox();
            this.txtTuningRotaryD = new System.Windows.Forms.TextBox();
            this.lblTuningRotaryP = new System.Windows.Forms.Label();
            this.lblTuningRotaryDUnits = new System.Windows.Forms.Label();
            this.lblTuningRotaryI = new System.Windows.Forms.Label();
            this.lblTuningRotaryD = new System.Windows.Forms.Label();
            this.lblTuningRotaryIUnits = new System.Windows.Forms.Label();
            this.lblTuningRotaryPUnits = new System.Windows.Forms.Label();
            this.gbxTuningLinear = new System.Windows.Forms.GroupBox();
            this.txtTuningLinearP = new System.Windows.Forms.TextBox();
            this.txtTuningLinearD = new System.Windows.Forms.TextBox();
            this.lblTuningLinearP = new System.Windows.Forms.Label();
            this.txtTuningLinearI = new System.Windows.Forms.TextBox();
            this.lblTuningLinearI = new System.Windows.Forms.Label();
            this.lblTuningLinearD = new System.Windows.Forms.Label();
            this.lblTuningLinearDUnits = new System.Windows.Forms.Label();
            this.lblTuningLinearPUnits = new System.Windows.Forms.Label();
            this.lblTuningLinearIUnits = new System.Windows.Forms.Label();
            this.gbxTeachResult = new System.Windows.Forms.GroupBox();
            this.btnUndo = new System.Windows.Forms.Button();
            this.txtTeachedHeight = new System.Windows.Forms.TextBox();
            this.txtTeachedSqLinearHome = new System.Windows.Forms.TextBox();
            this.txtTeachedSqLinear = new System.Windows.Forms.TextBox();
            this.lblTeachedHeight = new System.Windows.Forms.Label();
            this.lblTeachedSqLinearHome = new System.Windows.Forms.Label();
            this.lblTeachedSqLinear = new System.Windows.Forms.Label();
            this.lblTeachedHeightUnits = new System.Windows.Forms.Label();
            this.lblRotarySensitivity = new System.Windows.Forms.Label();
            this.lblSpeed = new System.Windows.Forms.Label();
            this.txtRotarySensitivity = new System.Windows.Forms.TextBox();
            this.txtSpeed = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtMaxTorque = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.lblMaxTorque = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.btnRun = new System.Windows.Forms.Button();
            this.btnSaveProgramToFile = new System.Windows.Forms.Button();
            this.btnSingleCycle = new System.Windows.Forms.Button();
            this.btnSaveToController = new System.Windows.Forms.Button();
            this.btnSaveSettingsToFile = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnContactInfo = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnHelpDocument = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.chkShowAll = new System.Windows.Forms.CheckBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.lblStatusTxt = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.cbDisableScndPitch = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.Run.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxPlugRun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRun)).BeginInit();
            this.Setup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxPlugSetup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSetup)).BeginInit();
            this.Tuning.SuspendLayout();
            this.gbxTuningRotary.SuspendLayout();
            this.gbxTuningLinear.SuspendLayout();
            this.gbxTeachResult.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // txtTerminal
            // 
            this.txtTerminal.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTerminal.Location = new System.Drawing.Point(406, 418);
            this.txtTerminal.Multiline = true;
            this.txtTerminal.Name = "txtTerminal";
            this.txtTerminal.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtTerminal.Size = new System.Drawing.Size(418, 272);
            this.txtTerminal.TabIndex = 0;
            this.txtTerminal.WordWrap = false;
            this.txtTerminal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTerminal_KeyPress);
            // 
            // txtPositionCloseToPart
            // 
            this.txtPositionCloseToPart.Location = new System.Drawing.Point(723, 147);
            this.txtPositionCloseToPart.Name = "txtPositionCloseToPart";
            this.txtPositionCloseToPart.Size = new System.Drawing.Size(55, 20);
            this.txtPositionCloseToPart.TabIndex = 504;
            this.txtPositionCloseToPart.Text = "50";
            this.txtPositionCloseToPart.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPositionCloseToPart_KeyUp);
            this.txtPositionCloseToPart.Leave += new System.EventHandler(this.txtPositionCloseToPart_Leave);
            // 
            // lblPositionCloseToPart
            // 
            this.lblPositionCloseToPart.AutoSize = true;
            this.lblPositionCloseToPart.Location = new System.Drawing.Point(461, 151);
            this.lblPositionCloseToPart.Name = "lblPositionCloseToPart";
            this.lblPositionCloseToPart.Size = new System.Drawing.Size(130, 13);
            this.lblPositionCloseToPart.TabIndex = 2;
            this.lblPositionCloseToPart.Text = "Position close to part [mm]";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(540, 400);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Controller output";
            // 
            // lblThreadDepth
            // 
            this.lblThreadDepth.AutoSize = true;
            this.lblThreadDepth.Location = new System.Drawing.Point(461, 133);
            this.lblThreadDepth.Name = "lblThreadDepth";
            this.lblThreadDepth.Size = new System.Drawing.Size(96, 13);
            this.lblThreadDepth.TabIndex = 2;
            this.lblThreadDepth.Text = "Thread depth [mm]";
            // 
            // txtThreadDepth
            // 
            this.txtThreadDepth.Location = new System.Drawing.Point(723, 129);
            this.txtThreadDepth.Name = "txtThreadDepth";
            this.txtThreadDepth.Size = new System.Drawing.Size(55, 20);
            this.txtThreadDepth.TabIndex = 508;
            this.txtThreadDepth.Text = "10";
            this.txtThreadDepth.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtThreadDepth_KeyUp);
            this.txtThreadDepth.Leave += new System.EventHandler(this.txtThreadDepth_Leave);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(482, 275);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(0, 13);
            this.label11.TabIndex = 2;
            // 
            // cmbActuator
            // 
            this.cmbActuator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbActuator.FormattingEnabled = true;
            this.cmbActuator.Items.AddRange(new object[] {
            "Lar35-050-100-XX-XXXX-MOD000"});
            this.cmbActuator.Location = new System.Drawing.Point(550, 34);
            this.cmbActuator.Name = "cmbActuator";
            this.cmbActuator.Size = new System.Drawing.Size(228, 21);
            this.cmbActuator.TabIndex = 500;
            this.cmbActuator.SelectedIndexChanged += new System.EventHandler(this.cmbActuator_SelectedIndexChanged);
            // 
            // lblActuator
            // 
            this.lblActuator.AutoSize = true;
            this.lblActuator.Location = new System.Drawing.Point(461, 39);
            this.lblActuator.Name = "lblActuator";
            this.lblActuator.Size = new System.Drawing.Size(47, 13);
            this.lblActuator.TabIndex = 2;
            this.lblActuator.Text = "Actuator";
            // 
            // lblPort
            // 
            this.lblPort.AutoSize = true;
            this.lblPort.Location = new System.Drawing.Point(850, 47);
            this.lblPort.Name = "lblPort";
            this.lblPort.Size = new System.Drawing.Size(26, 13);
            this.lblPort.TabIndex = 2;
            this.lblPort.Text = "Port";
            // 
            // cmbPort
            // 
            this.cmbPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPort.FormattingEnabled = true;
            this.cmbPort.Location = new System.Drawing.Point(887, 44);
            this.cmbPort.Name = "cmbPort";
            this.cmbPort.Size = new System.Drawing.Size(79, 21);
            this.cmbPort.Sorted = true;
            this.cmbPort.TabIndex = 109;
            this.cmbPort.SelectedIndexChanged += new System.EventHandler(this.cmbPort_SelectedIndexChanged);
            this.cmbPort.MouseDown += new System.Windows.Forms.MouseEventHandler(this.cmbPort_MouseDown);
            // 
            // btnLoadSettingsFromFile
            // 
            this.btnLoadSettingsFromFile.Location = new System.Drawing.Point(849, 100);
            this.btnLoadSettingsFromFile.Name = "btnLoadSettingsFromFile";
            this.btnLoadSettingsFromFile.Size = new System.Drawing.Size(118, 35);
            this.btnLoadSettingsFromFile.TabIndex = 110;
            this.btnLoadSettingsFromFile.Text = "Load settings from file";
            this.btnLoadSettingsFromFile.UseVisualStyleBackColor = true;
            this.btnLoadSettingsFromFile.Click += new System.EventHandler(this.btnLoadSettingsFromFile_Click);
            // 
            // chart1
            // 
            chartArea2.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea2);
            this.chart1.Location = new System.Drawing.Point(10, 418);
            this.chart1.Name = "chart1";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series2.Name = "Series1";
            this.chart1.Series.Add(series2);
            this.chart1.Size = new System.Drawing.Size(377, 272);
            this.chart1.TabIndex = 112;
            this.chart1.Text = "chart1";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label15.Location = new System.Drawing.Point(106, 400);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(194, 15);
            this.label15.TabIndex = 113;
            this.label15.Text = "Rotary Position Error vs. Linear Position";
            // 
            // btnActuatorInfo
            // 
            this.btnActuatorInfo.Location = new System.Drawing.Point(784, 11);
            this.btnActuatorInfo.Name = "btnActuatorInfo";
            this.btnActuatorInfo.Size = new System.Drawing.Size(49, 23);
            this.btnActuatorInfo.TabIndex = 114;
            this.btnActuatorInfo.Text = "Info";
            this.btnActuatorInfo.UseVisualStyleBackColor = true;
            this.btnActuatorInfo.Click += new System.EventHandler(this.btnActuatorinfo_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.Run);
            this.tabControl1.Controls.Add(this.Setup);
            this.tabControl1.Controls.Add(this.Tuning);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(847, 397);
            this.tabControl1.TabIndex = 115;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // Run
            // 
            this.Run.Controls.Add(this.pbxPlugRun);
            this.Run.Controls.Add(this.lblThreadDepthRun);
            this.Run.Controls.Add(this.lblThreadStartRun);
            this.Run.Controls.Add(this.lblHomePositionRun);
            this.Run.Controls.Add(this.lblActuatorRun);
            this.Run.Controls.Add(this.lblThreadClearanceRun);
            this.Run.Controls.Add(this.lblPluginfoRun);
            this.Run.Controls.Add(this.lblThreadClearance);
            this.Run.Controls.Add(this.lblThreadDepthMeasured);
            this.Run.Controls.Add(this.lblLandingHeight);
            this.Run.Controls.Add(this.pictureBoxRun);
            this.Run.Location = new System.Drawing.Point(4, 22);
            this.Run.Name = "Run";
            this.Run.Padding = new System.Windows.Forms.Padding(3);
            this.Run.Size = new System.Drawing.Size(839, 371);
            this.Run.TabIndex = 0;
            this.Run.Text = "Run";
            this.Run.UseVisualStyleBackColor = true;
            // 
            // pbxPlugRun
            // 
            this.pbxPlugRun.Image = ((System.Drawing.Image)(resources.GetObject("pbxPlugRun.Image")));
            this.pbxPlugRun.Location = new System.Drawing.Point(366, 128);
            this.pbxPlugRun.Name = "pbxPlugRun";
            this.pbxPlugRun.Size = new System.Drawing.Size(68, 55);
            this.pbxPlugRun.TabIndex = 532;
            this.pbxPlugRun.TabStop = false;
            // 
            // lblThreadDepthRun
            // 
            this.lblThreadDepthRun.AutoSize = true;
            this.lblThreadDepthRun.BackColor = System.Drawing.Color.Transparent;
            this.lblThreadDepthRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThreadDepthRun.Location = new System.Drawing.Point(563, 308);
            this.lblThreadDepthRun.Name = "lblThreadDepthRun";
            this.lblThreadDepthRun.Size = new System.Drawing.Size(103, 16);
            this.lblThreadDepthRun.TabIndex = 531;
            this.lblThreadDepthRun.Text = "Thread Depth";
            // 
            // lblThreadStartRun
            // 
            this.lblThreadStartRun.AutoSize = true;
            this.lblThreadStartRun.BackColor = System.Drawing.Color.Transparent;
            this.lblThreadStartRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThreadStartRun.Location = new System.Drawing.Point(563, 236);
            this.lblThreadStartRun.Name = "lblThreadStartRun";
            this.lblThreadStartRun.Size = new System.Drawing.Size(94, 16);
            this.lblThreadStartRun.TabIndex = 531;
            this.lblThreadStartRun.Text = "Thread Start";
            // 
            // lblHomePositionRun
            // 
            this.lblHomePositionRun.AutoSize = true;
            this.lblHomePositionRun.BackColor = System.Drawing.Color.Transparent;
            this.lblHomePositionRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHomePositionRun.Location = new System.Drawing.Point(563, 151);
            this.lblHomePositionRun.Name = "lblHomePositionRun";
            this.lblHomePositionRun.Size = new System.Drawing.Size(108, 16);
            this.lblHomePositionRun.TabIndex = 531;
            this.lblHomePositionRun.Text = "Home position";
            // 
            // lblActuatorRun
            // 
            this.lblActuatorRun.AutoSize = true;
            this.lblActuatorRun.BackColor = System.Drawing.Color.Silver;
            this.lblActuatorRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblActuatorRun.Location = new System.Drawing.Point(378, 27);
            this.lblActuatorRun.Name = "lblActuatorRun";
            this.lblActuatorRun.Size = new System.Drawing.Size(98, 16);
            this.lblActuatorRun.TabIndex = 530;
            this.lblActuatorRun.Text = "LAR Actuator\r\n";
            // 
            // lblThreadClearanceRun
            // 
            this.lblThreadClearanceRun.AutoSize = true;
            this.lblThreadClearanceRun.BackColor = System.Drawing.Color.Transparent;
            this.lblThreadClearanceRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThreadClearanceRun.Location = new System.Drawing.Point(123, 265);
            this.lblThreadClearanceRun.Name = "lblThreadClearanceRun";
            this.lblThreadClearanceRun.Size = new System.Drawing.Size(133, 32);
            this.lblThreadClearanceRun.TabIndex = 529;
            this.lblThreadClearanceRun.Text = "Thread Clearance\r\n(Thread Play)";
            // 
            // lblPluginfoRun
            // 
            this.lblPluginfoRun.BackColor = System.Drawing.Color.SteelBlue;
            this.lblPluginfoRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPluginfoRun.Location = new System.Drawing.Point(138, 57);
            this.lblPluginfoRun.Name = "lblPluginfoRun";
            this.lblPluginfoRun.Size = new System.Drawing.Size(158, 77);
            this.lblPluginfoRun.TabIndex = 528;
            this.lblPluginfoRun.Text = "\r\n  Thread Plug Gauge\r\n  w/ Cleanout Notch\r\n  and Lead in Area";
            // 
            // lblThreadClearance
            // 
            this.lblThreadClearance.AutoSize = true;
            this.lblThreadClearance.BackColor = System.Drawing.Color.Transparent;
            this.lblThreadClearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThreadClearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblThreadClearance.Location = new System.Drawing.Point(551, 57);
            this.lblThreadClearance.Name = "lblThreadClearance";
            this.lblThreadClearance.Size = new System.Drawing.Size(164, 20);
            this.lblThreadClearance.TabIndex = 523;
            this.lblThreadClearance.Text = "lblThreadClearance";
            // 
            // lblThreadDepthMeasured
            // 
            this.lblThreadDepthMeasured.AutoSize = true;
            this.lblThreadDepthMeasured.BackColor = System.Drawing.Color.Transparent;
            this.lblThreadDepthMeasured.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThreadDepthMeasured.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblThreadDepthMeasured.Location = new System.Drawing.Point(504, 35);
            this.lblThreadDepthMeasured.Name = "lblThreadDepthMeasured";
            this.lblThreadDepthMeasured.Size = new System.Drawing.Size(211, 20);
            this.lblThreadDepthMeasured.TabIndex = 522;
            this.lblThreadDepthMeasured.Text = "lblThreadDepthMeasured";
            // 
            // lblLandingHeight
            // 
            this.lblLandingHeight.AutoSize = true;
            this.lblLandingHeight.BackColor = System.Drawing.Color.Transparent;
            this.lblLandingHeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLandingHeight.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblLandingHeight.Location = new System.Drawing.Point(571, 15);
            this.lblLandingHeight.Name = "lblLandingHeight";
            this.lblLandingHeight.Size = new System.Drawing.Size(144, 20);
            this.lblLandingHeight.TabIndex = 521;
            this.lblLandingHeight.Text = "lblLandingHeight";
            // 
            // pictureBoxRun
            // 
            this.pictureBoxRun.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxRun.Image")));
            this.pictureBoxRun.Location = new System.Drawing.Point(82, 15);
            this.pictureBoxRun.Name = "pictureBoxRun";
            this.pictureBoxRun.Size = new System.Drawing.Size(633, 336);
            this.pictureBoxRun.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxRun.TabIndex = 520;
            this.pictureBoxRun.TabStop = false;
            // 
            // Setup
            // 
            this.Setup.Controls.Add(this.cmbController);
            this.Setup.Controls.Add(this.lblController);
            this.Setup.Controls.Add(this.lblThreadPitch);
            this.Setup.Controls.Add(this.lblCloseToPart);
            this.Setup.Controls.Add(this.lblHomePositionSetup);
            this.Setup.Controls.Add(this.lblThreadStartSetup);
            this.Setup.Controls.Add(this.lblThreadDepthSetup);
            this.Setup.Controls.Add(this.lblThreadClearanceSetup);
            this.Setup.Controls.Add(this.pbxPlugSetup);
            this.Setup.Controls.Add(this.lblActuatorSetup);
            this.Setup.Controls.Add(this.lblPluginfoSetup);
            this.Setup.Controls.Add(this.rbnBottom);
            this.Setup.Controls.Add(this.rbnTop);
            this.Setup.Controls.Add(this.chkCheckThreadClearance);
            this.Setup.Controls.Add(this.lblThreadClearance2);
            this.Setup.Controls.Add(this.lblThreadDepthMeasured2);
            this.Setup.Controls.Add(this.lblLandingHeight2);
            this.Setup.Controls.Add(this.cmbPitchUnits);
            this.Setup.Controls.Add(this.label44);
            this.Setup.Controls.Add(this.label42);
            this.Setup.Controls.Add(this.btnTeach);
            this.Setup.Controls.Add(this.lblMaxThreadClearance);
            this.Setup.Controls.Add(this.lblPartLocationMax);
            this.Setup.Controls.Add(this.lblPartLocationMin);
            this.Setup.Controls.Add(this.txtPartLocationMax);
            this.Setup.Controls.Add(this.txtPartLocationMin);
            this.Setup.Controls.Add(this.lblThreadDepth);
            this.Setup.Controls.Add(this.txtThreadDepth);
            this.Setup.Controls.Add(this.txtPitch);
            this.Setup.Controls.Add(this.chkCheckLandedHeight);
            this.Setup.Controls.Add(this.chkRotateReverse);
            this.Setup.Controls.Add(this.cmbThreadType);
            this.Setup.Controls.Add(this.cmbThreadDirection);
            this.Setup.Controls.Add(this.pictureBoxSetup);
            this.Setup.Controls.Add(this.txtPositionCloseToPart);
            this.Setup.Controls.Add(this.btnActuatorInfo);
            this.Setup.Controls.Add(this.lblPitch);
            this.Setup.Controls.Add(this.lblPositionCloseToPart);
            this.Setup.Controls.Add(this.cmbActuator);
            this.Setup.Controls.Add(this.lblThreadType);
            this.Setup.Controls.Add(this.lblThreadDirection);
            this.Setup.Controls.Add(this.lblActuator);
            this.Setup.Controls.Add(this.txtMaxThreadClearance);
            this.Setup.Location = new System.Drawing.Point(4, 22);
            this.Setup.Name = "Setup";
            this.Setup.Padding = new System.Windows.Forms.Padding(3);
            this.Setup.Size = new System.Drawing.Size(839, 371);
            this.Setup.TabIndex = 1;
            this.Setup.Text = "Setup";
            this.Setup.UseVisualStyleBackColor = true;
            // 
            // cmbController
            // 
            this.cmbController.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbController.FormattingEnabled = true;
            this.cmbController.Location = new System.Drawing.Point(550, 12);
            this.cmbController.Name = "cmbController";
            this.cmbController.Size = new System.Drawing.Size(228, 21);
            this.cmbController.TabIndex = 540;
            this.cmbController.SelectedIndexChanged += new System.EventHandler(this.cmbController_SelectedIndexChanged);
            // 
            // lblController
            // 
            this.lblController.AutoSize = true;
            this.lblController.Location = new System.Drawing.Point(461, 18);
            this.lblController.Name = "lblController";
            this.lblController.Size = new System.Drawing.Size(78, 13);
            this.lblController.TabIndex = 539;
            this.lblController.Text = "Controller Type";
            // 
            // lblThreadPitch
            // 
            this.lblThreadPitch.AutoSize = true;
            this.lblThreadPitch.BackColor = System.Drawing.Color.Transparent;
            this.lblThreadPitch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThreadPitch.Location = new System.Drawing.Point(4, 235);
            this.lblThreadPitch.Name = "lblThreadPitch";
            this.lblThreadPitch.Size = new System.Drawing.Size(96, 16);
            this.lblThreadPitch.TabIndex = 538;
            this.lblThreadPitch.Text = "Thread Pitch";
            // 
            // lblCloseToPart
            // 
            this.lblCloseToPart.AutoSize = true;
            this.lblCloseToPart.BackColor = System.Drawing.Color.Transparent;
            this.lblCloseToPart.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCloseToPart.Location = new System.Drawing.Point(4, 204);
            this.lblCloseToPart.Name = "lblCloseToPart";
            this.lblCloseToPart.Size = new System.Drawing.Size(96, 16);
            this.lblCloseToPart.TabIndex = 538;
            this.lblCloseToPart.Text = "Close to part";
            // 
            // lblHomePositionSetup
            // 
            this.lblHomePositionSetup.AutoSize = true;
            this.lblHomePositionSetup.BackColor = System.Drawing.Color.Transparent;
            this.lblHomePositionSetup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHomePositionSetup.Location = new System.Drawing.Point(4, 151);
            this.lblHomePositionSetup.Name = "lblHomePositionSetup";
            this.lblHomePositionSetup.Size = new System.Drawing.Size(108, 16);
            this.lblHomePositionSetup.TabIndex = 538;
            this.lblHomePositionSetup.Text = "Home position";
            // 
            // lblThreadStartSetup
            // 
            this.lblThreadStartSetup.AutoSize = true;
            this.lblThreadStartSetup.BackColor = System.Drawing.Color.Transparent;
            this.lblThreadStartSetup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThreadStartSetup.Location = new System.Drawing.Point(349, 240);
            this.lblThreadStartSetup.Name = "lblThreadStartSetup";
            this.lblThreadStartSetup.Size = new System.Drawing.Size(94, 16);
            this.lblThreadStartSetup.TabIndex = 537;
            this.lblThreadStartSetup.Text = "Thread Start";
            // 
            // lblThreadDepthSetup
            // 
            this.lblThreadDepthSetup.AutoSize = true;
            this.lblThreadDepthSetup.BackColor = System.Drawing.Color.Transparent;
            this.lblThreadDepthSetup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThreadDepthSetup.Location = new System.Drawing.Point(349, 320);
            this.lblThreadDepthSetup.Name = "lblThreadDepthSetup";
            this.lblThreadDepthSetup.Size = new System.Drawing.Size(103, 16);
            this.lblThreadDepthSetup.TabIndex = 536;
            this.lblThreadDepthSetup.Text = "Thread Depth";
            // 
            // lblThreadClearanceSetup
            // 
            this.lblThreadClearanceSetup.AutoSize = true;
            this.lblThreadClearanceSetup.BackColor = System.Drawing.Color.Transparent;
            this.lblThreadClearanceSetup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThreadClearanceSetup.Location = new System.Drawing.Point(4, 302);
            this.lblThreadClearanceSetup.Name = "lblThreadClearanceSetup";
            this.lblThreadClearanceSetup.Size = new System.Drawing.Size(133, 32);
            this.lblThreadClearanceSetup.TabIndex = 535;
            this.lblThreadClearanceSetup.Text = "Thread Clearance\r\n(Thread Play)";
            // 
            // pbxPlugSetup
            // 
            this.pbxPlugSetup.Image = ((System.Drawing.Image)(resources.GetObject("pbxPlugSetup.Image")));
            this.pbxPlugSetup.Location = new System.Drawing.Point(194, 125);
            this.pbxPlugSetup.Name = "pbxPlugSetup";
            this.pbxPlugSetup.Size = new System.Drawing.Size(70, 54);
            this.pbxPlugSetup.TabIndex = 534;
            this.pbxPlugSetup.TabStop = false;
            // 
            // lblActuatorSetup
            // 
            this.lblActuatorSetup.AutoSize = true;
            this.lblActuatorSetup.BackColor = System.Drawing.Color.Silver;
            this.lblActuatorSetup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblActuatorSetup.Location = new System.Drawing.Point(211, 16);
            this.lblActuatorSetup.Name = "lblActuatorSetup";
            this.lblActuatorSetup.Size = new System.Drawing.Size(98, 16);
            this.lblActuatorSetup.TabIndex = 533;
            this.lblActuatorSetup.Text = "LAR Actuator\r\n";
            // 
            // lblPluginfoSetup
            // 
            this.lblPluginfoSetup.BackColor = System.Drawing.Color.SteelBlue;
            this.lblPluginfoSetup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPluginfoSetup.Location = new System.Drawing.Point(9, 61);
            this.lblPluginfoSetup.Name = "lblPluginfoSetup";
            this.lblPluginfoSetup.Size = new System.Drawing.Size(153, 78);
            this.lblPluginfoSetup.TabIndex = 532;
            this.lblPluginfoSetup.Text = "\r\n  Thread Plug Gauge\r\n  w/ Cleanout Notch\r\n  and Lead in Area";
            // 
            // rbnBottom
            // 
            this.rbnBottom.AutoSize = true;
            this.rbnBottom.Location = new System.Drawing.Point(612, 275);
            this.rbnBottom.Name = "rbnBottom";
            this.rbnBottom.Size = new System.Drawing.Size(70, 17);
            this.rbnBottom.TabIndex = 531;
            this.rbnBottom.Text = "At bottom";
            this.rbnBottom.UseVisualStyleBackColor = true;
            // 
            // rbnTop
            // 
            this.rbnTop.AutoSize = true;
            this.rbnTop.Checked = true;
            this.rbnTop.Location = new System.Drawing.Point(612, 255);
            this.rbnTop.Name = "rbnTop";
            this.rbnTop.Size = new System.Drawing.Size(53, 17);
            this.rbnTop.TabIndex = 531;
            this.rbnTop.TabStop = true;
            this.rbnTop.Text = "At top";
            this.rbnTop.UseVisualStyleBackColor = true;
            // 
            // chkCheckThreadClearance
            // 
            this.chkCheckThreadClearance.AutoSize = true;
            this.chkCheckThreadClearance.Location = new System.Drawing.Point(462, 256);
            this.chkCheckThreadClearance.Name = "chkCheckThreadClearance";
            this.chkCheckThreadClearance.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkCheckThreadClearance.Size = new System.Drawing.Size(140, 17);
            this.chkCheckThreadClearance.TabIndex = 530;
            this.chkCheckThreadClearance.Text = "Check thread clearance";
            this.chkCheckThreadClearance.UseVisualStyleBackColor = true;
            this.chkCheckThreadClearance.CheckedChanged += new System.EventHandler(this.chkCheckThreadClearance_CheckedChanged);
            // 
            // lblThreadClearance2
            // 
            this.lblThreadClearance2.AutoSize = true;
            this.lblThreadClearance2.BackColor = System.Drawing.Color.Transparent;
            this.lblThreadClearance2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThreadClearance2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblThreadClearance2.Location = new System.Drawing.Point(8, 45);
            this.lblThreadClearance2.Name = "lblThreadClearance2";
            this.lblThreadClearance2.Size = new System.Drawing.Size(174, 20);
            this.lblThreadClearance2.TabIndex = 526;
            this.lblThreadClearance2.Text = "lblThreadClearance2";
            // 
            // lblThreadDepthMeasured2
            // 
            this.lblThreadDepthMeasured2.AutoSize = true;
            this.lblThreadDepthMeasured2.BackColor = System.Drawing.Color.Transparent;
            this.lblThreadDepthMeasured2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThreadDepthMeasured2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblThreadDepthMeasured2.Location = new System.Drawing.Point(8, 25);
            this.lblThreadDepthMeasured2.Name = "lblThreadDepthMeasured2";
            this.lblThreadDepthMeasured2.Size = new System.Drawing.Size(211, 20);
            this.lblThreadDepthMeasured2.TabIndex = 525;
            this.lblThreadDepthMeasured2.Text = "lblThreadDepthMeasured";
            // 
            // lblLandingHeight2
            // 
            this.lblLandingHeight2.AutoSize = true;
            this.lblLandingHeight2.BackColor = System.Drawing.Color.Transparent;
            this.lblLandingHeight2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLandingHeight2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblLandingHeight2.Location = new System.Drawing.Point(8, 9);
            this.lblLandingHeight2.Name = "lblLandingHeight2";
            this.lblLandingHeight2.Size = new System.Drawing.Size(154, 20);
            this.lblLandingHeight2.TabIndex = 524;
            this.lblLandingHeight2.Text = "lblLandingHeight2";
            // 
            // cmbPitchUnits
            // 
            this.cmbPitchUnits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPitchUnits.FormattingEnabled = true;
            this.cmbPitchUnits.Location = new System.Drawing.Point(784, 110);
            this.cmbPitchUnits.Name = "cmbPitchUnits";
            this.cmbPitchUnits.Size = new System.Drawing.Size(49, 21);
            this.cmbPitchUnits.TabIndex = 522;
            this.cmbPitchUnits.SelectedIndexChanged += new System.EventHandler(this.cmbPitchUnits_SelectedIndexChanged);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(541, 420);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(68, 13);
            this.label44.TabIndex = 519;
            this.label44.Text = "Teached SQ";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(541, 410);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(68, 13);
            this.label42.TabIndex = 519;
            this.label42.Text = "Teached SQ";
            // 
            // btnTeach
            // 
            this.btnTeach.Location = new System.Drawing.Point(784, 146);
            this.btnTeach.Name = "btnTeach";
            this.btnTeach.Size = new System.Drawing.Size(49, 23);
            this.btnTeach.TabIndex = 120;
            this.btnTeach.Text = "Teach";
            this.btnTeach.UseVisualStyleBackColor = true;
            this.btnTeach.Click += new System.EventHandler(this.btnTeach_Click);
            // 
            // lblMaxThreadClearance
            // 
            this.lblMaxThreadClearance.AutoSize = true;
            this.lblMaxThreadClearance.Location = new System.Drawing.Point(568, 301);
            this.lblMaxThreadClearance.Name = "lblMaxThreadClearance";
            this.lblMaxThreadClearance.Size = new System.Drawing.Size(132, 13);
            this.lblMaxThreadClearance.TabIndex = 119;
            this.lblMaxThreadClearance.Text = "Max thead clearance [mm]";
            // 
            // lblPartLocationMax
            // 
            this.lblPartLocationMax.AutoSize = true;
            this.lblPartLocationMax.Location = new System.Drawing.Point(568, 209);
            this.lblPartLocationMax.Name = "lblPartLocationMax";
            this.lblPartLocationMax.Size = new System.Drawing.Size(122, 13);
            this.lblPartLocationMax.TabIndex = 119;
            this.lblPartLocationMax.Text = "Landed height max [mm]";
            // 
            // lblPartLocationMin
            // 
            this.lblPartLocationMin.AutoSize = true;
            this.lblPartLocationMin.Location = new System.Drawing.Point(568, 188);
            this.lblPartLocationMin.Name = "lblPartLocationMin";
            this.lblPartLocationMin.Size = new System.Drawing.Size(119, 13);
            this.lblPartLocationMin.TabIndex = 119;
            this.lblPartLocationMin.Text = "Landed height min [mm]";
            // 
            // txtPartLocationMax
            // 
            this.txtPartLocationMax.Location = new System.Drawing.Point(723, 205);
            this.txtPartLocationMax.Name = "txtPartLocationMax";
            this.txtPartLocationMax.Size = new System.Drawing.Size(55, 20);
            this.txtPartLocationMax.TabIndex = 506;
            this.txtPartLocationMax.Text = "55";
            this.txtPartLocationMax.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPartLocationMax_KeyUp);
            this.txtPartLocationMax.Leave += new System.EventHandler(this.txtPartLocationMax_Leave);
            // 
            // txtPartLocationMin
            // 
            this.txtPartLocationMin.Location = new System.Drawing.Point(722, 184);
            this.txtPartLocationMin.Name = "txtPartLocationMin";
            this.txtPartLocationMin.Size = new System.Drawing.Size(55, 20);
            this.txtPartLocationMin.TabIndex = 505;
            this.txtPartLocationMin.Text = "51";
            this.txtPartLocationMin.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPartLocationMin_KeyUp);
            this.txtPartLocationMin.Leave += new System.EventHandler(this.txtPartLocationMin_Leave);
            // 
            // txtPitch
            // 
            this.txtPitch.Location = new System.Drawing.Point(723, 110);
            this.txtPitch.Name = "txtPitch";
            this.txtPitch.Size = new System.Drawing.Size(55, 20);
            this.txtPitch.TabIndex = 507;
            this.txtPitch.Text = "2";
            this.txtPitch.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPitch_KeyUp);
            this.txtPitch.Leave += new System.EventHandler(this.txtPitch_Leave);
            // 
            // chkCheckLandedHeight
            // 
            this.chkCheckLandedHeight.AutoSize = true;
            this.chkCheckLandedHeight.Location = new System.Drawing.Point(464, 168);
            this.chkCheckLandedHeight.Name = "chkCheckLandedHeight";
            this.chkCheckLandedHeight.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkCheckLandedHeight.Size = new System.Drawing.Size(223, 17);
            this.chkCheckLandedHeight.TabIndex = 117;
            this.chkCheckLandedHeight.Text = "Check landed height (counter bore depth)";
            this.chkCheckLandedHeight.UseVisualStyleBackColor = true;
            this.chkCheckLandedHeight.CheckedChanged += new System.EventHandler(this.chkCheckLandedHeight_CheckedChanged);
            // 
            // chkRotateReverse
            // 
            this.chkRotateReverse.AutoSize = true;
            this.chkRotateReverse.Location = new System.Drawing.Point(462, 233);
            this.chkRotateReverse.Name = "chkRotateReverse";
            this.chkRotateReverse.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkRotateReverse.Size = new System.Drawing.Size(203, 17);
            this.chkRotateReverse.TabIndex = 117;
            this.chkRotateReverse.Text = "Rotate in reverse to locate first thread";
            this.chkRotateReverse.UseVisualStyleBackColor = true;
            this.chkRotateReverse.CheckedChanged += new System.EventHandler(this.chkRotateReverse_CheckedChanged);
            // 
            // cmbThreadType
            // 
            this.cmbThreadType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbThreadType.FormattingEnabled = true;
            this.cmbThreadType.Location = new System.Drawing.Point(550, 78);
            this.cmbThreadType.Name = "cmbThreadType";
            this.cmbThreadType.Size = new System.Drawing.Size(228, 21);
            this.cmbThreadType.TabIndex = 503;
            // 
            // cmbThreadDirection
            // 
            this.cmbThreadDirection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbThreadDirection.FormattingEnabled = true;
            this.cmbThreadDirection.Location = new System.Drawing.Point(550, 56);
            this.cmbThreadDirection.Name = "cmbThreadDirection";
            this.cmbThreadDirection.Size = new System.Drawing.Size(228, 21);
            this.cmbThreadDirection.TabIndex = 502;
            // 
            // pictureBoxSetup
            // 
            this.pictureBoxSetup.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxSetup.Image")));
            this.pictureBoxSetup.Location = new System.Drawing.Point(6, 3);
            this.pictureBoxSetup.Name = "pictureBoxSetup";
            this.pictureBoxSetup.Size = new System.Drawing.Size(450, 362);
            this.pictureBoxSetup.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxSetup.TabIndex = 115;
            this.pictureBoxSetup.TabStop = false;
            // 
            // lblPitch
            // 
            this.lblPitch.AutoSize = true;
            this.lblPitch.Location = new System.Drawing.Point(461, 114);
            this.lblPitch.Name = "lblPitch";
            this.lblPitch.Size = new System.Drawing.Size(31, 13);
            this.lblPitch.TabIndex = 2;
            this.lblPitch.Text = "Pitch";
            // 
            // lblThreadType
            // 
            this.lblThreadType.AutoSize = true;
            this.lblThreadType.Location = new System.Drawing.Point(461, 83);
            this.lblThreadType.Name = "lblThreadType";
            this.lblThreadType.Size = new System.Drawing.Size(64, 13);
            this.lblThreadType.TabIndex = 2;
            this.lblThreadType.Text = "Thread type";
            // 
            // lblThreadDirection
            // 
            this.lblThreadDirection.AutoSize = true;
            this.lblThreadDirection.Location = new System.Drawing.Point(461, 60);
            this.lblThreadDirection.Name = "lblThreadDirection";
            this.lblThreadDirection.Size = new System.Drawing.Size(84, 13);
            this.lblThreadDirection.TabIndex = 2;
            this.lblThreadDirection.Text = "Thread direction";
            // 
            // txtMaxThreadClearance
            // 
            this.txtMaxThreadClearance.Location = new System.Drawing.Point(722, 297);
            this.txtMaxThreadClearance.Name = "txtMaxThreadClearance";
            this.txtMaxThreadClearance.Size = new System.Drawing.Size(55, 20);
            this.txtMaxThreadClearance.TabIndex = 509;
            this.txtMaxThreadClearance.Text = "100";
            this.txtMaxThreadClearance.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtThreadClearance_KeyUp);
            this.txtMaxThreadClearance.Leave += new System.EventHandler(this.txtThreadClearance_Leave);
            // 
            // Tuning
            // 
            this.Tuning.Controls.Add(this.cbDisableScndPitch);
            this.Tuning.Controls.Add(this.gbxTuningRotary);
            this.Tuning.Controls.Add(this.gbxTuningLinear);
            this.Tuning.Controls.Add(this.gbxTeachResult);
            this.Tuning.Controls.Add(this.lblRotarySensitivity);
            this.Tuning.Controls.Add(this.lblSpeed);
            this.Tuning.Controls.Add(this.txtRotarySensitivity);
            this.Tuning.Controls.Add(this.txtSpeed);
            this.Tuning.Controls.Add(this.label40);
            this.Tuning.Controls.Add(this.label20);
            this.Tuning.Controls.Add(this.label11);
            this.Tuning.Controls.Add(this.txtMaxTorque);
            this.Tuning.Controls.Add(this.label25);
            this.Tuning.Controls.Add(this.lblMaxTorque);
            this.Tuning.Location = new System.Drawing.Point(4, 22);
            this.Tuning.Name = "Tuning";
            this.Tuning.Size = new System.Drawing.Size(839, 371);
            this.Tuning.TabIndex = 2;
            this.Tuning.Text = "Tuning";
            this.Tuning.UseVisualStyleBackColor = true;
            // 
            // gbxTuningRotary
            // 
            this.gbxTuningRotary.Controls.Add(this.txtTuningRotaryP);
            this.gbxTuningRotary.Controls.Add(this.txtTuningRotaryI);
            this.gbxTuningRotary.Controls.Add(this.txtTuningRotaryD);
            this.gbxTuningRotary.Controls.Add(this.lblTuningRotaryP);
            this.gbxTuningRotary.Controls.Add(this.lblTuningRotaryDUnits);
            this.gbxTuningRotary.Controls.Add(this.lblTuningRotaryI);
            this.gbxTuningRotary.Controls.Add(this.lblTuningRotaryD);
            this.gbxTuningRotary.Controls.Add(this.lblTuningRotaryIUnits);
            this.gbxTuningRotary.Controls.Add(this.lblTuningRotaryPUnits);
            this.gbxTuningRotary.Location = new System.Drawing.Point(26, 197);
            this.gbxTuningRotary.Name = "gbxTuningRotary";
            this.gbxTuningRotary.Size = new System.Drawing.Size(130, 136);
            this.gbxTuningRotary.TabIndex = 531;
            this.gbxTuningRotary.TabStop = false;
            this.gbxTuningRotary.Text = " Tuning rotary ";
            // 
            // txtTuningRotaryP
            // 
            this.txtTuningRotaryP.Location = new System.Drawing.Point(40, 37);
            this.txtTuningRotaryP.Name = "txtTuningRotaryP";
            this.txtTuningRotaryP.Size = new System.Drawing.Size(55, 20);
            this.txtTuningRotaryP.TabIndex = 514;
            this.txtTuningRotaryP.Text = "100";
            this.txtTuningRotaryP.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtTuningRotaryP_KeyUp);
            this.txtTuningRotaryP.Leave += new System.EventHandler(this.txtTuningRotaryP_Leave);
            // 
            // txtTuningRotaryI
            // 
            this.txtTuningRotaryI.Location = new System.Drawing.Point(40, 61);
            this.txtTuningRotaryI.Name = "txtTuningRotaryI";
            this.txtTuningRotaryI.Size = new System.Drawing.Size(55, 20);
            this.txtTuningRotaryI.TabIndex = 515;
            this.txtTuningRotaryI.Text = "100";
            this.txtTuningRotaryI.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtTuningRotaryI_KeyUp);
            this.txtTuningRotaryI.Leave += new System.EventHandler(this.txtTuningRotaryI_Leave);
            // 
            // txtTuningRotaryD
            // 
            this.txtTuningRotaryD.Location = new System.Drawing.Point(40, 85);
            this.txtTuningRotaryD.Name = "txtTuningRotaryD";
            this.txtTuningRotaryD.Size = new System.Drawing.Size(55, 20);
            this.txtTuningRotaryD.TabIndex = 516;
            this.txtTuningRotaryD.Text = "100";
            this.txtTuningRotaryD.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtTuningRotaryD_KeyUp);
            this.txtTuningRotaryD.Leave += new System.EventHandler(this.txtTuningRotaryD_Leave);
            // 
            // lblTuningRotaryP
            // 
            this.lblTuningRotaryP.AutoSize = true;
            this.lblTuningRotaryP.Location = new System.Drawing.Point(22, 39);
            this.lblTuningRotaryP.Name = "lblTuningRotaryP";
            this.lblTuningRotaryP.Size = new System.Drawing.Size(14, 13);
            this.lblTuningRotaryP.TabIndex = 2;
            this.lblTuningRotaryP.Text = "P";
            // 
            // lblTuningRotaryDUnits
            // 
            this.lblTuningRotaryDUnits.AutoSize = true;
            this.lblTuningRotaryDUnits.Location = new System.Drawing.Point(99, 87);
            this.lblTuningRotaryDUnits.Name = "lblTuningRotaryDUnits";
            this.lblTuningRotaryDUnits.Size = new System.Drawing.Size(15, 13);
            this.lblTuningRotaryDUnits.TabIndex = 2;
            this.lblTuningRotaryDUnits.Text = "%";
            // 
            // lblTuningRotaryI
            // 
            this.lblTuningRotaryI.AutoSize = true;
            this.lblTuningRotaryI.Location = new System.Drawing.Point(22, 63);
            this.lblTuningRotaryI.Name = "lblTuningRotaryI";
            this.lblTuningRotaryI.Size = new System.Drawing.Size(10, 13);
            this.lblTuningRotaryI.TabIndex = 2;
            this.lblTuningRotaryI.Text = "I";
            // 
            // lblTuningRotaryD
            // 
            this.lblTuningRotaryD.AutoSize = true;
            this.lblTuningRotaryD.Location = new System.Drawing.Point(22, 87);
            this.lblTuningRotaryD.Name = "lblTuningRotaryD";
            this.lblTuningRotaryD.Size = new System.Drawing.Size(15, 13);
            this.lblTuningRotaryD.TabIndex = 2;
            this.lblTuningRotaryD.Text = "D";
            // 
            // lblTuningRotaryIUnits
            // 
            this.lblTuningRotaryIUnits.AutoSize = true;
            this.lblTuningRotaryIUnits.Location = new System.Drawing.Point(99, 63);
            this.lblTuningRotaryIUnits.Name = "lblTuningRotaryIUnits";
            this.lblTuningRotaryIUnits.Size = new System.Drawing.Size(15, 13);
            this.lblTuningRotaryIUnits.TabIndex = 2;
            this.lblTuningRotaryIUnits.Text = "%";
            // 
            // lblTuningRotaryPUnits
            // 
            this.lblTuningRotaryPUnits.AutoSize = true;
            this.lblTuningRotaryPUnits.Location = new System.Drawing.Point(99, 39);
            this.lblTuningRotaryPUnits.Name = "lblTuningRotaryPUnits";
            this.lblTuningRotaryPUnits.Size = new System.Drawing.Size(15, 13);
            this.lblTuningRotaryPUnits.TabIndex = 2;
            this.lblTuningRotaryPUnits.Text = "%";
            // 
            // gbxTuningLinear
            // 
            this.gbxTuningLinear.Controls.Add(this.txtTuningLinearP);
            this.gbxTuningLinear.Controls.Add(this.txtTuningLinearD);
            this.gbxTuningLinear.Controls.Add(this.lblTuningLinearP);
            this.gbxTuningLinear.Controls.Add(this.txtTuningLinearI);
            this.gbxTuningLinear.Controls.Add(this.lblTuningLinearI);
            this.gbxTuningLinear.Controls.Add(this.lblTuningLinearD);
            this.gbxTuningLinear.Controls.Add(this.lblTuningLinearDUnits);
            this.gbxTuningLinear.Controls.Add(this.lblTuningLinearPUnits);
            this.gbxTuningLinear.Controls.Add(this.lblTuningLinearIUnits);
            this.gbxTuningLinear.Location = new System.Drawing.Point(26, 38);
            this.gbxTuningLinear.Name = "gbxTuningLinear";
            this.gbxTuningLinear.Size = new System.Drawing.Size(130, 136);
            this.gbxTuningLinear.TabIndex = 530;
            this.gbxTuningLinear.TabStop = false;
            this.gbxTuningLinear.Text = " Tuning linear ";
            // 
            // txtTuningLinearP
            // 
            this.txtTuningLinearP.Location = new System.Drawing.Point(36, 31);
            this.txtTuningLinearP.Name = "txtTuningLinearP";
            this.txtTuningLinearP.Size = new System.Drawing.Size(55, 20);
            this.txtTuningLinearP.TabIndex = 511;
            this.txtTuningLinearP.Text = "100";
            this.txtTuningLinearP.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtTuningLinearP_KeyUp);
            this.txtTuningLinearP.Leave += new System.EventHandler(this.txtTuningLinearP_Leave);
            // 
            // txtTuningLinearD
            // 
            this.txtTuningLinearD.Location = new System.Drawing.Point(36, 79);
            this.txtTuningLinearD.Name = "txtTuningLinearD";
            this.txtTuningLinearD.Size = new System.Drawing.Size(55, 20);
            this.txtTuningLinearD.TabIndex = 513;
            this.txtTuningLinearD.Text = "100";
            this.txtTuningLinearD.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtTuningLinearD_KeyUp);
            this.txtTuningLinearD.Leave += new System.EventHandler(this.txtTuningLinearD_Leave);
            // 
            // lblTuningLinearP
            // 
            this.lblTuningLinearP.AutoSize = true;
            this.lblTuningLinearP.Location = new System.Drawing.Point(18, 33);
            this.lblTuningLinearP.Name = "lblTuningLinearP";
            this.lblTuningLinearP.Size = new System.Drawing.Size(14, 13);
            this.lblTuningLinearP.TabIndex = 2;
            this.lblTuningLinearP.Text = "P";
            // 
            // txtTuningLinearI
            // 
            this.txtTuningLinearI.Location = new System.Drawing.Point(36, 55);
            this.txtTuningLinearI.Name = "txtTuningLinearI";
            this.txtTuningLinearI.Size = new System.Drawing.Size(55, 20);
            this.txtTuningLinearI.TabIndex = 512;
            this.txtTuningLinearI.Text = "100";
            this.txtTuningLinearI.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtTuningLinearI_KeyUp);
            this.txtTuningLinearI.Leave += new System.EventHandler(this.txtTuningLinearI_Leave);
            // 
            // lblTuningLinearI
            // 
            this.lblTuningLinearI.AutoSize = true;
            this.lblTuningLinearI.Location = new System.Drawing.Point(18, 57);
            this.lblTuningLinearI.Name = "lblTuningLinearI";
            this.lblTuningLinearI.Size = new System.Drawing.Size(10, 13);
            this.lblTuningLinearI.TabIndex = 2;
            this.lblTuningLinearI.Text = "I";
            // 
            // lblTuningLinearD
            // 
            this.lblTuningLinearD.AutoSize = true;
            this.lblTuningLinearD.Location = new System.Drawing.Point(18, 81);
            this.lblTuningLinearD.Name = "lblTuningLinearD";
            this.lblTuningLinearD.Size = new System.Drawing.Size(15, 13);
            this.lblTuningLinearD.TabIndex = 2;
            this.lblTuningLinearD.Text = "D";
            // 
            // lblTuningLinearDUnits
            // 
            this.lblTuningLinearDUnits.AutoSize = true;
            this.lblTuningLinearDUnits.Location = new System.Drawing.Point(95, 81);
            this.lblTuningLinearDUnits.Name = "lblTuningLinearDUnits";
            this.lblTuningLinearDUnits.Size = new System.Drawing.Size(15, 13);
            this.lblTuningLinearDUnits.TabIndex = 2;
            this.lblTuningLinearDUnits.Text = "%";
            // 
            // lblTuningLinearPUnits
            // 
            this.lblTuningLinearPUnits.AutoSize = true;
            this.lblTuningLinearPUnits.Location = new System.Drawing.Point(95, 33);
            this.lblTuningLinearPUnits.Name = "lblTuningLinearPUnits";
            this.lblTuningLinearPUnits.Size = new System.Drawing.Size(15, 13);
            this.lblTuningLinearPUnits.TabIndex = 2;
            this.lblTuningLinearPUnits.Text = "%";
            // 
            // lblTuningLinearIUnits
            // 
            this.lblTuningLinearIUnits.AutoSize = true;
            this.lblTuningLinearIUnits.Location = new System.Drawing.Point(95, 57);
            this.lblTuningLinearIUnits.Name = "lblTuningLinearIUnits";
            this.lblTuningLinearIUnits.Size = new System.Drawing.Size(15, 13);
            this.lblTuningLinearIUnits.TabIndex = 2;
            this.lblTuningLinearIUnits.Text = "%";
            // 
            // gbxTeachResult
            // 
            this.gbxTeachResult.Controls.Add(this.btnUndo);
            this.gbxTeachResult.Controls.Add(this.txtTeachedHeight);
            this.gbxTeachResult.Controls.Add(this.txtTeachedSqLinearHome);
            this.gbxTeachResult.Controls.Add(this.txtTeachedSqLinear);
            this.gbxTeachResult.Controls.Add(this.lblTeachedHeight);
            this.gbxTeachResult.Controls.Add(this.lblTeachedSqLinearHome);
            this.gbxTeachResult.Controls.Add(this.lblTeachedSqLinear);
            this.gbxTeachResult.Controls.Add(this.lblTeachedHeightUnits);
            this.gbxTeachResult.Location = new System.Drawing.Point(251, 206);
            this.gbxTeachResult.Name = "gbxTeachResult";
            this.gbxTeachResult.Size = new System.Drawing.Size(261, 127);
            this.gbxTeachResult.TabIndex = 529;
            this.gbxTeachResult.TabStop = false;
            this.gbxTeachResult.Text = " Teach result";
            // 
            // btnUndo
            // 
            this.btnUndo.Location = new System.Drawing.Point(168, 48);
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(49, 23);
            this.btnUndo.TabIndex = 521;
            this.btnUndo.Text = "Undo";
            this.btnUndo.UseVisualStyleBackColor = true;
            this.btnUndo.Visible = false;
            this.btnUndo.Click += new System.EventHandler(this.btnUndo_Click);
            // 
            // txtTeachedHeight
            // 
            this.txtTeachedHeight.Location = new System.Drawing.Point(96, 83);
            this.txtTeachedHeight.Name = "txtTeachedHeight";
            this.txtTeachedHeight.ReadOnly = true;
            this.txtTeachedHeight.Size = new System.Drawing.Size(51, 20);
            this.txtTeachedHeight.TabIndex = 520;
            // 
            // txtTeachedSqLinearHome
            // 
            this.txtTeachedSqLinearHome.Location = new System.Drawing.Point(96, 16);
            this.txtTeachedSqLinearHome.Name = "txtTeachedSqLinearHome";
            this.txtTeachedSqLinearHome.ReadOnly = true;
            this.txtTeachedSqLinearHome.Size = new System.Drawing.Size(51, 20);
            this.txtTeachedSqLinearHome.TabIndex = 520;
            // 
            // txtTeachedSqLinear
            // 
            this.txtTeachedSqLinear.Location = new System.Drawing.Point(96, 49);
            this.txtTeachedSqLinear.Name = "txtTeachedSqLinear";
            this.txtTeachedSqLinear.ReadOnly = true;
            this.txtTeachedSqLinear.Size = new System.Drawing.Size(51, 20);
            this.txtTeachedSqLinear.TabIndex = 520;
            // 
            // lblTeachedHeight
            // 
            this.lblTeachedHeight.AutoSize = true;
            this.lblTeachedHeight.Location = new System.Drawing.Point(11, 86);
            this.lblTeachedHeight.Name = "lblTeachedHeight";
            this.lblTeachedHeight.Size = new System.Drawing.Size(38, 13);
            this.lblTeachedHeight.TabIndex = 519;
            this.lblTeachedHeight.Text = "Height";
            // 
            // lblTeachedSqLinearHome
            // 
            this.lblTeachedSqLinearHome.AutoSize = true;
            this.lblTeachedSqLinearHome.Location = new System.Drawing.Point(11, 20);
            this.lblTeachedSqLinearHome.Name = "lblTeachedSqLinearHome";
            this.lblTeachedSqLinearHome.Size = new System.Drawing.Size(67, 13);
            this.lblTeachedSqLinearHome.TabIndex = 519;
            this.lblTeachedSqLinearHome.Text = "SQ retracted";
            // 
            // lblTeachedSqLinear
            // 
            this.lblTeachedSqLinear.AutoSize = true;
            this.lblTeachedSqLinear.Location = new System.Drawing.Point(11, 53);
            this.lblTeachedSqLinear.Name = "lblTeachedSqLinear";
            this.lblTeachedSqLinear.Size = new System.Drawing.Size(83, 13);
            this.lblTeachedSqLinear.TabIndex = 519;
            this.lblTeachedSqLinear.Text = "SQ close to part";
            // 
            // lblTeachedHeightUnits
            // 
            this.lblTeachedHeightUnits.AutoSize = true;
            this.lblTeachedHeightUnits.Location = new System.Drawing.Point(157, 88);
            this.lblTeachedHeightUnits.Name = "lblTeachedHeightUnits";
            this.lblTeachedHeightUnits.Size = new System.Drawing.Size(26, 13);
            this.lblTeachedHeightUnits.TabIndex = 2;
            this.lblTeachedHeightUnits.Text = "mm.";
            // 
            // lblRotarySensitivity
            // 
            this.lblRotarySensitivity.AutoSize = true;
            this.lblRotarySensitivity.Location = new System.Drawing.Point(270, 96);
            this.lblRotarySensitivity.Name = "lblRotarySensitivity";
            this.lblRotarySensitivity.Size = new System.Drawing.Size(86, 13);
            this.lblRotarySensitivity.TabIndex = 527;
            this.lblRotarySensitivity.Text = "Rotary sensitivity";
            // 
            // lblSpeed
            // 
            this.lblSpeed.AutoSize = true;
            this.lblSpeed.Location = new System.Drawing.Point(270, 52);
            this.lblSpeed.Name = "lblSpeed";
            this.lblSpeed.Size = new System.Drawing.Size(38, 13);
            this.lblSpeed.TabIndex = 512;
            this.lblSpeed.Text = "Speed";
            // 
            // txtRotarySensitivity
            // 
            this.txtRotarySensitivity.Location = new System.Drawing.Point(371, 91);
            this.txtRotarySensitivity.Name = "txtRotarySensitivity";
            this.txtRotarySensitivity.Size = new System.Drawing.Size(55, 20);
            this.txtRotarySensitivity.TabIndex = 518;
            this.txtRotarySensitivity.Text = "100";
            this.txtRotarySensitivity.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtRotarySensitivity_KeyUp);
            this.txtRotarySensitivity.Leave += new System.EventHandler(this.txtRotarySensitivity_Leave);
            // 
            // txtSpeed
            // 
            this.txtSpeed.Location = new System.Drawing.Point(371, 48);
            this.txtSpeed.Name = "txtSpeed";
            this.txtSpeed.Size = new System.Drawing.Size(55, 20);
            this.txtSpeed.TabIndex = 517;
            this.txtSpeed.Text = "100";
            this.txtSpeed.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSpeed_KeyUp);
            this.txtSpeed.Leave += new System.EventHandler(this.txtSpeed_Leave);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(430, 51);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(15, 13);
            this.label40.TabIndex = 2;
            this.label40.Text = "%";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(431, 95);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(39, 13);
            this.label20.TabIndex = 2;
            this.label20.Text = "counts";
            // 
            // txtMaxTorque
            // 
            this.txtMaxTorque.Location = new System.Drawing.Point(371, 134);
            this.txtMaxTorque.Name = "txtMaxTorque";
            this.txtMaxTorque.Size = new System.Drawing.Size(55, 20);
            this.txtMaxTorque.TabIndex = 510;
            this.txtMaxTorque.Text = "100";
            this.txtMaxTorque.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtMaxTorque_KeyUp);
            this.txtMaxTorque.Leave += new System.EventHandler(this.txtMaxTorque_Leave);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(432, 135);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(15, 13);
            this.label25.TabIndex = 2;
            this.label25.Text = "%";
            // 
            // lblMaxTorque
            // 
            this.lblMaxTorque.AutoSize = true;
            this.lblMaxTorque.Location = new System.Drawing.Point(270, 138);
            this.lblMaxTorque.Name = "lblMaxTorque";
            this.lblMaxTorque.Size = new System.Drawing.Size(91, 13);
            this.lblMaxTorque.TabIndex = 2;
            this.lblMaxTorque.Text = "Permisable torque";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(940, 355);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(5, 5);
            this.button2.TabIndex = 518;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnRun
            // 
            this.btnRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRun.Location = new System.Drawing.Point(849, 566);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(120, 35);
            this.btnRun.TabIndex = 120;
            this.btnRun.Text = "RUN";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // btnSaveProgramToFile
            // 
            this.btnSaveProgramToFile.Location = new System.Drawing.Point(849, 197);
            this.btnSaveProgramToFile.Name = "btnSaveProgramToFile";
            this.btnSaveProgramToFile.Size = new System.Drawing.Size(118, 35);
            this.btnSaveProgramToFile.TabIndex = 120;
            this.btnSaveProgramToFile.Text = "Save program to file";
            this.btnSaveProgramToFile.UseVisualStyleBackColor = true;
            this.btnSaveProgramToFile.Click += new System.EventHandler(this.btnSaveProgramToFile_Click);
            // 
            // btnSingleCycle
            // 
            this.btnSingleCycle.Location = new System.Drawing.Point(849, 295);
            this.btnSingleCycle.Name = "btnSingleCycle";
            this.btnSingleCycle.Size = new System.Drawing.Size(118, 35);
            this.btnSingleCycle.TabIndex = 120;
            this.btnSingleCycle.Text = "Single cycle";
            this.btnSingleCycle.UseVisualStyleBackColor = true;
            this.btnSingleCycle.Click += new System.EventHandler(this.btnSingleCycle_Click);
            // 
            // btnSaveToController
            // 
            this.btnSaveToController.Location = new System.Drawing.Point(849, 237);
            this.btnSaveToController.Name = "btnSaveToController";
            this.btnSaveToController.Size = new System.Drawing.Size(118, 35);
            this.btnSaveToController.TabIndex = 120;
            this.btnSaveToController.Text = "Save program to controller";
            this.btnSaveToController.UseVisualStyleBackColor = true;
            this.btnSaveToController.Click += new System.EventHandler(this.btnSaveInController_Click);
            // 
            // btnSaveSettingsToFile
            // 
            this.btnSaveSettingsToFile.Location = new System.Drawing.Point(849, 140);
            this.btnSaveSettingsToFile.Name = "btnSaveSettingsToFile";
            this.btnSaveSettingsToFile.Size = new System.Drawing.Size(118, 35);
            this.btnSaveSettingsToFile.TabIndex = 110;
            this.btnSaveSettingsToFile.Text = "Save settings to file";
            this.btnSaveSettingsToFile.UseVisualStyleBackColor = true;
            this.btnSaveSettingsToFile.Click += new System.EventHandler(this.btnSaveSettingsToFile_Click);
            // 
            // btnStop
            // 
            this.btnStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStop.Location = new System.Drawing.Point(851, 618);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(120, 60);
            this.btnStop.TabIndex = 116;
            this.btnStop.Text = "STOP";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnContactInfo
            // 
            this.btnContactInfo.Location = new System.Drawing.Point(851, 461);
            this.btnContactInfo.Name = "btnContactInfo";
            this.btnContactInfo.Size = new System.Drawing.Size(120, 27);
            this.btnContactInfo.TabIndex = 117;
            this.btnContactInfo.Text = "Contact information";
            this.btnContactInfo.UseVisualStyleBackColor = true;
            this.btnContactInfo.Click += new System.EventHandler(this.btnContactInfo_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(841, 396);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(136, 59);
            this.pictureBox2.TabIndex = 118;
            this.pictureBox2.TabStop = false;
            // 
            // btnHelpDocument
            // 
            this.btnHelpDocument.Location = new System.Drawing.Point(851, 503);
            this.btnHelpDocument.Name = "btnHelpDocument";
            this.btnHelpDocument.Size = new System.Drawing.Size(120, 27);
            this.btnHelpDocument.TabIndex = 117;
            this.btnHelpDocument.Text = "Help document";
            this.btnHelpDocument.UseVisualStyleBackColor = true;
            this.btnHelpDocument.Click += new System.EventHandler(this.button4_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(919, 383);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(52, 316);
            this.listBox1.TabIndex = 119;
            // 
            // chkShowAll
            // 
            this.chkShowAll.AutoSize = true;
            this.chkShowAll.Location = new System.Drawing.Point(758, 398);
            this.chkShowAll.Name = "chkShowAll";
            this.chkShowAll.Size = new System.Drawing.Size(66, 17);
            this.chkShowAll.TabIndex = 120;
            this.chkShowAll.Text = "Show all";
            this.chkShowAll.UseVisualStyleBackColor = true;
            this.chkShowAll.CheckedChanged += new System.EventHandler(this.chkShowAll_CheckedChanged);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(704, 526);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox2.Size = new System.Drawing.Size(153, 57);
            this.textBox2.TabIndex = 121;
            // 
            // lblStatusTxt
            // 
            this.lblStatusTxt.AutoSize = true;
            this.lblStatusTxt.Location = new System.Drawing.Point(850, 76);
            this.lblStatusTxt.Name = "lblStatusTxt";
            this.lblStatusTxt.Size = new System.Drawing.Size(37, 13);
            this.lblStatusTxt.TabIndex = 519;
            this.lblStatusTxt.Text = "Status";
            // 
            // lblStatus
            // 
            this.lblStatus.Location = new System.Drawing.Point(889, 75);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(78, 15);
            this.lblStatus.TabIndex = 519;
            this.lblStatus.Text = "Status";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbDisableScndPitch
            // 
            this.cbDisableScndPitch.AutoSize = true;
            this.cbDisableScndPitch.Checked = true;
            this.cbDisableScndPitch.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbDisableScndPitch.Location = new System.Drawing.Point(273, 175);
            this.cbDisableScndPitch.Name = "cbDisableScndPitch";
            this.cbDisableScndPitch.Size = new System.Drawing.Size(176, 17);
            this.cbDisableScndPitch.TabIndex = 532;
            this.cbDisableScndPitch.Text = "Disable Secondary Pitch Check";
            this.cbDisableScndPitch.UseVisualStyleBackColor = true;
            this.cbDisableScndPitch.CheckedChanged += new System.EventHandler(this.cbDisableScndPitch_CheckedChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 702);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.lblStatusTxt);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.chkShowAll);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.btnHelpDocument);
            this.Controls.Add(this.btnContactInfo);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.btnRun);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btnSaveProgramToFile);
            this.Controls.Add(this.txtTerminal);
            this.Controls.Add(this.btnSingleCycle);
            this.Controls.Add(this.btnSaveToController);
            this.Controls.Add(this.btnLoadSettingsFromFile);
            this.Controls.Add(this.btnSaveSettingsToFile);
            this.Controls.Add(this.lblPort);
            this.Controls.Add(this.cmbPort);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SMAC Thread Check Center";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.Run.ResumeLayout(false);
            this.Run.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxPlugRun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRun)).EndInit();
            this.Setup.ResumeLayout(false);
            this.Setup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxPlugSetup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSetup)).EndInit();
            this.Tuning.ResumeLayout(false);
            this.Tuning.PerformLayout();
            this.gbxTuningRotary.ResumeLayout(false);
            this.gbxTuningRotary.PerformLayout();
            this.gbxTuningLinear.ResumeLayout(false);
            this.gbxTuningLinear.PerformLayout();
            this.gbxTeachResult.ResumeLayout(false);
            this.gbxTeachResult.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        // Private componant definitions
        private System.Windows.Forms.Button btnActuatorInfo;
        private System.Windows.Forms.Button btnContactInfo;
        private System.Windows.Forms.Button btnHelpDocument;
        private System.Windows.Forms.Button btnLoadSettingsFromFile;
        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.Button btnSaveProgramToFile;
        private System.Windows.Forms.Button btnSaveSettingsToFile;
        private System.Windows.Forms.Button btnSaveToController;
        private System.Windows.Forms.Button btnSingleCycle;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnTeach;
        private System.Windows.Forms.Button btnUndo;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.CheckBox chkShowAll;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.GroupBox gbxTeachResult;
        private System.Windows.Forms.GroupBox gbxTuningLinear;
        private System.Windows.Forms.GroupBox gbxTuningRotary;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblActuator;
        private System.Windows.Forms.Label lblActuatorRun;
        private System.Windows.Forms.Label lblActuatorSetup;
        private System.Windows.Forms.Label lblCloseToPart;
        private System.Windows.Forms.Label lblHomePositionRun;
        private System.Windows.Forms.Label lblHomePositionSetup;
        private System.Windows.Forms.Label lblLandingHeight2;
        private System.Windows.Forms.Label lblLandingHeight;
        private System.Windows.Forms.Label lblMaxThreadClearance;
        private System.Windows.Forms.Label lblMaxTorque;
        private System.Windows.Forms.Label lblPartLocationMax;
        private System.Windows.Forms.Label lblPartLocationMin;
        private System.Windows.Forms.Label lblPitch;
        private System.Windows.Forms.Label lblPluginfoRun;
        private System.Windows.Forms.Label lblPluginfoSetup;
        private System.Windows.Forms.Label lblPort;
        private System.Windows.Forms.Label lblPositionCloseToPart;
        private System.Windows.Forms.Label lblRotarySensitivity;
        private System.Windows.Forms.Label lblSpeed;
        private System.Windows.Forms.Label lblStatusTxt;
        private System.Windows.Forms.Label lblTeachedHeight;
        private System.Windows.Forms.Label lblTeachedHeightUnits;
        private System.Windows.Forms.Label lblTeachedSqLinear;
        private System.Windows.Forms.Label lblTeachedSqLinearHome;
        private System.Windows.Forms.Label lblThreadClearance2;
        private System.Windows.Forms.Label lblThreadClearance;
        private System.Windows.Forms.Label lblThreadClearanceRun;
        private System.Windows.Forms.Label lblThreadClearanceSetup;
        private System.Windows.Forms.Label lblThreadDepth;
        private System.Windows.Forms.Label lblThreadDepthMeasured2;
        private System.Windows.Forms.Label lblThreadDepthMeasured;
        private System.Windows.Forms.Label lblThreadDepthRun;
        private System.Windows.Forms.Label lblThreadDepthSetup;
        private System.Windows.Forms.Label lblThreadDirection;
        private System.Windows.Forms.Label lblThreadPitch;
        private System.Windows.Forms.Label lblThreadStartRun;
        private System.Windows.Forms.Label lblThreadStartSetup;
        private System.Windows.Forms.Label lblThreadType;
        private System.Windows.Forms.Label lblTuningLinearD;
        private System.Windows.Forms.Label lblTuningLinearDUnits;
        private System.Windows.Forms.Label lblTuningLinearI;
        private System.Windows.Forms.Label lblTuningLinearIUnits;
        private System.Windows.Forms.Label lblTuningLinearP;
        private System.Windows.Forms.Label lblTuningLinearPUnits;
        private System.Windows.Forms.Label lblTuningRotaryD;
        private System.Windows.Forms.Label lblTuningRotaryDUnits;
        private System.Windows.Forms.Label lblTuningRotaryI;
        private System.Windows.Forms.Label lblTuningRotaryIUnits;
        private System.Windows.Forms.Label lblTuningRotaryP;
        private System.Windows.Forms.Label lblTuningRotaryPUnits;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.PictureBox pbxPlugRun;
        private System.Windows.Forms.PictureBox pbxPlugSetup;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBoxRun;
        private System.Windows.Forms.PictureBox pictureBoxSetup;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage Run;
        private System.Windows.Forms.TabPage Setup;
        private System.Windows.Forms.TabPage Tuning;
        private System.Windows.Forms.TextBox textBox2;

        // Public componant definitions
        public System.Windows.Forms.CheckBox chkCheckLandedHeight;
        public System.Windows.Forms.CheckBox chkCheckThreadClearance;
        public System.Windows.Forms.CheckBox chkRotateReverse;
        public System.Windows.Forms.ComboBox cmbActuator;
        public System.Windows.Forms.ComboBox cmbPitchUnits;
        public System.Windows.Forms.ComboBox cmbPort;
        public System.Windows.Forms.ComboBox cmbThreadDirection;
        public System.Windows.Forms.ComboBox cmbThreadType;
        public System.Windows.Forms.Label lblStatus;
        public System.Windows.Forms.RadioButton rbnBottom;
        public System.Windows.Forms.RadioButton rbnTop;
        public System.Windows.Forms.TextBox txtMaxThreadClearance;
        public System.Windows.Forms.TextBox txtMaxTorque;
        public System.Windows.Forms.TextBox txtPartLocationMax;
        public System.Windows.Forms.TextBox txtPartLocationMin;
        public System.Windows.Forms.TextBox txtPitch;
        public System.Windows.Forms.TextBox txtPositionCloseToPart;
        public System.Windows.Forms.TextBox txtRotarySensitivity;
        public System.Windows.Forms.TextBox txtSpeed;
        public System.Windows.Forms.TextBox txtTeachedHeight;
        public System.Windows.Forms.TextBox txtTeachedSqLinear;
        public System.Windows.Forms.TextBox txtTeachedSqLinearHome;
        public System.Windows.Forms.TextBox txtTerminal;
        public System.Windows.Forms.TextBox txtThreadDepth;
        public System.Windows.Forms.TextBox txtTuningLinearD;
        public System.Windows.Forms.TextBox txtTuningLinearI;
        public System.Windows.Forms.TextBox txtTuningLinearP;
        public System.Windows.Forms.TextBox txtTuningRotaryD;
        public System.Windows.Forms.TextBox txtTuningRotaryI;
        public System.Windows.Forms.TextBox txtTuningRotaryP;
        public System.Windows.Forms.ComboBox cmbController;
        private System.Windows.Forms.Label lblController;
        private System.Windows.Forms.CheckBox cbDisableScndPitch;
    }
}

