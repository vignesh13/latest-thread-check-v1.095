﻿// Revision request to auto populate pull down of actuator names based on file

using System;
using System.Collections.Generic;
using System.IO;

namespace SMAC_Thread_Check_Center
{

    public class Actuator
    {
        private string strName = ""; // holds ini file name for checking
        private double linearResolution; // holds resolution 
        private double linearStroke; // holds stroke length

        private double rotaryCountsPerRevolution; // holds counts per rev
        private double rotaryGearRatio; // holds gear ratio
        private int maxSQ; // holds maxSQ

        private string fullPath = ""; // holds path to file

        public Variables variables = null; // holds all variables

        //
        // Construct an actuator and read the settings from an ini file
        // Input ini file containing stroke, rotary, and PID settings
        //
        public void Load(string filename)
        {
            // MessageBox.Show(Environment.CurrentDirectory)
            // replaces exclamation mark with colon mark in file name
            filename = filename.Replace(':', '!');
            variables = null;

            /*
            // searches each file in the actuator config directory until chosen one is found
            foreach (string s in Directory.GetFiles(Environment.CurrentDirectory + "\\Actuators", "*.ini*", SearchOption.AllDirectories))
            {
                if (Path.GetFileNameWithoutExtension(s) == filename)
                {
                    strName = filename.Replace('!', ':');
                    fullPath = s;

                    variables = new Variables(Constants.Defaults()); // loads default stroke, rotary, and PID settings
                    IniFile inifile = new IniFile(fullPath);

                    // get basic values from file
                    linearResolution = inifile.ReadDoubleValue("Linear", "Resolution");
                    linearStroke = inifile.ReadDoubleValue("Linear", "Stroke");
                    rotaryCountsPerRevolution = inifile.ReadDoubleValue("Rotary", "CountsPerRevolution");
                    rotaryGearRatio = inifile.ReadDoubleValue("Rotary", "GearRatio");
                    maxSQ = inifile.ReadIntValue("Rotary", "MaxSQ");
                    ReadOtherVariables("Constants");
                    break;
                }
            }
            */

            if (Globals.mainForm.cmbController.Text == "LAC-26")
            {
                // searches each file in the actuator config directory until chosen one is found
                foreach (string s in Directory.GetFiles(Environment.CurrentDirectory + "\\Actuators\\LAC26", "*.ini*", SearchOption.AllDirectories))
                {
                    if (Path.GetFileNameWithoutExtension(s) == filename)
                    {
                        strName = filename.Replace('!', ':');
                        fullPath = s;

                        variables = new Variables(Constants.Defaults()); // loads default stroke, rotary, and PID settings
                        IniFile inifile = new IniFile(fullPath);

                        // get basic values from file
                        linearResolution = inifile.ReadDoubleValue("Linear", "Resolution");
                        linearStroke = inifile.ReadDoubleValue("Linear", "Stroke");
                        rotaryCountsPerRevolution = inifile.ReadDoubleValue("Rotary", "CountsPerRevolution");
                        rotaryGearRatio = inifile.ReadDoubleValue("Rotary", "GearRatio");
                        maxSQ = inifile.ReadIntValue("Rotary", "MaxSQ");
                        ReadOtherVariables("Constants");
                        break;
                    }
                }
            }
            else
            {
                // searches each file in the actuator config directory until chosen one is found
                foreach (string s in Directory.GetFiles(Environment.CurrentDirectory + "\\Actuators\\LAC25", "*.ini*", SearchOption.AllDirectories))
                {
                    if (Path.GetFileNameWithoutExtension(s) == filename)
                    {
                        strName = filename.Replace('!', ':');
                        fullPath = s;

                        variables = new Variables(Constants.Defaults()); // loads default stroke, rotary, and PID settings
                        IniFile inifile = new IniFile(fullPath);

                        // get basic values from file
                        linearResolution = inifile.ReadDoubleValue("Linear", "Resolution");
                        linearStroke = inifile.ReadDoubleValue("Linear", "Stroke");
                        rotaryCountsPerRevolution = inifile.ReadDoubleValue("Rotary", "CountsPerRevolution");
                        rotaryGearRatio = inifile.ReadDoubleValue("Rotary", "GearRatio");
                        maxSQ = inifile.ReadIntValue("Rotary", "MaxSQ");
                        ReadOtherVariables("Constants");
                        break;
                    }
                }
            }
        }

        // Adds variables from chosen actuator into list
        public void ReadOtherVariables(string Section)
        {
            IniFile inifile = new IniFile(fullPath);
            foreach (string s in inifile.ReadSectionKeynames(Section))
            {
                variables.SetValue(s, inifile.ReadIntValue(Section, s));
            }
        }

        // create second list to hold data from actuator file along with definitions
        public string[] Info()
        {
            List<string> info = new List<string>();

            info.Add("Actuator: " + strName);
            info.Add("");
            info.Add("Linear:");
            info.Add("Encoder resolution: " + linearResolution.ToString().Replace(',', '.') + " um");
            info.Add("Stroke : " + linearStroke.ToString().Replace(',', '.') + " mm");
            info.Add("");
            info.Add("Rotary:");
            info.Add("Encoder counts per motor revolution: " + rotaryCountsPerRevolution.ToString().Replace(',', '.'));
            info.Add("Gear ratio: " + rotaryGearRatio.ToString().Replace(',', '.') + " : 1");
            info.Add("Encoder counts per revolution: " + (rotaryGearRatio * rotaryCountsPerRevolution).ToString().Replace(',', '.'));
            info.Add("Max SQ: " + maxSQ.ToString());
            return info.ToArray();
        }

        // Getter for basic motor info / variables
        public string Name()
        {
            return strName;
        }

        public double LinearResoloution()
        {
            return linearResolution;
        }

        public double LinearStroke()
        {
            return linearStroke;
        }

        public double RotaryResoloution()
        {
            return rotaryCountsPerRevolution * rotaryGearRatio;
        }

        public int RotaeyMaxSQ()
        {
            return maxSQ;
        }
        public Variables GetVariables()
        {
            return variables;
        }
    }
}
