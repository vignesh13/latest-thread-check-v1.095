﻿using System.Windows.Forms;

namespace SMAC_Thread_Check_Center
{
    //
    // The InputBoxInt always uses a . as the decimal separator (not depending on regional settings)
    //

    public class InputBoxInt
    {
        private int Min = 0;
        private int Max = 0;
        private int iValue = 0;
        private TextBox Textbox = null;
        private System.Drawing.Color okForecolor = System.Drawing.Color.Black;
        private System.Drawing.Color okBackcolor = System.Drawing.Color.White;
        private string Name = "";

        //
        // Summary: default constructor, sets values, colors, and decimal notation for a textbox using ints
        //
        public InputBoxInt(TextBox textbox, int min, int max, int initialvalue, string name)
        {
            Min = min;
            Max = max;
            Textbox = textbox;
            // app always uses . so convert , to . for regional use
            Textbox.Text = initialvalue.ToString().Replace(",", ".");
            Name = name;
            iValue = initialvalue;
            okForecolor = Textbox.ForeColor;
            okBackcolor = Textbox.BackColor;
        }

        //
        // Summary: just set limits for int textbox
        //
        public void SetLimits(int min, int max)
        {
            Min = min;
            Max = max;
        }

        //
        // Summary: gets int value, converts it to a string to display
        //
        // Return: string value with , replaced with .
        //
        public string Stringvalue
        {
            get
            {
                ReadValue();
                return iValue.ToString();
            }
            set
            {
                Textbox.Text = value.Trim();
            }
        }

        //
        // Summary: gets int value and displays as string
        //
        // Return: input int
        //
        public int Intvalue
        {
            get
            {
                ReadValue();
                return iValue;
            }
            set
            {
                iValue = value;
                Textbox.Text = value.ToString();
            }
        }

        //
        // Summary: gets string value, replaces , with . and displays
        //
        public void ReadValue()
        {
            string message = "";

            if (!int.TryParse(Textbox.Text, out iValue))
            {
                ErrorColors();
                message = "Invalid entry " + Textbox.Text + " for " + Name + "\r" +
                          "The value you entered is not a valid number";
                message += "\rValue replaced by " + Min.ToString();
                MessageBox.Show(message);
                iValue = Min; // set to min by default for safety
                Textbox.Text = Min.ToString(); // display
            }

            ClipToLimits(); // check input value and display
            NormalColors(); // set normal colors
        }

        //
        // Summary: Set ForeColor red and BackColor light yellow for warning
        //
        private void ErrorColors()
        {
            Textbox.ForeColor = System.Drawing.Color.Red;
            Textbox.BackColor = System.Drawing.Color.LightYellow;
        }

        //
        // Summary: Set ForeColor and BackColor to normal
        //
        private void NormalColors()
        {
            Textbox.ForeColor = okForecolor;
            Textbox.BackColor = okBackcolor;
        }

        //
        // Summary: Check input value to see if null or outside of range
        //
        private void ClipToLimits()
        {
            string message = ""; // holds error message to be displayed
            int newValue = 0; // holds default value for each case

            // set int value to min if below range and send error message
            if (iValue < Min)
            {
                message = "Invalid entry " + Textbox.Text + " for " + Name + "\r" +
                          "The minimum value is " + Min.ToString();
                newValue = Min;
            }
            // set int value to max if above range and send error message
            if (iValue > Max)
            {
                message = "Invalid entry " + Textbox.Text + " for " + Name + "\r" +
                          "The maximum value is " + Max.ToString();
                newValue = Max;
            }
            // if value in range then replace double value with new value and alert user
            if (message != "")
            {
                ErrorColors();
                message += "\rValue will be replaced by " + newValue.ToString();
                MessageBox.Show(message);
                iValue = newValue;
                NormalColors();
            }
        }
    }
}
