+----------------------------------------------------+
|               The master program                   |
+----------------------------------------------------+
;
;   I/O ALLOCATION
; ==================================
; OUTPUT 0: READY/RETRACTED/HOME
; OUTPUT 1: PASS
; OUTPUT 2: FAIL
; OUTPUT 3: ERROR/ JAM
; INPUT 0: START TEST CYLE
; INPUT 1: RESET (In Case of Jam Condition)
;
; Register usage:
;
; 20: Min position of linear during reverse thread find
; 21: Linear datum point. This is the position of the softland
; 22: Linear 1 pitch above the softland position
; 23: Linear 0.5 pitch above the softland position
; 24: Linear 0.5 depth below datum (point for push/pull test)
; 25: Not used
; 26: Rotary position of next check
; 27: Linear position of last check
; 28: Not used
; 29: Result of push/pull test
; 30: General purpose working register
; 31: Linear position of required depth
;
0MF
RM
; **CLEAR SCREEN, CONFIGURE I/O CHANNELS******
MD0,MG"!I0"
MD1,CH0,CH1,CH2,CH3,CF0,CF1,CF2,CF3
;
; **LOAD PID'S FOR AXIS 1 (Linear) & AXIS 2 (Rotary)
MD2,0MF,PH0,1PM,1SG[P_LINEAR],SI[I_LINEAR],SD[D_LINEAR],IL[IL_LINEAR],FR1
MD3,2PM,SG[P_ROTARY],SI[I_ROTARY],SD[D_ROTARY],IL[IL_ROTARY]
;
; ****OPTIONAL Setting up Interrupt (input 3)********
; **  This Disables the Linear and Rotary axis when input 3 is activated.
MD4,EV3,AL9,LV3
;
; Set Servo phasing for rotary to 0 for right handed thread and to 3 for left handed
;
MD5,2PH[PHASING_ROTARY],MJ10
;
; The interrupt routine
;
MD9,0MF,DV3,WF3,UM1,EV3,MJ0
;
; Homing routine for linear axis
;
MD10,1SV[V_HOMING_LINEAR],SA[A_HOMING_LINEAR],SQ[Q_HOMING_LINEAR],VM,MN,DI1,GO,WA250
MD11,RW538,IB[HARDSTOP_THRESHOLD_LINEAR],DI0,JR2,RP,1FI,MJ12
MD12,RL448,IC10,MJ13,NO,RW538,IG[INDEX_LIMIT_LINEAR],MJ14,NO,RP
MD13,AB,WA5,PM,GH,MG"LINEAR HOME",CN0,MJ15
MD14,MG"NO INDEX",0MF,CN3,MG"CLEAR THREAD GAUGE, PUSH RESET INPUT",WN1,MJ0
;
; Homing routine for rotary axis
;
MD15,2DH,SV[V_HOMING_ROTARY],SA[A_HOMING_ROTARY],SQ[Q_HOMING_ROTARY],VM,MN,DI0,GO,WA100,2FE1
MD16,RL592,IC11,MJ17,NO,RW682,IG[0.25_REV_ROTARY],MJ21,NO,RL638,IG[1.5_REV_ROTARY],MJ22,NO,RP
MD17,2PM,MN,MR-[0.25_REV_ROTARY],GO,WS10
MD18,2VM,MN,DI0,GO,2FE1
MD19,RL592,IC11,MJ20,NO,RW682,IG[0.25_REV_ROTARY],MJ21,NO,RP
MD20,2PM,GH,MG"ROTARY HOME",2MF,MJ25
MD21,2MF,MG"ROTARY JAM",CN3,MJ0
MD22,2MF,MG"NO COURSE FOUND",CN3,MJ15
;
; Start of main loop
; First wait for start input (input0) to become active
;
MD25,WN0,WA100,WF0
MD26,UM1,MG"!I1",CF0,CF1,CF2,CF3
MD27,AL0,AR20,AR21,AR22,AR23,AR24,AR25,AR26,AR27,AR28,AR29,AR30,AR31,MJ30
;
; Move linear close to part
;
MD30,1PM,MN,1SQ[Q_MOVE_CLOSE_TO_PART_LINEAR],SV[V_MOVE_CLOSE_TO_PART_LINEAR],SA[A_MOVE_CLOSE_TO_PART_LINEAR],MA[POS_CLOSE_TO_PART_LINEAR],GO,WS50,MJ40
;
; Softland on part
MD40,1VM,MN,SQ[Q1_SOFTLAND_LINEAR],SV[V_SOFTLAND_LINEAR],SA[A_SOFTLAND_LINEAR],DI0,GO,WA100
MD41,RW538,IG[PERR_SOFTLAND_LINEAR],MJ42,NO,RP
;
; Change to force mode, capture and report the datum (Format is Hnnnn)
; Check if the datum is within limits
; Calculate the position 1 pitch above the datum and 0.5 pitch above datum
; Calculate the position for the push/pull test
; Calculate the position for the minimum depth
;
MD42,1PM,SQ30000,QM0,MN,SQ[Q2_SOFTLAND_LINEAR],WA150,RL494,AR21,MG"!H":0
MD43,RL494,IB[PART_POSITION_MIN_LINEAR],MJ45,NO,IG[PART_POSITION_MAX_LINEAR],MJ46,NO,MG"HEIGHT OK"
MD44,RA21,AS[0.5_PITCH_LINEAR],AR24,AS[0.5_PITCH_LINEAR],AR23,RA21,AA[0.5_DEPTH_LINEAR],AR24,AA[0.5_DEPTH_LINEAR],AR31,AL0,WL1830,MJ50
;
MD45,1PM,MN,GH,WS20,CN2,MG"PART TOO HIGH",MJ10
MD46,1PM,MN,GH,WS20,CN2,MG"PART TOO LOW/NO PART PRESENT",MJ10
;
; Rotate in reverse to find first thread
; During reverse rotation Reg20 holds the minimum value (peak detect) of the linear.
; The start of thread is found when the linear is 0.5 pitch above the detected minimum
; This process must be completed within 1.5 revolution, if not then "no thread found" error.
; During search the following error of the rotary may not exceed 0.5 revolution
;
MD50,RA21,AR20,2VM,2MN,2SV[V_REVERSE_FIND_ROTARY],SA[A_REVERSE_FIND_ROTARY],SQ[Q_REVERSE_FIND_ROTARY],DI1,GO,2DH,WA20
MD51,MC169,IG[0.5_REV_ROTARY],MJ54,NO,RL638,IB-[1.5_REV_ROTARY],MJ53,NO,RL494,IB@20,AR20,RP,AS@20,IB[0.5_PITCH_LINEAR],RP,NO
MD52,2PM,2DH,RL494,AR30,MG"Threads Aligned",MJ60
MD53,MG"No Threads, Misaligned",MJ106
MD54,MG"Rottary Error,Clear Jam",MJ106
;
;
; Start rotation forward
; Rotate 1.5 revolution into the thread and then check if the linear has moved at least
; 1 full pitch inward
;
MD60,2VM,2DH,MN,DI0,MC150,GO,WA20,MC151
MD61,MC169,IG[0.5_REV_ROTARY],MJ64,NO,RL638,IG[1.5_REV_ROTARY],MJ62,NO,RP
MD62,RL494,AS@30,IG[1.0_PITCH_LINEAR],MJ70,NO,MJ63
;  
MD63,MG"NO THREADS/MISALIGNED",MJ105
MD64,MG"ROTARY BLOCKED",MJ106
;
; The thread has been picked up.
; Now rotate further with low linear force until required depth is reached.
; During the move the following error of the rotary and the increase of the linear are checked.
; If the depth is to the desired depth to achieve (GOOD PART) jump to line 80
;
MD70,2VM,MN,MC152,DI0,GO,WA200,RL494,AR27,RL638,MC170,AA[1.0_REV_ROTARY],AR26
MD71,MC171,MC120,RL494,IG@31,2MF,MJ90,RP
; 
; Depth achieved: Next will be the push/pull test (if enabled)
;
; 
MD90,MG"90-DEPTH PASSED",RL494,AS@21,MG"!D":0,2AB,MJ95
;
; 
; For the push/pull test:
; First rotate back to half depth
; During move check rotary following error and check if linear keeps decreasing
;
MD95,1MF,RL494,2VM,DI1,MN,MC150,GO,WA250,MC152,RL494,AR27,RL638,AS[1.0_REV_ROTARY],AR26
MD96,MC121,RL494,IB@24,NO,JR2,RP,2SA[A_BRAKE_FOR_PUSH_PULL_TEST_ROTARY],2SQ[Q_BRAKE_FOR_PUSH_PULL_TEST_ROTARY],2PM,2ST,WS100
;
; Rotary stopped at half depth: Do the push/pull test now
; 
MD97,1AB,1PM,MN,SQ[Q_FOR_PUSH_PULL_TEST_LINEAR],1QM0,SQ-[Q_FOR_PUSH_PULL_TEST_LINEAR],WA300,RL494,AR29
MD98,1QM0,SQ[Q_FOR_PUSH_PULL_TEST_LINEAR],GO,WA300,RL494,AS@29,AR29,MG"!P":29
MD99,IG[PUSH_PULL_LIMIT],MJ100,NO,MJ110
MD100,MG"Push-pull test out of limits",MJ105
;
; Macro 105: Set error output and rotate out of thread
; Macro 106: Set error output and move linear back home
;
MD105,CN2,MJ110
MD106,CN2,MJ113
;
; Macro 110: Rotate out of thread and then send linear back home
;
MD110,2AB,2MF,2VM,MN,DI1,MC150,GO,WA250,MC152
; Move to 1.0 pitch above datum point
MD111,1PM,MN,SV[V_ROTATE_BACK_LINEAR],SQ[Q_ROTATE_BACK_LINEAR],MA@22,GO,RL494,AR27,RL638,AS[1.0_REV_ROTARY],AR26
;
; During the reverse move monitor rotary following error
; Also check if the linear keeps moving op after each revolution
;
MD112,MC121,RL494,IB@23,NO,MJ113,RP
MD113,2MF,1PM,MN,SV[V_MOVE_BACK_HOME_LINEAR],SA[A_MOVE_BACK_HOME_LINEAR],SQ[Q_MOVE_BACK_HOME_LINEAR],GH,WS300,RL494,IG[0.5_PITCH_LINEAR],NO,MJ114,MJ115
MD114,MG"LINEAR NOT RETRACTED",CN2,CN3,MJ10
MD115,MG"LINEAR RETRACTED",CN0,MJ25
;
; Error Checks during rotate into thread:
; - Check if position error of the rotary stays within 0.5 revolution
; - Check after each revolution if the linear position has increased at least 80 % of pitch
;
MD120,RW682,MC170,IG[0.5_REV_ROTARY],MJ122,NO,RL638,MC170,IB@26,RC,NO,RL494,AS@27,IB[0.8_PITCH_LINEAR],MJ123,NO,RA26,AA[1.0_REV_ROTARY],AR26,RL494,AR27,RC
;
; Error Checks during rotate out of thread:
; - Check if position error of the rotary stays within 0.5 revolution
; - Check after each revolution if the linear position has decreased at least 80 % of pitch
;
MD121,RW682,MC170,IG[0.5_REV_ROTARY],MJ220,NO,RL638,MC170,IG@26,RC,NO,RL494,AS@27,IG-[0.8_PITCH_LINEAR],MJ225,NO,RA26,AS[1.0_REV_ROTARY],AR26,RL494,AR27,RC
MD122,MG"ROTARY BLOCKED",MJ105
MD123,MG"DULL THREAD",MJ106


;
;
; ****Soft Start for the Rotary *****
MD150,2SV[V_SOFT1_ROTARY],SA[A_SOFT1_ROTARY],SQ[Q_SOFT1_ROTARY],RC
MD151,2SV[V_SOFT2_ROTARY],SA[A_SOFT2_ROTARY],SQ[Q_SOFT2_ROTARY],RC
MD152,2SV[V_SOFT3_ROTARY],SA[A_SOFT3_ROTARY],SQ[Q_SOFT3_ROTARY],RC
MD153,2SV[V_SOFT4_ROTARY],SA[A_SOFT4_ROTARY],SQ[Q_SOFT4_ROTARY],RC
;
; Macro 169: Get absolute value of rotary folloing error in accum
;
MD169,RW682,MJ170
;
; Calculate absolute value of ACC
; Negative values are made positive by complementing and adding 1.
; This is a faster operation than multiplying by -1
; 
MD170,IB0,AC,AA1,RC
;
; Report linear position and rotary error (separated by a comma)
;
MD171,RL494,AS@21,MG"!":0:N,MG",":N,RW682,TR0,RC
;
; Macro 220 is executed when the rotary is blocked during a reverse move
; Manual intervention of the operator is necessary.
; Only way out is to press the reset button
;
MD220,0AB,MF
MD221,MG"Rotary blocked during reverse move"
MD222,MG"Remove block and press reset button",WN1,MJ0
;
; Macro 225 is executed when the linear is is not moving during a reverse move
; Manual intervention of the operator is necessary.
; Only way out is to press the reset button
;
MD225,0AB,MF
MD226,MG"Dull thread during reverse move"
MD227,MG"Clear jam and press reset button",WN1,MJ0
;
; Routine to teach linear force at landing position:
;
; - Softland on the part (without initial fast move)
; - Do a position move to 0.5 pitch above softland position
; - Report softland position (format !Lnnnn)
; - After a wait of 1 second report THRO at this position (Format !Qnnnn)
; - Go back home
;
MD240,1PM,SQ30000,VM,MN,SQ[Q_SETUP_LINEAR],SV[V_SETUP_LINEAR],SA[A_SETUP_LINEAR],DI0,GO,WA100
MD241,RW538,IG[PERR_SOFTLAND_LINEAR],MJ242,NO,RP
MD242,RL494,MG"!L":0,AS[1.0_PITCH_LINEAR],AR30,1PM,MA@30,GO,WS1000,RW530,MG"!Q":0,MJ0
;
;
; Routine used for debuggung only
;
MD250,MG"Rotate Backward",2AB,2VM,MN,DI1,MC150,GO,WA250,MC152
+--------------------------------------------------------------------+
|              Variables and their default values                    |
+--------------------------------------------------------------------+
P_LINEAR=12
I_LINEAR=50
D_LINEAR=600
IL_LINEAR=8000
P_ROTARY=80
I_ROTARY=120
D_ROTARY=800
IL_ROTARY=10000
V_HOMING_LINEAR=1500000
A_HOMING_LINEAR=20000
Q_HOMING_LINEAR=20000
HARDSTOP_THRESHOLD_LINEAR=-2500
INDEX_LIMIT_LINEAR=2500
V_HOMING_ROTARY=600000
A_HOMING_ROTARY=4000
Q_HOMING_ROTARY=15000
Q_MOVE_CLOSE_TO_PART_LINEAR=25000
V_MOVE_CLOSE_TO_PART_LINEAR=1250000
A_MOVE_CLOSE_TO_PART_LINEAR=20000
Q1_SOFTLAND_LINEAR=25000
V_SOFTLAND_LINEAR=500000
A_SOFTLAND_LINEAR=20000
PERR_SOFTLAND_LINEAR=2500
Q2_SOFTLAND_LINEAR=25000
V_REVERSE_FIND_ROTARY=1000000
A_REVERSE_FIND_ROTARY=50000
Q_REVERSE_FIND_ROTARY=15000
A_BRAKE_FOR_PUSH_PULL_TEST_ROTARY=3000
Q_BRAKE_FOR_PUSH_PULL_TEST_ROTARY=4000
Q_FOR_PUSH_PULL_TEST_LINEAR=20000
V_ROTATE_BACK_LINEAR=200000
Q_ROTATE_BACK_LINEAR=10000
V_MOVE_BACK_HOME_LINEAR=2000000
A_MOVE_BACK_HOME_LINEAR=30000
Q_MOVE_BACK_HOME_LINEAR=15000
V_SOFT1_ROTARY=50000
A_SOFT1_ROTARY=2000
Q_SOFT1_ROTARY=4000
V_SOFT2_ROTARY=1300000
A_SOFT2_ROTARY=60000
Q_SOFT2_ROTARY=22000
V_SOFT3_ROTARY=1000000
A_SOFT3_ROTARY=60000
Q_SOFT3_ROTARY=22000
V_SOFT4_ROTARY=2000000
A_SOFT4_ROTARY=60000
Q_SOFT4_ROTARY=22000
Q_SETUP_LINEAR=25000
V_SETUP_LINEAR=500000
A_SETUP_LINEAR=20000
PHASING_ROTARY=0
POS_CLOSE_TO_PART_LINEAR=45000
PART_POSITION_MIN_LINEAR=46000
PART_POSITION_MAX_LINEAR=50000
PUSH_PULL_LIMIT=100
0.25_REV_ROTARY=7000
0.5_REV_ROTARY=14000
1.0_REV_ROTARY=28000
1.5_REV_ROTARY=42000
0.5_PITCH_LINEAR=500
0.8_PITCH_LINEAR=800
1.0_PITCH_LINEAR=1000
0.5_DEPTH_LINEAR=3500
