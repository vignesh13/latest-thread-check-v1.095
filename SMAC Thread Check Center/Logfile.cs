﻿using System;
using System.Diagnostics;
using System.IO;

//
// Logfile will be created in the users "My documents" folder
// Constructor: Logfile(filename)
//
// After setting the Enabled property the first Add() or Addline() call will create the logfile
//
// Properties:
// Enabled (bool)   - true
// - false
// Timestamp (bool) - true: a 7 digit timestamp (in msec) will be added to each log item
// - false: no time stamp will be added
//

namespace SMAC_Thread_Check_Center
{
    class Logfile
    {
        StreamWriter streamWriter = null;
        private string fileName = "Logfile.txt"; // default log file name

        //
        // Summary: The Filename property
        //
        public string Filename
        {
            get
            {
                return fileName;
            }
            set
            {
                fileName = value;
            }
        }
        private bool enabled = false;

        //
        // Summary: The Enabled property
        //
        public bool Enabled
        {
            get
            {
                return enabled;
            }
            set
            {
                enabled = value;
                // if logging is enabled and there isn't a streamWritter then try to create one, start writing, and start timer
                if (enabled && (streamWriter == null))
                {
                    try
                    {
                        // uncommented line below for choosing where to store log file
                        // MessageBox.Show(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + Path.DirectorySeparatorChar + fileName);
                        streamWriter = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + Path.DirectorySeparatorChar + fileName);
                        streamWriter.WriteLine("Log started " + DateTime.Now.ToString());
                        stopwatch.Reset();
                        stopwatch.Start();
                    }
                    catch
                    {
                    }
                }
                // close and delete streamWriter
                else if (!enabled && (streamWriter != null)) // Not enabled anymore
                {
                    streamWriter.Close();
                    streamWriter = null;
                }
            }
        }

        //
        // The Timestamp property, create public getter and setter
        //
        private bool timestamp = false;
        public bool Timestamp
        {
            get
            {
                return timestamp;
            }
            set
            {
                timestamp = value;
            }
        }

        private bool first = true; // keep track of first write to logfile by current app

        private Stopwatch stopwatch = new Stopwatch(); // create stopwatch object of timing

        public Logfile()
        {
        }

        // The logfile constructor
        public Logfile(string filename)
        {
            fileName = filename;
        }

        // The logfile destructor
        ~Logfile()
        {
            /*
            if (streamWriter != null)
            {
                streamWriter.Close();
            }
            */

        }

        // If writing first line for current app and timestamp is on the write elapse time in log file
        public void Add(string s)
        {
            if (enabled)
            {
                try
                {
                    if (first && timestamp)
                    {
                        streamWriter.Write(stopwatch.ElapsedMilliseconds.ToString("D8") + " ");
                        first = false;
                    }
                    streamWriter.Write(s);
                    streamWriter.Flush();
                }
                catch
                {
                }
            }
        }

        // adds the first line to logfile for current app
        public void AddLine(string s)
        {
            Add(s);
            Add(Environment.NewLine);
            first = true;
        }
    }
}
