﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SMAC_Thread_Check_Center
{
    //
    // The InputBoxDouble always uses a . as the decimal separator (not depending on regional settings)
    //

    public class InputBoxDouble
    {
        private double Min = 0;
        private double Max = 0;
        private double dValue = 0;
        private TextBox Textbox = null;
        private string separator = ".";  // assume that the . is the separator
        private System.Drawing.Color okForecolor = System.Drawing.Color.Black;
        private System.Drawing.Color okBackcolor = System.Drawing.Color.White;
        private string Name = "";

        //
        // Summary: default constructor, sets values, colors, and decimal notation for a textbox using doubles
        //
        public InputBoxDouble(TextBox textbox, double min, double max, double initialvalue, string name)
        {
            Min = min;
            Max = max;
            Textbox = textbox;

            // app always uses . so convert , to . for regional use
            Textbox.Text = initialvalue.ToString().Replace(",", ".");
            double dummy = 0.5;
            if (dummy.ToString().Contains(','))
            {
                separator = ",";
            }

            Name = name;
            dValue = initialvalue;
            okForecolor = Textbox.ForeColor;
            okBackcolor = Textbox.BackColor;
        }

        //
        // Summary: just set limits for double textbox
        //
        public void SetLimits(double min, double max)
        {
            Min = min;
            Max = max;
        }

        //
        // Summary: gets string value, replaces , with . and displays
        // replacement to correct for regional ToString 
        //
        // Return: string value with , replaced with .
        //
        public string Stringvalue
        {
            // 
            get
            {
                ReadValue();
                return Textbox.Text.Trim().Replace(",", ".");
            }
            set
            {
                Textbox.Text = value.Trim();
            }
        }

        //
        // Summary: gets double value and displays
        //
        // Return: double value with , replaced with .
        //
        public double Doublevalue
        {
            get
            {
                ReadValue();
                return dValue;
            }
            set
            {
                dValue = value;
                Textbox.Text = Utils.neutralDouble(dValue);
            }
        }

        //
        // Summary: gets string value, replaces , with . and displays
        //
        public void ReadValue()
        {
            string message = "";

            // try to convert string to double accounting for region conversions
            if (!double.TryParse(Textbox.Text.Replace(".", separator).Replace(",", separator), out dValue))
            {
                // !double.TryParse(strTemp.Replace(".", localSeparator), out retval)
                // pop-up message box if it fails and set error colors
                ErrorColors();
                message = "Invalid entry " + Textbox.Text + " for " + Name + "\r" +
                          "The value you entered is not a valid number";
                message += "\rValue replaced by " + Utils.neutralDouble(Min);
                MessageBox.Show(message);

                Doublevalue = Min; // set to min by default for safety
                Textbox.Text = Utils.neutralDouble(Min); // display
            }

            ClipToLimits(); // check input value and display
            NormalColors(); // set normal colors
        }

        //
        // Summary: Set ForeColor red and BackColor light yellow for warning
        //
        private void ErrorColors()
        {
            Textbox.ForeColor = System.Drawing.Color.Red;
            Textbox.BackColor = System.Drawing.Color.LightYellow;
        }

        //
        // Summary: Set ForeColor and BackColor to normal
        //
        private void NormalColors()
        {
            Textbox.ForeColor = okForecolor;
            Textbox.BackColor = okBackcolor;
        }


        //
        // Summary: Check input value to see if null or outside of range
        //
        private void ClipToLimits()
        {
            string message = ""; // holds error message to be displayed
            double newValue = 0; // holds default value for each case

            // set double value to min if below range and send error message
            if (dValue < Min)
            {
                message = "Invalid entry " + Textbox.Text + " for " + Name + "\r" +
                          "The minimum value is " + Utils.neutralDouble(Min);
                newValue = Min;
            }
            // set double value to max if above range and send error message
            if (dValue > Max)
            {
                message = "Invalid entry " + Textbox.Text + " for " + Name + "\r" +
                          "The maximum value is " + Utils.neutralDouble(Max);
                newValue = Max;
            }
            // if value in range then replace double value with new value and alert user
            if (message != "")
            {
                ErrorColors();
                message += "\rValue will be replaced by " + Utils.neutralDouble(newValue);
                MessageBox.Show(message);
                Doublevalue = newValue;
                NormalColors();
            }
        }
    }
}
