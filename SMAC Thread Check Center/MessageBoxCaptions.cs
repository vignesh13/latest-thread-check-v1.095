﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace SMAC_Thread_Check_Center
{
    public class MessageBoxCaptions
    {
        //
        // Summary: declare loading string and loading library methods with 
        // access to exported function in a native Windows DLL
        //
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        static extern int LoadString(IntPtr hInstance, uint uID, StringBuilder lpBuffer, int nBufferMax);

        [DllImport("kernel32")]
        static extern IntPtr LoadLibrary(string lpFileName);

        // setting hard coded values for caption messages
        private const uint OK_CAPTION = 800;
        private const uint CANCEL_CAPTION = 801;
        private const uint ABORT_CAPTION = 802;
        private const uint RETRY_CAPTION = 803;
        private const uint IGNORE_CAPTION = 804;
        private const uint YES_CAPTION = 805;
        private const uint NO_CAPTION = 806;
        private const uint CLOSE_CAPTION = 807;
        private const uint HELP_CAPTION = 808;
        private const uint TRYAGAIN_CAPTION = 809;
        private const uint CONTINUE_CAPTION = 810;

        // create mutable string for holding interpreted captions 
        private StringBuilder sb = new StringBuilder(256);
        private IntPtr user32;

        // initialize caption values
        public string Ok = "";
        public string Cancel = "";
        public string Abort = "";
        public string Retry = "";
        public string Ignore = "";
        public string Yes = "";
        public string No = "";
        public string Close = "";
        public string Help = "";
        public string TryAgain = "";
        public string Cont = "";

        // converts caption numbers into string using library
        public MessageBoxCaptions()
        {
            // User32.dll is a module that contains Windows API functions related the Windows user interface 
            user32 = LoadLibrary(Environment.SystemDirectory + "\\User32.dll");

            Ok = String(OK_CAPTION);
            Cancel = String(CANCEL_CAPTION);
            Abort = String(ABORT_CAPTION);
            Retry = String(RETRY_CAPTION);
            Ignore = String(IGNORE_CAPTION);
            Yes = String(YES_CAPTION);
            No = String(NO_CAPTION);
            Close = String(CLOSE_CAPTION);
            Help = String(HELP_CAPTION);
            TryAgain = String(TRYAGAIN_CAPTION);
            Cont = String(CONTINUE_CAPTION);
        }

        //
        // Summary: Loads a string resource from exe file associated with a 
        // module, copies string into buffer, and appends a 
        // terminating null character. Saves interpreted caption
        // in string buffer.
        //
        private string String(uint caption)
        {
            LoadString(user32, caption, sb, sb.Capacity);
            return sb.ToString().Replace("&", "");
        }

    }
}
