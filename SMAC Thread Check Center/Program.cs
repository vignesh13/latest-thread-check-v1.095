﻿using System;
using System.Windows.Forms;

namespace SMAC_Thread_Check_Center
{
    static class Program
    {
        // The main entry point for the application.
        [STAThread] // required to communicate with COM components
        static void Main()
        {
            Application.EnableVisualStyles(); // allow user side styles
            Application.SetCompatibleTextRenderingDefault(false); // use GDI graphics library
            Application.Run(new MainForm()); // run main form
        }
    }
}
